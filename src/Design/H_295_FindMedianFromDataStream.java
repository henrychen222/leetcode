/**
 * 03/18/21 afternoon  evening
 * https://leetcode.com/problems/implement-trie-prefix-tree/
 */

package Design;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;
import java.util.TreeSet;

class MedianFinder1 {

	PriorityQueue<double[]> pq;

	public MedianFinder1() {
		// issue with pq ordering
		pq = new PriorityQueue<double[]>((x, y) -> Double.compare(x[0], y[0]));
	}

	public void addNum(int num) {
		pq.add(new double[] { num });
	}

	public double findMedian() {
		int n = pq.size();
		// System.out.println(n);
		int m = n >> 1;
		int i = 0;
		double v, right;
		v = right = 0;
		double left = 0;
		for (double[] e : pq) {
			if (i == m - 1) {
				left = e[0];
			} else if (i == m) {
				v = right = e[0];
				break;
			}
			i++;
		}
		System.out.println(Arrays.deepToString(pq.toArray()));
		// System.out.println(v + " " + left + " " + right);
		return (n & 1) == 1 ? v : (double) (left + right) / 2;
	}
}

class MedianFinder {

	PriorityQueue<Integer> pq;

	public MedianFinder() { // issue don't know why not sort correctly
//		pq = new PriorityQueue<Integer>((x, y) -> {
//			if (x > y) return 1;
//			else if (x < y) return -1;
//			return 0;
//		});
		pq = new PriorityQueue<Integer>((x, y) -> x - y);
		// pq = new PriorityQueue<Integer>(Collections.reverseOrder());
		// pq = new PriorityQueue<Integer>((x, y) -> x.compareTo(y));
	}

	public void addNum(int num) {
		pq.add(num);
		System.out.println(Arrays.toString(pq.toArray()));
	}

	public double findMedian() {
		int n = pq.size();
		int m = n >> 1;
		int i = 0;
		double v, right;
		v = right = 0;
		double left = 0;
//		PriorityQueue<Integer> pqc = pq;  // also pq always be cleaned, why, it is a copy
//		while (!pqc.isEmpty()) {
//			int cur = pqc.poll();
//			if (i == m - 1) {
//				left = cur;
//			} else if (i == m) {
//				v = right = cur;
//				break;
//			}
//			i++;
//		}
		for (double e : pq) {
			if (i == m - 1) {
				left = e;
			} else if (i == m) {
				v = right = e;
				break;
			}
			i++;
		}
		return (n & 1) == 1 ? v : (left + right) / 2;
	}

}

class MedianFinder3 {

	TreeSet<Integer> se;

	public MedianFinder3() {
		se = new TreeSet<>(); // wrong, set will remove duplicates
	}

	public void addNum(int num) {
		se.add(num);
	}

	public double findMedian() {
		int n = se.size();
		int m = n >> 1;
		int i = 0;
		double v = 0;
		double right = 0;
		double left = 0;
		System.out.println(se);
		if ((n & 1) == 1) {
			for (double e : se) {
				if (i == m)
					v = e;
				i++;
			}
			return v;
		} else {
			for (double e : se) {
				if (i == m - 1) {
					left = e;
				} else if (i == m) {
					right = e;
				}
				i++;
			}
			return (left + right) / 2;
		}
	}
}

// Accepted --- 1674ms 5.06%
class MedianFinder4 {

	List<Integer> l;

	public MedianFinder4() {
		l = new ArrayList<>();
	}

	public void addNum(int num) {
		l.add(num);
	}

	public double findMedian() {
		Collections.sort(l);
		int n = l.size();
		int m = n >> 1;
		return (n & 1) == 1 ? l.get(m) : (double) (l.get(m - 1) + l.get(m)) / 2;
	}
}

public class H_295_FindMedianFromDataStream {

	public static void main(String[] args) {
//		MedianFinder medianFinder = new MedianFinder();
//		medianFinder.addNum(1);
//		medianFinder.addNum(2);
//		System.out.println(medianFinder.findMedian()); // 1.5
//		medianFinder.addNum(3);
//		System.out.println(medianFinder.findMedian()); // 2.0

		System.out.println();
		MedianFinder debug2 = new MedianFinder();
		debug2.addNum(6);
		System.out.println(debug2.findMedian()); // 6
		debug2.addNum(10);
		System.out.println(debug2.findMedian()); // 8
		debug2.addNum(2);
		System.out.println(debug2.findMedian()); // 6
		debug2.addNum(6);
		System.out.println(debug2.findMedian()); // 6
		debug2.addNum(5);
		System.out.println(debug2.findMedian()); // 6
		debug2.addNum(0);
		System.out.println(debug2.findMedian()); // 5.5
		debug2.addNum(6);
		System.out.println(debug2.findMedian()); // 6
		debug2.addNum(3);
		System.out.println(debug2.findMedian()); // 5.5
		debug2.addNum(1);
		System.out.println(debug2.findMedian()); // 5
		debug2.addNum(0);
		System.out.println(debug2.findMedian()); // 4
		debug2.addNum(0);
		System.out.println(debug2.findMedian()); // 3
	}

}