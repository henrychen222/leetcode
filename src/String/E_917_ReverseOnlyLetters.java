/**
 * 1.10 night created
 * 
 * 1.18 evening
 * https://leetcode.com/problems/reverse-only-letters/
 * 
 * Character.isLetter()  same to c++ isalpha()
 */
package String;

import java.util.Stack;

public class E_917_ReverseOnlyLetters {

	/**
	 * Accepted --- 2ms 42MB 12.28%
	 * 
	 * https://leetcode.com/articles/reverse-only-letters/
	 */
	public String reverseOnlyLetters_stack(String S) {
		Stack<Character> letters = new Stack<Character>();
		for (char c : S.toCharArray())
			if (Character.isLetter(c))
				letters.push(c);

		StringBuilder ans = new StringBuilder();
		for (char c : S.toCharArray()) {
			if (Character.isLetter(c))
				ans.append(letters.pop());
			else
				ans.append(c);
		}

		return ans.toString();
	}

	/**
	 * Accepted --- 1ms 41.3 MB 47.50%
	 * 
	 * https://leetcode.com/articles/reverse-only-letters/
	 */
	public String reverseOnlyLetters_two_pointers1(String S) {
		StringBuilder ans = new StringBuilder();
		int j = S.length() - 1;
		for (int i = 0; i < S.length(); ++i) { // two pointer i and j
			if (Character.isLetter(S.charAt(i))) {
				while (!Character.isLetter(S.charAt(j)))
					j--;
				ans.append(S.charAt(j--));
			} else {
				ans.append(S.charAt(i));
			}
		}
		return ans.toString();
	}

	/**
	 * Accepted --- 0ms 40.9 MB 100.00%
	 * 
	 * https://github.com/fishercoder1534/Leetcode/blob/master/src/main/java/com/fishercoder/solutions/_917.java
	 */
	public String reverseOnlyLetters_two_pointers2(String S) {
		char[] array = S.toCharArray();
		for (int i = 0, j = array.length - 1; i < j;) { // two pointer i and j
			if (Character.isLetter(array[i]) && Character.isLetter(array[j])) {
				char temp = array[i];
				array[i++] = array[j];
				array[j--] = temp;
			} else if (Character.isLetter(array[i])) {
				j--;
			} else if (Character.isLetter(array[j])) {
				i++;
			} else {
				i++;
				j--;
			}
		}
		return new String(array);
	}

	/**
	 * Accepted --- 1ms 41.2 MB 47.50%
	 * 
	 * https://github.com/varunu28/LeetCode-Java-Solutions/blob/master/Contests/Weekly%20Contest%20105/Reverse%20Only%20Letters.java
	 */
	public String reverseOnlyLetters_two_pointers3(String S) {
		char[] chars = S.toCharArray();

		// two pointer start and end
		int start = 0;
		int end = chars.length - 1;

		while (start < end) {
			if (Character.isLetter(chars[start]) && Character.isLetter(chars[end])) {
				char temp = chars[start];
				chars[start] = chars[end];
				chars[end] = temp;

				start++;
				end--;
			} else if (Character.isLetter(chars[start]) && !Character.isLetter(chars[end])) {
				end--;
			} else if (!Character.isLetter(chars[start]) && Character.isLetter(chars[end])) {
				start++;
			} else {
				start++;
				end--;
			}
		}
		return String.valueOf(chars);
	}

	/**
	 * Accepted --- 0ms 41.4 MB 100.00%
	 * 
	 * https://segmentfault.com/a/1190000016951137
	 */
	public String reverseOnlyLetters_two_pointers4(String S) {
		char[] str = S.toCharArray();
		int i = 0, j = str.length - 1; // two pointer i and j
		while (i < j) {
			while (i < j && !Character.isLetter(str[i]))
				i++;
			while (i < j && !Character.isLetter(str[j]))
				j--;
			swap(str, i++, j--);
		}
		StringBuilder sb = new StringBuilder();
		sb.append(str);
		return sb.toString();
	}

	private void swap(char[] str, int i, int j) {
		char temp = str[i];
		str[i] = str[j];
		str[j] = temp;
	}

	/**
	 * Accepted --- 0ms 41.2 MB 100.00%
	 * 
	 * https://codedestine.com/reverse-only-letters-of-string/
	 */
	public String reverseOnlyLetters_two_pointers5(String str) {
		char[] array = str.toCharArray();
		int start = 0, end = array.length - 1; // two pointer start and end
		char temp;
		boolean isStartFound = false, isEndFound = false;
		while (start < end) {

			if ((array[start] >= 'a' && array[start] <= 'z') || (array[start] >= 'A' && array[start] <= 'Z')) {
				isStartFound = true;
			} else {
				start++;
			}

			if ((array[end] >= 'a' && array[end] <= 'z') || (array[end] >= 'A' && array[end] <= 'Z')) {
				isEndFound = true;
			} else {
				end--;
			}

			if (isStartFound && isEndFound) {
				temp = array[end];
				array[end] = array[start];
				array[start] = temp;
				isStartFound = false;
				isEndFound = false;
				start++;
				end--;
			}
		}
		return String.valueOf(array);
	}

	public static void main(String[] args) {
		E_917_ReverseOnlyLetters test = new E_917_ReverseOnlyLetters();

		String s1 = "ab-cd";
		String s2 = "a-bC-dEf-ghIj";
		String s3 = "Test1ng-Leet=code-Q!";

		System.out.println(test.reverseOnlyLetters_stack(s1)); // "dc-ba"
		System.out.println(test.reverseOnlyLetters_stack(s2)); // "j-Ih-gfE-dCba"
		System.out.println(test.reverseOnlyLetters_stack(s3)); // "Qedo1ct-eeLg=ntse-T!"

		System.out.println("");
		System.out.println(test.reverseOnlyLetters_two_pointers1(s1));
		System.out.println(test.reverseOnlyLetters_two_pointers1(s2));
		System.out.println(test.reverseOnlyLetters_two_pointers1(s3));

		// best
		System.out.println("");
		System.out.println(test.reverseOnlyLetters_two_pointers2(s1));
		System.out.println(test.reverseOnlyLetters_two_pointers2(s2));
		System.out.println(test.reverseOnlyLetters_two_pointers2(s3));

		System.out.println("");
		System.out.println(test.reverseOnlyLetters_two_pointers3(s1));
		System.out.println(test.reverseOnlyLetters_two_pointers3(s2));
		System.out.println(test.reverseOnlyLetters_two_pointers3(s3));

		// best
		System.out.println("");
		System.out.println(test.reverseOnlyLetters_two_pointers4(s1));
		System.out.println(test.reverseOnlyLetters_two_pointers4(s2));
		System.out.println(test.reverseOnlyLetters_two_pointers4(s3));

		// best
		System.out.println("");
		System.out.println(test.reverseOnlyLetters_two_pointers5(s1));
		System.out.println(test.reverseOnlyLetters_two_pointers5(s2));
		System.out.println(test.reverseOnlyLetters_two_pointers5(s3));

	}

}
