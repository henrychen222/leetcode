/**
 * 1.3 night
 * https://leetcode.com/problems/greatest-common-divisor-of-strings/
 */
package String;

public class E_1071_GreatestCommonDivisorStrings {

	/**
	 * Accepted --- 1ms 36.1 MB 48.05% 暴力解
	 * https://leetcode.jp/leetcode-1071-greatest-common-divisor-of-strings-%E8%A7%A3%E9%A2%98%E6%80%9D%E8%B7%AF%E5%88%86%E6%9E%90/
	 */
	public String gcdOfStrings(String str1, String str2) {
		if (str1.equals(str2)) {
			return str2;
		}

		// 最大公约数的最大范围（不可能大于较大数的一半以上）
		int maxLength = Math.max(str1.length(), str2.length()) / 2;

		// 循环查找公约数, 如果不能被两字符长度整除，则不是公约数，跳过。
		for (int i = maxLength; i > 0; i--) {
			if (str1.length() % i != 0 || str2.length() % i != 0) {
				continue;
			}

			// 以当前公约数为长度截取字符串前i位，取得公约字符串
			String common = str1.substring(0, i);

			// 查看2字符串是否都由公约字符串组成，是的话，直接返回该公约字符串，否则继续循环
			if (isCommon(str1, common) && isCommon(str2, common)) {
				return common;
			}
		}
		return "";
	}

	boolean isCommon(String str, String common) {
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) != common.charAt(i % common.length())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Accepted --- 0ms 38 MB 100% 递归
	 * https://leetcode.jp/leetcode-1071-greatest-common-divisor-of-strings-%E8%A7%A3%E9%A2%98%E6%80%9D%E8%B7%AF%E5%88%86%E6%9E%90/
	 */
	public String gcdOfStrings2(String str1, String str2) {
		// 保证str1长度大于str2长度
		if (str1.length() < str2.length()) {
			return gcdOfStrings(str2, str1);
		}
		// 如果2字符串相同，直接返回其中一个
		if (str1.equals(str2)) {
			return str1;
		}
		// 如果较短的字符串为空，返回空
		if (str2.equals("")) {
			return str2;
		}
		// 如果较长字符串1以字符串2开头
		if (str1.startsWith(str2)) {
			// 将字符串1截取掉字符串2的部分，递归道下一步。
			// 本题实际递归的终止条件为str1等于str2
			return gcdOfStrings(str2, str1.substring(str2.length()));
		}
		return "";
	}

	public static void main(String[] args) {
		E_1071_GreatestCommonDivisorStrings test = new E_1071_GreatestCommonDivisorStrings();

		String str1_example1 = "ABCABC", str2_example1 = "ABC";
		String str1_example2 = "ABABAB", str2_example2 = "ABAB";
		String str1_example3 = "LEET", str2_example3 = "CODE";

		System.out.println(test.gcdOfStrings(str1_example1, str2_example1)); // "ABC"
		System.out.println(test.gcdOfStrings(str1_example2, str2_example2)); // "AB"
		System.out.println(test.gcdOfStrings(str1_example3, str2_example3)); // ""

		System.out.println(test.gcdOfStrings2(str1_example1, str2_example1));
		System.out.println(test.gcdOfStrings2(str1_example2, str2_example2));
		System.out.println(test.gcdOfStrings2(str1_example3, str2_example3));

	}

}
