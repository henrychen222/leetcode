package String;

import java.util.ArrayList;
import java.util.List;

public class M_1268_Trie {

	M_1268_TrieNode root = new M_1268_TrieNode();

	private void insertWord(String product) {
		M_1268_TrieNode node = root;
		for (char c : product.toCharArray()) {
			if (node.children[c - 'a'] == null) {
				node.children[c - 'a'] = new M_1268_TrieNode();
			}
			node = node.children[c - 'a'];
		}

		if (node.end != true) {
			node.end = true;
			node.str = product;
		}

		node.count++;
	}

	public void insert(String[] products) {
		for (String product : products) {
			insertWord(product);
		}
	}

	private List<String> search(String pattern) {
		List<String> result = new ArrayList<>();
		M_1268_TrieNode node = root;
		for (char c : pattern.toCharArray()) {
			if (node.children[c - 'a'] == null) {
				return result;
			}
			node = node.children[c - 'a'];
		}
		Solution(node, result);
		return result;
	}

	public List<List<String>> searchWord(String word) {
		List<List<String>> result = new ArrayList<List<String>>();
		for (int i = 1; i <= word.length(); i++) {
			result.add(search(word.substring(0, i)));
		}
		return result;
	}

	private void Solution(M_1268_TrieNode root, List<String> result) {
		if (root.end) {
			for (int i = 0; i < root.count; i++) {
				result.add(root.str);
				if (result.size() == 3) {
					return;
				}
			}
		}
		for (M_1268_TrieNode node : root.children) {
			if (node != null) {
				Solution(node, result);
			}
			if (result.size() == 3) {
				return;
			}
		}
	}

}
