/**
 * 12.28 evening
 * https://leetcode.com/problems/minimum-swaps-to-make-strings-equal/
 */
package String;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class M_1247_MinimumSwapsMakeStringsEqual {

	// translated from c++, Accepted --- 1ms, 34.4MB, 50.84%
	public int minimumSwap(String s1, String s2) {
		int xy = 0;
		int yx = 0;
		for (int i = 0; i < s1.length(); ++i) {
			if (s1.charAt(i) == 'x' && s2.charAt(i) == 'y') {
				++xy;
			}
			if (s1.charAt(i) == 'y' && s2.charAt(i) == 'x') {
				++yx;
			}
		}
		if ((xy + yx) % 2 != 0) {
			return -1;
		}
		return (xy + 1) / 2 + (yx + 1) / 2;
	}

	// Accepted --- 0ms, 34.4MB, 100.00%
	public int minimumSwap2(String s1, String s2) {
		int n = s1.length(), i = 0, xy = 0, yx = 0;
		for (i = 0; i < n; ++i) {
			char c1 = s1.charAt(i);
			char c2 = s2.charAt(i);
			if (c1 == 'x' && c2 == 'y') {
				++xy;
			}
			if (c1 == 'y' && c2 == 'x') {
				++yx;
			}
		}
		if ((xy - yx) % 2 != 0) {
			return -1;
		}
		if (xy == 0 && yx == 0) {
			return 0;
		}
		if (xy % 2 == 0 && yx % 2 == 0) {
			return xy / 2 + yx / 2;
		}
		return Math.min(xy, yx) + 1 + Math.abs(xy - yx) / 2;
	}

	public static void main(String[] args) {
		M_1247_MinimumSwapsMakeStringsEqual test = new M_1247_MinimumSwapsMakeStringsEqual();

		String s1_one = "xx", s2_one = "yy";
		String s1_two = "xy", s2_two = "yx";
		String s1_three = "xx", s2_three = "xy";
		String s1_four = "xxyyxyxyxx", s2_four = "xyyxyxxxyx";

		System.out.println(test.minimumSwap(s1_one, s2_one)); // 1
		System.out.println(test.minimumSwap(s1_two, s2_two)); // 2
		System.out.println(test.minimumSwap(s1_three, s2_three)); // -1
		System.out.println(test.minimumSwap(s1_four, s2_four)); // 4

		assertEquals(test.minimumSwap(s1_one, s2_one), 1);
		assertEquals(test.minimumSwap(s1_two, s2_two), 2);
		assertEquals(test.minimumSwap(s1_three, s2_three), -1);
		assertEquals(test.minimumSwap(s1_four, s2_four), 4);
		assertTrue(test.minimumSwap(s1_one, s2_one) == 1);
		assertTrue(test.minimumSwap(s1_two, s2_two) == 2);
		assertTrue(test.minimumSwap(s1_three, s2_three) == -1);
		assertTrue(test.minimumSwap(s1_four, s2_four) == 4);

		System.out.println("");
		System.out.println(test.minimumSwap2(s1_one, s2_one)); // 1
		System.out.println(test.minimumSwap2(s1_two, s2_two)); // 2
		System.out.println(test.minimumSwap2(s1_three, s2_three)); // -1
		System.out.println(test.minimumSwap2(s1_four, s2_four)); // 4

	}

}
