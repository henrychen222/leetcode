/**
 * 9.26 evening 
 * https://leetcode.com/problems/can-make-palindrome-from-substring/
 */
package String;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Can_Make_Palindrome_from_Substring_1177 {
	/**
	 * Method 1: Binary Search --- ACCEPT, 731ms, too slow
	 */
	public List<Boolean> canMakePaliQueries_BS(String s, int[][] queries) {
		List<Boolean> res = new ArrayList<>();
		List<Integer>[] indices = new List[26];
		for (int i = 0; i < 26; i++) {
			indices[i] = new ArrayList<>();
		}

		for (int i = 0; i < s.length(); i++) {
			indices[s.charAt(i) - 'a'].add(i);
		}

		for (int i = 0; i < queries.length; i++) {
			res.add(canMake_BS(indices, queries[i][0], queries[i][1], queries[i][2]));
		}
		return res;
	}

	private boolean canMake_BS(List[] indices, int left, int right, int k) {
		int sum = 0;
		for (int i = 0; i < 26; i++) {
			List<Integer> list = indices[i];
			int r = getRightBound(list, right);
			int l = getLeftBound(list, left);
			if (l >= 0 && r >= 0 && l <= r) {
				sum += (r - l + 1) % 2;
			}
		}
		return sum / 2 <= k;
	}

	private int getLeftBound(List<Integer> list, int target) {
		if (list.size() == 0) {
			return -1;
		}
		int left = 0, right = list.size() - 1;
		while (left < right - 1) {
			int mid = left + (right - left) / 2;
			if (list.get(mid) < target) {
				left = mid + 1;
			} else {
				right = mid;
			}
		}
		if (list.get(left) >= target) {
			return left;
		} else if (list.get(right) >= target) {
			return right;
		}
		return -1;
	}

	private int getRightBound(List<Integer> list, int target) {
		if (list.size() == 0) {
			return -1;
		}
		int left = 0, right = list.size() - 1;
		while (left < right - 1) {
			int mid = left + (right - left) / 2;
			if (list.get(mid) > target) {
				right = mid - 1;
			} else {
				left = mid;
			}
		}
		if (list.get(right) <= target) {
			return right;
		} else if (list.get(left) <= target) {
			return left;
		}
		return -1;
	}

	/**
	 * Method 2: TreeMap --- Fail, Time Limit Exceeded (Correct)
	 */
	public List<Boolean> canMakePaliQueries_TreeMap(String s, int[][] queries) {
		List<Boolean> result = new ArrayList<>();
		TreeMap<Integer, Integer>[] indices = new TreeMap[26]; // TreeMap array
		for (int i = 0; i < 26; i++) {
			indices[i] = new TreeMap<>();
		}

		for (int i = 0; i < s.length(); i++) {
			int index = s.charAt(i) - 'a';
			indices[index].put(i, indices[index].size());
		}

		for (int i = 0; i < queries.length; i++) {
			result.add(canMake_TreeMap(indices, queries[i][0], queries[i][1], queries[i][2]));
		}

		return result;
	}

	private boolean canMake_TreeMap(TreeMap[] indices, int left, int right, int k) {
		int sum = 0;
		for (int i = 0; i < 26; i++) {
			TreeMap<Integer, Integer> map = indices[i];
			Map.Entry<Integer, Integer> leftBound = map.ceilingEntry(left);
			Map.Entry<Integer, Integer> rightBound = map.floorEntry(right);
			if (leftBound != null && rightBound != null && leftBound.getValue() <= rightBound.getValue()) {
				sum += ((rightBound.getValue() - leftBound.getValue() + 1) % 2);
			}
		}
		return sum / 2 <= k;
	}

	/**
	 * Method 3: Prefix sum. Traverse s, find all char appearance from 0 to i. Then
	 * based on given range, count the char frequency. Find all odd chars and see if
	 * they are more than replacement limit. ---ACCEPT, 63 ms
	 */
	public List<Boolean> canMakePaliQueries_PrefixSum(String s, int[][] queries) {
		List<Boolean> out = new ArrayList<>();
		int[][] prefixSum = new int[s.length() + 1][26];
		for (int i = 1; i <= s.length(); i++) {
			prefixSum[i] = prefixSum[i - 1].clone();
			prefixSum[i][s.charAt(i - 1) - 'a']++;
		}

		for (int[] query : queries) {
			if (query[2] > 13) {
				out.add(true);
			} else {
				int count = 0;
				for (int i = 0; i < 26; i++) {
					count += (prefixSum[query[1] + 1][i] - prefixSum[query[0]][i]) % 2;
				}
				out.add(count / 2 <= query[2]);
			}
		}
		return out;
	}

	public static void main(String[] args) {
		Can_Make_Palindrome_from_Substring_1177 test = new Can_Make_Palindrome_from_Substring_1177();
		String s = "abcda";
		int[][] queries = new int[][] { { 3, 3, 0 }, { 1, 2, 0 }, { 0, 3, 1 }, { 0, 3, 2 }, { 0, 4, 1 } };
		System.out.println(test.canMakePaliQueries_BS(s, queries));
		System.out.println(test.canMakePaliQueries_TreeMap(s, queries));
		System.out.println(test.canMakePaliQueries_PrefixSum(s, queries));

	}

}
