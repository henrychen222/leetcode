/**
 * 1.10 night created
 * 
 * 1.18 evening
 * https://leetcode.com/problems/word-subsets/
 */
package String;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class M_916_WordSubsets {

	/**
	 * Accepted --- 22ms 55.5 MB 75.34%
	 * 
	 * https://leetcode.com/articles/word-subsets/
	 */
	public List<String> wordSubsets(String[] A, String[] B) {
		int[] bmax = count("");
		for (String b : B) {
			int[] bCount = count(b);
			for (int i = 0; i < 26; ++i)
				bmax[i] = Math.max(bmax[i], bCount[i]);
		}

		List<String> ans = new ArrayList<String>();
		search: for (String a : A) {
			int[] aCount = count(a);
			for (int i = 0; i < 26; ++i)
				if (aCount[i] < bmax[i])
					continue search;
			ans.add(a);
		}
		return ans;
	}

	public int[] count(String S) {
		int[] ans = new int[26];
		for (char c : S.toCharArray())
			ans[c - 'a']++;
		return ans;
	}

	/**
	 * Accepted --- 22ms 56.1 MB 75.34%
	 * 
	 * https://massivealgorithms.blogspot.com/2018/11/leetcode-916-word-subsets.html
	 * https://leetcode.com/problems/word-subsets/discuss/175854/C%2B%2BJavaPython-Straight-Forward
	 */
	public List<String> wordSubsets2(String[] A, String[] B) {
		int[] count = new int[26], tmp;
		int i;
		for (String b : B) {
			tmp = count(b);
			for (i = 0; i < 26; ++i)
				count[i] = Math.max(count[i], tmp[i]);
		}
		List<String> res = new ArrayList<>();
		for (String a : A) {
			tmp = count(a);
			for (i = 0; i < 26; ++i)
				if (tmp[i] < count[i])
					break;
			if (i == 26)
				res.add(a);
		}
		return res;
	}

	/**
	 * Accepted --- 69ms 60.3 MB 12.33%
	 * 
	 * https://github.com/Cee/Leetcode/blob/master/916%20-%20Word%20Subsets.java
	 */
	public List<String> wordSubsets3(String[] A, String[] B) {
		int[] max = new int[26];
		for (String b : B) {
			Map<Character, Integer> map = new HashMap<>();
			for (char c : b.toCharArray()) {
				map.put(c, map.getOrDefault(c, 0) + 1);
				max[c - 'a'] = Math.max(max[c - 'a'], map.get(c));
			}
		}

		List<String> ans = new ArrayList<>();
		for (String a : A) {
			Map<Character, Integer> map = new HashMap<>();
			for (char c : a.toCharArray()) {
				map.put(c, map.getOrDefault(c, 0) + 1);
			}
			boolean valid = true;
			for (char c = 'a'; c <= 'z'; c++) {
				if (max[c - 'a'] > 0 && (!map.containsKey(c) || map.get(c).intValue() < max[c - 'a'])) {
					valid = false;
					break;
				}
			}
			if (valid) {
				ans.add(a);
			}
		}
		return ans;
	}

	/**
	 * Accepted --- 36ms 55.8 MB 21.23%
	 * 
	 * https://www.cnblogs.com/Dylan-Java-NYC/p/11993123.html
	 * https://www.ancii.com/aha767x8n/
	 */
	public List<String> wordSubsets4(String[] A, String[] B) {
		List<String> res = new ArrayList<>();
		if (A == null || A.length == 0) {
			return res;
		}

		if (B == null || B.length == 0) {
			return Arrays.asList(A);
		}

		int[] map = new int[26];
		for (String b : B) {
			int[] bMap = new int[26];
			for (char c : b.toCharArray()) {
				bMap[c - 'a']++;
			}

			for (int i = 0; i < 26; i++) {
				map[i] = Math.max(map[i], bMap[i]);
			}
		}

		for (String a : A) {
			int[] aMap = new int[26];
			for (char c : a.toCharArray()) {
				aMap[c - 'a']++;
			}

			boolean isNotSubSet = false;
			for (int i = 0; i < 26; i++) {
				if (aMap[i] < map[i]) {
					isNotSubSet = true;
					break;
				}
			}

			if (!isNotSubSet) {
				res.add(a);
			}
		}

		return res;
	}

	public static void main(String[] args) {
		M_916_WordSubsets test = new M_916_WordSubsets();

		String[] A = new String[] { "amazon", "apple", "facebook", "google", "leetcode" };
		String[] B = new String[] { "e", "o" };

		String[] A2 = new String[] { "amazon", "apple", "facebook", "google", "leetcode" };
		String[] B2 = new String[] { "l", "e" };

		String[] A3 = new String[] { "amazon", "apple", "facebook", "google", "leetcode" };
		String[] B3 = new String[] { "e", "oo" };

		String[] A4 = new String[] { "amazon", "apple", "facebook", "google", "leetcode" };
		String[] B4 = new String[] { "lo", "eo" };

		String[] A5 = new String[] { "amazon", "apple", "facebook", "google", "leetcode" };
		String[] B5 = new String[] { "ec", "oc", "ceo" };

		System.out.println(test.wordSubsets(A, B)); // ["facebook","google","leetcode"]
		System.out.println(test.wordSubsets(A2, B2)); // ["apple","google","leetcode"]
		System.out.println(test.wordSubsets(A3, B3)); // ["facebook","google"]
		System.out.println(test.wordSubsets(A4, B4)); // ["google","leetcode"]
		System.out.println(test.wordSubsets(A5, B5)); // ["facebook","leetcode"]

		System.out.println("");
		System.out.println(test.wordSubsets2(A, B));
		System.out.println(test.wordSubsets2(A2, B2));
		System.out.println(test.wordSubsets2(A3, B3));
		System.out.println(test.wordSubsets2(A4, B4));
		System.out.println(test.wordSubsets2(A5, B5));

		System.out.println("");
		System.out.println(test.wordSubsets3(A, B));
		System.out.println(test.wordSubsets3(A2, B2));
		System.out.println(test.wordSubsets3(A3, B3));
		System.out.println(test.wordSubsets3(A4, B4));
		System.out.println(test.wordSubsets3(A5, B5));

		System.out.println("");
		System.out.println(test.wordSubsets4(A, B));
		System.out.println(test.wordSubsets4(A2, B2));
		System.out.println(test.wordSubsets4(A3, B3));
		System.out.println(test.wordSubsets4(A4, B4));
		System.out.println(test.wordSubsets4(A5, B5));

	}

}
