/**
 * 9.26 evening 
 */
package String;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Maximum_Number_of_Balloons_1189 {

	public int maxNumberOfBalloons(String text) {
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		// map.put('b', 0);
		// map.put('a', 0);
		// map.put('l', 0);
		// map.put('o', 0);
		// map.put('n', 0);

		for (int i = 0; i < text.length(); i++) {
			if (text.charAt(i) == 'a' || text.charAt(i) == 'b' || text.charAt(i) == 'l' || text.charAt(i) == 'o'
					|| text.charAt(i) == 'n') {
				map.put(text.charAt(i), i + 1); // ????
			}
		}

		// for (char it : text.toCharArray()) {
		// if (it == 'a' || it == 'b' || it == 'l' || it == 'o' || it == 'n')
		// map.put(it, 0);
		// map.get(it);
		// }
		// }

		int s = Math.min(map.get('a'), Math.min(map.get('b'), map.get('n')));
		int d = Math.min(map.get('l'), map.get('o'));

		if (d >= 2 * s) {
			return s;
		}
		return d / 2;
	}

	public static void main(String[] args) {
		Maximum_Number_of_Balloons_1189 test = new Maximum_Number_of_Balloons_1189();
		String text1 = "nlaebolko";
		String text2 = "loonbalxballpoon";
		String text3 = "leetcode";
		System.out.println(test.maxNumberOfBalloons(text1)); // 1
		System.out.println(test.maxNumberOfBalloons(text2)); // 2
//		System.out.println(test.maxNumberOfBalloons(text3)); // 0
	}

}
