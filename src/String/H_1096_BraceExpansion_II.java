/**
 * 12.31 night
 * https://leetcode.com/problems/brace-expansion-ii/
 */
package String;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class H_1096_BraceExpansion_II {

	/**
	 * Accepted --- 9ms 37.5 MB 83.39%
	 * https://leetcode.com/problems/brace-expansion-ii/discuss/458182/Short-and-clean-Java-recursive
	 * (Recursive)
	 */
	public List<String> braceExpansionII(String expression) {
		if (expression == null || expression.length() == 0)
			return new ArrayList<>();
		List<List<String>> group = new ArrayList<>();
		Set<String> res = new TreeSet<>();
		int level = 0, start = -1;
		for (int i = 0; i < expression.length(); i++) {
			char cur = expression.charAt(i);
			if (cur == '{') {
				if (level == 0)
					start = i + 1;
				level++;
			} else if (cur == '}') {
				level--;
				if (level == 0)
					group.add(braceExpansionII(expression.substring(start, i)));
			} else if (cur == ',' && level == 0) {
				res.addAll(combine(group));
				group.clear();
			} else if (level == 0) {
				group.add(Arrays.asList(String.valueOf(cur)));
			}
		}
		res.addAll(combine(group));
		return new ArrayList<>(res);
	}

	private List<String> combine(List<List<String>> group) {
		List<String> res = new ArrayList<>();
		res.add("");
		for (List<String> g : group) {
			List<String> temp = new ArrayList<>();
			for (String p : res) {
				for (String s : g) {
					temp.add(p + s);
				}
			}
			res = temp;
		}
		return res;
	}

	/**
	 * Accepted --- 5ms 37 MB 99.78%
	 * https://leetcode.com/problems/brace-expansion-ii/discuss/434568/Java-solution-100-5ms
	 */
	public List<String> braceExpansionII2(String expression) {
		List<String> splits = splits(expression);
		if (splits.size() == 1) {
			int i = 0;
			List<String> multi = new ArrayList<>();
			while (i < expression.length()) {
				char c = expression.charAt(i);
				if (c == '{') {
					int j = findClose(expression, i);
					String sub = expression.substring(i + 1, j);
					List<String> res = braceExpansionII(sub);
					multi = multi.isEmpty() ? res : doMulti(multi, res);
					i = j + 1;
				} else {
					int j = i + 1;
					while (j < expression.length()) {
						if (expression.charAt(j) >= 'a' && expression.charAt(j) <= 'z')
							j++;
						else
							break;
					}
					List<String> res = Collections.singletonList(expression.substring(i, j));
					multi = multi.isEmpty() ? res : doMulti(multi, res);
					i = j;
				}
			}
			return multi;

		}
		List<String> merge = new ArrayList<>();
		for (String split : splits) {
			List<String> subs = braceExpansionII(split);
			for (String sub : subs) {
				if (!merge.contains(sub)) {
					merge.add(sub);
				}
			}
		}
		Collections.sort(merge);
		return merge;
	}

	public List<String> splits(String exp) {
		int i = 0;
		List<String> res = new ArrayList<>();
		while (i < exp.length()) {
			int j = i;
			int open = 0;

			while (j < exp.length()) {
				char c = exp.charAt(j);
				if (c == ',') {
					if (open == 0)
						break;
				} else if (c == '{') {
					open++;
				} else if (c == '}') {
					open--;
				}
				j++;
			}
			res.add(exp.substring(i, j));
			i = j + 1;
		}
		Collections.sort(res);
		return res;
	}

	private List<String> doMulti(List<String> multi, List<String> res) {
		List<String> product = new ArrayList<>();
		for (String m : multi) {
			for (String re : res) {
				product.add(m + re);
			}
		}
		Collections.sort(product);
		return product;
	}

	public int findClose(String exp, int st) {
		int i = st + 1;
		int open = 0;
		while (i < exp.length()) {
			if (exp.charAt(i) == '}') {
				if (open > 0) {
					open--;
				} else if (open == 0) {
					return i;
				}
			} else if (exp.charAt(i) == '{') {
				open++;
			}
			i++;
		}
		return i;
	}

	public static void main(String[] args) {
		H_1096_BraceExpansion_II test = new H_1096_BraceExpansion_II();
		String expression1 = "{a,b}{c,{d,e}}";
		String expression2 = "{{a,z},a{b,c},{ab,z}}";

		System.out.println(test.braceExpansionII(expression1)); // ["ac","ad","ae","bc","bd","be"]
		System.out.println(test.braceExpansionII(expression2)); // ["a","ab","ac","z"]

		System.out.println("");
		System.out.println(test.braceExpansionII2(expression1)); // ["ac","ad","ae","bc","bd","be"]
		System.out.println(test.braceExpansionII2(expression2)); // ["a","ab","ac","z"]

	}

}
