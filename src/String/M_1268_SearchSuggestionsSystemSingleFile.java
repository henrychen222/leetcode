package String;

import java.util.ArrayList;
import java.util.List;

public class M_1268_SearchSuggestionsSystemSingleFile {
	private class TrieNode {
		boolean end = false;
		String str = null;
		int count = 0;
		TrieNode[] children = new TrieNode[26];
	}

	private class Trie {
		TrieNode root = new TrieNode();

		private void insertWord(String product) {
			TrieNode node = root;
			for (char c : product.toCharArray()) {
				if (node.children[c - 'a'] == null) {
					node.children[c - 'a'] = new TrieNode();
				}
				node = node.children[c - 'a'];
			}
			if (node.end != true) {
				node.end = true;
				node.str = product;
			}
			node.count++;
		}

		public void insert(String[] products) {
			for (String product : products) {
				insertWord(product);
			}
		}

		private List<String> search(String pattern) {
			List<String> result = new ArrayList<>();
			TrieNode node = root;
			for (char c : pattern.toCharArray()) {
				if (node.children[c - 'a'] == null) {
					return result;
				}
				node = node.children[c - 'a'];
			}
			Solution(node, result);
			return result;
		}

		public List<List<String>> searchWord(String word) {
			List<List<String>> result = new ArrayList<>();
			for (int i = 1; i <= word.length(); i++) {
				result.add(search(word.substring(0, i)));
			}
			return result;
		}

		private void Solution(TrieNode root, List<String> result) {
			if (root.end) {
				for (int i = 0; i < root.count; i++) {
					result.add(root.str);
					if (result.size() == 3) {
						return;
					}
				}
			}
			for (TrieNode node : root.children) {
				if (node != null) {
					Solution(node, result);
				}
				if (result.size() == 3) {
					return;
				}
			}
		}
	}

	public List<List<String>> suggestedProducts(String[] products, String searchWord) {
		Trie trie = new Trie();
		trie.insert(products);
		return trie.searchWord(searchWord);
	}

	public static void main(String[] args) {
		M_1268_SearchSuggestionsSystemSingleFile test = new M_1268_SearchSuggestionsSystemSingleFile();

		String[] products = new String[] { "mobile", "mouse", "moneypot", "monitor", "mousepad" };
		String searchWord = "mouse";

		String[] products2 = new String[] { "havana" };
		String searchWord2 = "havana";

		String[] products3 = new String[] { "bags", "baggage", "banner", "box", "cloths" };
		String searchWord3 = "bags";

		String[] products4 = new String[] { "havana" };
		String searchWord4 = "tatiana";

		System.out.println(test.suggestedProducts(products, searchWord));
		System.out.println(test.suggestedProducts(products2, searchWord2));
		System.out.println(test.suggestedProducts(products3, searchWord3));
		System.out.println(test.suggestedProducts(products4, searchWord4));

	}

}
