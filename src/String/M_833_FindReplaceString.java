/**
 * 1.10 night created
 * https://leetcode.com/problems/find-and-replace-in-string/
 * 
 * 2.2 evening
 */
package String;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Tuple {
	int index;
	String source;
	String target;

	public Tuple(int i, String s, String t) {
		index = i;
		source = s;
		target = t;
	}
}

class MyTupleComparator implements Comparator<Tuple> {
	public int compare(Tuple a, Tuple b) {
		return a.index - b.index;
	}
}

public class M_833_FindReplaceString {

	/**
	 * Accepted --- 8ms 41.5 MB
	 * 
	 * https://juejin.im/post/5b8098b16fb9a01a1e02056f
	 */
	public String findReplaceString_HashMap_ArrayList(String S, int[] indexes, String[] sources, String[] targets) {
		Map<Integer, Integer> map = new HashMap<>();
		List<Integer> indList = new ArrayList<>();
		int n = indexes.length;
		for (int i = 0; i < n; i++) {
			map.put(indexes[i], i);
			indList.add(indexes[i]);
		}

		Collections.sort(indList);

		// 从右往左替换
		for (int i = n - 1; i >= 0; i--) {
			int pos = indList.get(i);
			int curInd = map.get(pos);

			// 检查sources[curInd]是否出现在指定pos，如果出现，则替换
			if (S.indexOf(sources[curInd], pos) == pos) {
				S = S.substring(0, pos) + targets[curInd] + S.substring(pos + sources[curInd].length());
			}
		}

		return S;
	}

	/**
	 * Accepted --- 1ms 38.2 MB 99.90%
	 * 
	 * http://buttercola.blogspot.com/2019/03/leetcode-833-find-and-replace-in-string.html
	 */
	public String findReplaceString_Tuple(String S, int[] indexes, String[] sources, String[] targets) {
		// create tuple and sort
		Tuple[] tuples = new Tuple[indexes.length];
		for (int i = 0; i < indexes.length; i++) {
			Tuple tuple = new Tuple(indexes[i], sources[i], targets[i]);
			tuples[i] = tuple;
		}

		Arrays.sort(tuples, new MyTupleComparator());

		int prev = 0;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < indexes.length; i++) {
			int index = tuples[i].index;
			String source = tuples[i].source;
			String target = tuples[i].target;

			if (S.substring(index, index + source.length()).equals(source)) {
				// prev seg
				sb.append(S.substring(prev, index));

				// curr seg
				sb.append(target);
				prev = index + source.length();
			}
		}

		sb.append(S.substring(prev));

		return sb.toString();
	}

	/**
	 * Accepted --- 1ms 38.6 MB 99.90%
	 * 
	 * http://buttercola.blogspot.com/2019/03/leetcode-833-find-and-replace-in-string.html
	 */
	public String findReplaceString_HashMap(String S, int[] indexes, String[] sources, String[] targets) {
		// Use hash map to store the indexes to be replaced
		Map<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < indexes.length; i++) {
			map.put(indexes[i], i);
		}

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < S.length(); i++) {
			if (map.containsKey(i)) {
				int index = map.get(i);
				String source = sources[index];
				String target = targets[index];

				if (i + source.length() <= S.length() && S.substring(i, i + source.length()).equals(source)) {
					sb.append(target);
					i += source.length() - 1;
				} else {
					sb.append(S.charAt(i));
				}
			} else {
				sb.append(S.charAt(i));
			}
		}

		return sb.toString();
	}

	/**
	 * Accepted --- 1ms 38.4 MB 99.90%
	 * 
	 * https://leetcode.com/articles/find-and-replace-in-string/
	 */
	public String findReplaceString_Direct(String S, int[] indexes, String[] sources, String[] targets) {
		int N = S.length();
		int[] match = new int[N];
		Arrays.fill(match, -1);

		for (int i = 0; i < indexes.length; ++i) {
			int ix = indexes[i];
			if (S.substring(ix, ix + sources[i].length()).equals(sources[i]))
				match[ix] = i;
		}

		StringBuilder ans = new StringBuilder();
		int ix = 0;
		while (ix < N) {
			if (match[ix] >= 0) {
				ans.append(targets[match[ix]]);
				ix += sources[match[ix]].length();
			} else {
				ans.append(S.charAt(ix++));
			}
		}
		return ans.toString();
	}

	public static void main(String[] args) {
		M_833_FindReplaceString test = new M_833_FindReplaceString();

		String S = "abcd";
		int[] indexes = new int[] { 0, 2 };
		String[] sources = new String[] { "a", "cd" };
		String[] targets = new String[] { "eee", "ffff" };

		String S2 = "abcd";
		String[] sources2 = new String[] { "ab", "ec" };

		System.out.println(test.findReplaceString_HashMap_ArrayList(S, indexes, sources, targets)); // "eeebffff"
		System.out.println(test.findReplaceString_HashMap_ArrayList(S2, indexes, sources2, targets)); // "eeecd"

		System.out.println("");
		System.out.println(test.findReplaceString_Tuple(S, indexes, sources, targets));
		System.out.println(test.findReplaceString_Tuple(S2, indexes, sources2, targets));

		System.out.println("");
		System.out.println(test.findReplaceString_HashMap(S, indexes, sources, targets));
		System.out.println(test.findReplaceString_HashMap(S2, indexes, sources2, targets));

		System.out.println("");
		System.out.println(test.findReplaceString_Direct(S, indexes, sources, targets));
		System.out.println(test.findReplaceString_Direct(S2, indexes, sources2, targets));

	}

}
