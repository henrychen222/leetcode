/**
 * Created 12.31 night  did 1/1/2020
 * https://leetcode.com/problems/smallest-subsequence-of-distinct-characters
 */
package String;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class M_1081_SmallestSubsequenceDistinctCharacters {

	/**
	 * Accepted --- 2ms, 34.6 MB 92.72%
	 * https://blog.csdn.net/qq_24342739/article/details/93127015
	 * https://www.twblogs.net/a/5d0bbf0dbd9eee1ede042413 (same)
	 */
	public String smallestSubsequence(String text) {
		int[] maxIndexes = new int[26];
		boolean[] used = new boolean[26];
		Stack<Character> stack = new Stack<Character>();

		for (int i = 0; i < text.length(); i++) {
			char ch = text.charAt(i);
			maxIndexes[ch - 'a'] = i;
		}

		for (int i = 0; i < text.length(); i++) {
			char ch = text.charAt(i);

			if (used[ch - 'a'] == false) {
				while (!stack.empty()) {
					char tmpCh = stack.peek(); // 取出末尾字符，不出栈
					if (maxIndexes[tmpCh - 'a'] > i && ch < tmpCh) {
						stack.pop(); // 末尾字符出栈
						used[tmpCh - 'a'] = false;
					} else {
						break;
					}
				}
				stack.push(ch);
				used[ch - 'a'] = true;
			}
		}

		StringBuilder sb = new StringBuilder();
		for (Character character : stack) {
			sb.append(character);
		}

		return sb.toString();
	}

	/**
	 * Accepted --- 2ms 34.5 MB 92.72%
	 * https://www.cnblogs.com/Dylan-Java-NYC/p/11976305.html
	 * http://www.manongjc.com/detail/14-crznhakcmdgqdsf.html (same)
	 */
	public String smallestSubsequence2(String text) {
		int[] last = new int[26];
		Stack<Character> stk = new Stack<>();
		boolean[] used = new boolean[26];

		for (int i = 0; i < text.length(); i++) {
			last[text.charAt(i) - 'a'] = i;
		}

		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			if (used[c - 'a']) {
				continue;
			}

			while (!stk.isEmpty() && stk.peek() > c && last[stk.peek() - 'a'] > i) {
				char top = stk.pop();
				used[top - 'a'] = false;
			}

			stk.push(c);
			used[c - 'a'] = true;
		}

		StringBuilder sb = new StringBuilder();
		for (char c : stk) {
			sb.append(c);
		}

		return sb.toString();
	}

	/**
	 * Accepted --- 8ms 34.8 MB 7.64%
	 * http://www.noteanddata.com/leetcode-1081-Smallest-Subsequence-of-Distinct-Characters-java-solution-note.html
	 */
	public String smallestSubsequence3(String text) {
		Map<Character, Integer> map = new HashMap<>();
		Set<Character> used = new HashSet<>();
		Stack<Character> stack = new Stack<>();

		for (char ch : text.toCharArray()) {
			map.put(ch, map.getOrDefault(ch, 0) + 1);
		}

		for (char ch : text.toCharArray()) {
			if (used.contains(ch)) {
				map.put(ch, map.get(ch) - 1);
				continue;
			}

			while (stack.size() > 0 && ch < stack.peek() && map.get(stack.peek()) > 1) {
				char peek = stack.pop();
				map.put(peek, map.get(peek) - 1);
				used.remove(peek);
			}
			stack.push(ch);
			used.add(ch);
		}

		StringBuilder sb = new StringBuilder();
		while (!stack.isEmpty()) {
			sb.append(stack.pop());
		}
		return sb.reverse().toString();
	}

	public static void main(String[] args) {
		M_1081_SmallestSubsequenceDistinctCharacters test = new M_1081_SmallestSubsequenceDistinctCharacters();

		String s1 = "cdadabcc";
		String s2 = "abcd";
		String s3 = "ecbacba";
		String s4 = "leetcode";

		System.out.println(test.smallestSubsequence(s1)); // "adbc"
		System.out.println(test.smallestSubsequence(s2)); // "abcd"
		System.out.println(test.smallestSubsequence(s3)); // "eacb"
		System.out.println(test.smallestSubsequence(s4)); // "letcod"

		System.out.println("\n" + test.smallestSubsequence2(s1));
		System.out.println(test.smallestSubsequence2(s2));
		System.out.println(test.smallestSubsequence2(s3));
		System.out.println(test.smallestSubsequence2(s4));

		System.out.println("\n" + test.smallestSubsequence3(s1));
		System.out.println(test.smallestSubsequence3(s2));
		System.out.println(test.smallestSubsequence3(s3));
		System.out.println(test.smallestSubsequence3(s4));

	}

}
