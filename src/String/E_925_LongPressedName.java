/**
 * 1.10 night created
 * https://leetcode.com/problems/long-pressed-name/
 * 
 * 1.12 evening
 */
package String;

import java.util.ArrayList;
import java.util.List;

public class E_925_LongPressedName {

	/**
	 * Accepted --- 0ms 35.1 MB 100.00%
	 * 
	 * https://leetcode.com/articles/long-pressed-name/
	 */
	public boolean isLongPressedName_twoPointers1(String name, String typed) {
		int j = 0;
		for (char c : name.toCharArray()) {
			if (j == typed.length())
				return false;

			// If mismatch...
			if (typed.charAt(j) != c) {
				// If it's the first char of the block, ans is false.
				if (j == 0 || typed.charAt(j - 1) != typed.charAt(j))
					return false;

				// Discard all similar chars
				char cur = typed.charAt(j);
				while (j < typed.length() && typed.charAt(j) == cur)
					j++;

				// If next isn't a match, ans is false.
				if (j == typed.length() || typed.charAt(j) != c)
					return false;
			}
			j++;
		}
		return true;
	}

	/**
	 * Accepted --- 1ms 35.2 MB 51.50%
	 * 
	 * https://github.com/junyu0577/DataStructureAndAlgorithm/blob/master/Solution/src/com/github/junyu/solution/leetCode/easy/string/_925_Long_Pressed_Name.java
	 */
	public boolean isLongPressedName_twoPointers2(String name, String typed) {
		if (name == null || typed == null)
			return false;
		if ("".equals(name) || "".equals(typed))
			return false;
		int p1 = 0;
		int p2 = 0;
		char lastChar = name.charAt(p1);
		while (p1 < name.length() && p2 < typed.length()) {
			if (name.charAt(p1) == typed.charAt(p2)) {
				lastChar = name.charAt(p1);
				p1++;
				p2++;
			} else if ((typed.charAt(p2) == lastChar)) {
				p2++;
			} else {
				return false;
			}
		}
		if (p1 < name.length()) {
			return false;
		}
		return true;
	}

	/**
	 * Accepted --- 0ms 34.8 MB 100.00%
	 * 
	 * http://codingbydumbbell.blogspot.com/2019/02/leetcode-925-long-pressed-name.html
	 */
	public boolean isLongPressedName_twoPointers3(String name, String typed) {
		// 若第一個字元不符，則排除
		if (name.charAt(0) != typed.charAt(0))
			return false;
		// 若最後一個字元不符，則排除
		if (name.charAt(name.length() - 1) != typed.charAt(typed.length() - 1))
			return false;
		int i = 0;
		for (char c : typed.toCharArray()) {
			if (i < name.length()) {
				if (c != name.charAt(i)) {
					if (c == name.charAt(i - 1))
						continue; // 代表是長案的產物
					else
						return false;
				} else
					i++; // 代表字元相同
			} else { // 代表已經判斷最後一字元
				if (c != name.charAt(i - 1))
					return false;
			}
		}
		return true;
	}

	/**
	 * Accepted --- 0ms 35 MB 100.00%
	 *
	 * http://www.noteanddata.com/leetcode-925-Long-Pressed-Name-solution-note.html
	 */
	public boolean isLongPressedName_twoPointers4(String name, String typed) {
		int i = 0, j = 0; // two pointer
		while (i < name.length() && j < typed.length()) {
			if (name.charAt(i) == typed.charAt(j)) {
				i++;
				j++;
			} else {
				if (j == 0 || typed.charAt(j) != typed.charAt(j - 1))
					return false;
				j++;
			}
		}
		if (i != name.length())
			return false;
		while (j < typed.length()) {
			if (typed.charAt(j) != typed.charAt(j - 1))
				return false;
			j++;
		}
		return true;
	}

	/**
	 * Accepted --- 0ms 34.7 MB 100.00%
	 * 
	 * https://gist.github.com/karlbishnu/4bb0544e695096b79fb9c3794d8dffb1
	 */
	public boolean isLongPressedName_twoPointers5(String name, String typed) {
		int i = 0, j = 0; // two pointer
		int count_t = 0;
		int count_n = 0;
		for (; i < name.length() && j < typed.length(); i += count_n, j += count_t) {
			char n = name.charAt(i);
			char t = typed.charAt(j);
			count_t = count(typed, j, t);
			count_n = count(name, i, n);
			if (t != n || count_t < count_n)
				return false;
		}
		return i == name.length() && j == typed.length();
	}

	private int count(String s, int i, char c) {
		int res = 0;
		while (i < s.length() && s.charAt(i++) == c) {
			res++;
		}
		return res;
	}

	/**
	 * Accepted --- 1ms 35.3 MB 51.50%
	 * 
	 * https://leetcode.com/articles/long-pressed-name/
	 */
	public boolean isLongPressedName_GroupIntoBlocks(String name, String typed) {
		Group g1 = groupify(name);
		Group g2 = groupify(typed);
		if (!g1.key.equals(g2.key))
			return false;

		for (int i = 0; i < g1.count.size(); ++i)
			if (g1.count.get(i) > g2.count.get(i))
				return false;
		return true;
	}

	public Group groupify(String S) {
		StringBuilder key = new StringBuilder();
		List<Integer> count = new ArrayList<Integer>();
		int anchor = 0;
		int N = S.length();
		for (int i = 0; i < N; ++i) {
			if (i == N - 1 || S.charAt(i) != S.charAt(i + 1)) { // end of group
				key.append(S.charAt(i));
				count.add(i - anchor + 1);
				anchor = i + 1;
			}
		}

		return new Group(key.toString(), count);
	}

	class Group {
		String key;
		List<Integer> count;

		Group(String k, List<Integer> c) {
			key = k;
			count = c;
		}
	}

	public static void main(String[] args) {
		E_925_LongPressedName test = new E_925_LongPressedName();

		String name = "alex", typed = "aaleex";
		String name2 = "saeed", typed2 = "ssaaedd";
		String name3 = "leelee", typed3 = "lleeelee";
		String name4 = "laiden", typed4 = "laiden";

		System.out.println(test.isLongPressedName_twoPointers1(name, typed)); // true
		System.out.println(test.isLongPressedName_twoPointers1(name2, typed2)); // false
		System.out.println(test.isLongPressedName_twoPointers1(name3, typed3)); // true
		System.out.println(test.isLongPressedName_twoPointers1(name4, typed4)); // true

		System.out.println("");
		System.out.println(test.isLongPressedName_twoPointers2(name, typed));
		System.out.println(test.isLongPressedName_twoPointers2(name2, typed2));
		System.out.println(test.isLongPressedName_twoPointers2(name3, typed3));
		System.out.println(test.isLongPressedName_twoPointers2(name4, typed4));

		System.out.println("");
		System.out.println(test.isLongPressedName_twoPointers3(name, typed));
		System.out.println(test.isLongPressedName_twoPointers3(name2, typed2));
		System.out.println(test.isLongPressedName_twoPointers3(name3, typed3));
		System.out.println(test.isLongPressedName_twoPointers3(name4, typed4));

		System.out.println("");
		System.out.println(test.isLongPressedName_twoPointers4(name, typed));
		System.out.println(test.isLongPressedName_twoPointers4(name2, typed2));
		System.out.println(test.isLongPressedName_twoPointers4(name3, typed3));
		System.out.println(test.isLongPressedName_twoPointers4(name4, typed4));

		System.out.println("");
		System.out.println(test.isLongPressedName_twoPointers5(name, typed));
		System.out.println(test.isLongPressedName_twoPointers5(name2, typed2));
		System.out.println(test.isLongPressedName_twoPointers5(name3, typed3));
		System.out.println(test.isLongPressedName_twoPointers5(name4, typed4));

		System.out.println("");
		System.out.println(test.isLongPressedName_GroupIntoBlocks(name, typed));
		System.out.println(test.isLongPressedName_GroupIntoBlocks(name2, typed2));
		System.out.println(test.isLongPressedName_GroupIntoBlocks(name3, typed3));
		System.out.println(test.isLongPressedName_GroupIntoBlocks(name4, typed4));

	}

}
