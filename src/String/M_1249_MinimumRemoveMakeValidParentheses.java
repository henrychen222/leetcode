/**
 * 12.27 evening 
 * https://leetcode.com/problems/minimum-remove-to-make-valid-parentheses/
 */

package String;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Stack;

public class M_1249_MinimumRemoveMakeValidParentheses {

	// // Wrong
	// public String minRemoveToMakeValid2(String s) {
	// int j = 0;
	// StringBuilder sb = new StringBuilder(s);
	// for (int i = 0; i < s.length(); i++) {
	// boolean found = false;
	// if (sb.charAt(i) == '(') {
	// j = i + 1;
	// // for (; j < s.length() - 1 && sb.charAt(j) != ')'; j++) {
	// // }
	// while (j < s.length() - 1 && sb.charAt(j) != ')') {
	// j++;
	// }
	// if (j <= s.length() - 1 && sb.charAt(j) == ')') {
	// sb.setCharAt(j, ',');
	// found = true;
	// }
	// if (!found) {
	// sb.setCharAt(i, ',');
	// }
	// } else if (sb.charAt(i) == ')') {
	// sb.setCharAt(i, ',');
	//
	// }
	// }
	//
	// String result = sb.toString();
	// result = result.replaceAll("\\.", "");
	// result = result.replaceAll("\\,", ")");
	// return result;
	// }

	// Accepted 15ms, 37.6MB, 83.76%
	public String minRemoveToMakeValid(String s) {
		if (s.length() == 0 || s == null) {
			return s;
		}

		StringBuilder sb = new StringBuilder();
		int left = 0;
		for (char c : s.toCharArray()) {
			if (c == '(') {
				left++;
			} else if (c == ')') {
				if (left == 0) {
					continue;
				}
				left--;
			}
			sb.append(c);
		}

		StringBuilder result = new StringBuilder();
		for (int i = sb.length() - 1; i >= 0; i--) {
			char c = sb.charAt(i);
			if (c == '(' && left-- > 0) {
				continue;
			}
			result.append(c);
		}
		return result.reverse().toString();
	}

	// Accepted 29ms, 38.5MB, 50.00%
	public String minRemoveToMakeValid_Stack(String s) {
		boolean[] wrong = new boolean[s.length()]; // 应被删除的位
		Stack<Integer> stack = new Stack<>(); // 记录正括号出现的下标

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c == '(') {
				stack.push(i);
			} else if (c == ')') {
				// 如果Stack中有正括号, pop出该正括号, 说明当前反括号与pop出的正括号是有效的
				if (stack.size() > 0) {
					stack.pop();
				}
				// 如果Stack中没有正括号, 当前反括号是多余的，应被删除
				else {
					wrong[i] = true;
				}
			}
		}

		// stack中剩余的正括号是多余的
		while (stack.size() > 0) {
			wrong[stack.pop()] = true;
		}

		StringBuilder result = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			// 所有不需要被删除的字符可以组成返回结果
			if (!wrong[i]) {
				result.append(s.charAt(i));
			}
		}
		return result.toString();
	}

	public static void main(String[] args) {

		M_1249_MinimumRemoveMakeValidParentheses test = new M_1249_MinimumRemoveMakeValidParentheses();
		String s1 = "lee(t(c)o)de)";
		String s2 = "a)b(c)d";
		String s3 = "))((";
		String s4 = "(a(b(c)d)";

		// System.out.println(test.minRemoveToMakeValid2(s1)); // "lee(t(co)de)" OR "lee(t(c)o)de"
		// System.out.println(test.minRemoveToMakeValid2(s2)); // "ab(c)d"
		// System.out.println(test.minRemoveToMakeValid2(s3)); // ""
		// System.out.println(test.minRemoveToMakeValid2(s4)); // "(a(bc)d)" OR "a(b(c)d)"

		System.out.println(test.minRemoveToMakeValid(s1)); // "lee(t(co)de)" OR "lee(t(c)o)de"
		System.out.println(test.minRemoveToMakeValid(s2)); // "ab(c)d"
		System.out.println(test.minRemoveToMakeValid(s3)); // ""
		System.out.println(test.minRemoveToMakeValid(s4)); // "(a(bc)d)" OR "a(b(c)d)"
		assertEquals(test.minRemoveToMakeValid(s1), "lee(t(c)o)de");
		assertEquals(test.minRemoveToMakeValid(s2), "ab(c)d");
		assertEquals(test.minRemoveToMakeValid(s3), "");
		assertEquals(test.minRemoveToMakeValid(s4), "(a(bc)d)");

		System.out.println("\n\n");

		System.out.println(test.minRemoveToMakeValid_Stack(s1)); // "lee(t(co)de)" OR "lee(t(c)o)de"
		System.out.println(test.minRemoveToMakeValid_Stack(s2)); // "ab(c)d"
		System.out.println(test.minRemoveToMakeValid_Stack(s3)); // ""
		System.out.println(test.minRemoveToMakeValid_Stack(s4)); // "(a(bc)d)" OR "a(b(c)d)"
		assertEquals(test.minRemoveToMakeValid_Stack(s1), "lee(t(c)o)de");
		assertEquals(test.minRemoveToMakeValid_Stack(s2), "ab(c)d");
		assertEquals(test.minRemoveToMakeValid_Stack(s3), "");
		assertEquals(test.minRemoveToMakeValid_Stack(s4), "a(b(c)d)");

	}

}
