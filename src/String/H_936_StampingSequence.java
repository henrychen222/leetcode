/**
 * 1.10 night created
 * https://leetcode.com/problems/stamping-the-sequence/
 * 
 * 1.12 afternoon
 */
package String;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

public class H_936_StampingSequence {

	/**
	 * Accepted --- 35ms 38 MB 28.10%
	 * 
	 * http://www.noteanddata.com/leetcode-936-Stamping-The-Sequence-java-solution-note.html
	 */
	public int[] movesToStamp(String stamp, String target) {
		boolean[] visited = new boolean[target.length()];
		char[] arr = target.toCharArray();
		List<Integer> list = new ArrayList<>();
		int matchCount = 0;
		while (matchCount != target.length()) {
			boolean matched = false;
			for (int i = 0; i < arr.length && !matched; ++i) {
				if (visited[i])
					continue;
				if (match(arr, i, stamp)) {
					matched = true;
					for (int j = 0; j < stamp.length(); ++j) {
						if (arr[i + j] != '*') {
							matchCount++;
							arr[i + j] = '*';
						}
					}
					visited[i] = true;
					list.add(i);
				}

			}
			if (!matched) {
				break;
			}
		}

		if (matchCount != target.length())
			return new int[] {};

		int[] ret = new int[list.size()];
		for (int i = 0; i < list.size(); ++i) {
			ret[i] = list.get(list.size() - i - 1);
		}
		return ret;
	}

	public boolean match(char[] arr, int index, String stamp) {
		int i = 0;
		for (; i < stamp.length() && i + index < arr.length; ++i) {
			if (arr[i + index] != stamp.charAt(i) && arr[i + index] != '*') {
				return false;
			}
		}
		return i == stamp.length();
	}

	/**
	 * Accepted --- 15ms 38.6 MB 35.95%
	 * 
	 * Reverse Simulation
	 * 
	 * https://github.com/cherryljr/LeetCode/blob/master/Stamping%20The%20Sequence.java
	 * https://massivealgorithms.blogspot.com/2019/03/leetcode-936-stamping-sequence.html
	 */
	String myTarget = null; // 因为需要在 doStamp 中对 target 进行修改，所以需要一个成员变量以便访问（Java中字符串类型的限制）

	public int[] movesToStamp_reverse_simulation(String stamp, String target) {
		int M = stamp.length(), N = target.length();
		myTarget = target;
		int count = 0; // 替换的字符个数
		boolean[] visited = new boolean[N]; // 用于记录哪些位置开始的字符被修改过
		List<Integer> index = new ArrayList<>(); // 用于记录每次修改的起始位置
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < M; i++) {
			sb.append('*');
		}
		// 事先构造出用于替换的字符串
		String replace = sb.toString();

		while (count < N) {
			boolean found = false;
			for (int i = 0; i <= N - M; i++) {
				if (visited[i]) {
					continue;
				}
				int len = doStamp(stamp, i, replace);
				if (len == 0) {
					continue;
				}
				visited[i] = true;
				count += len;
				index.add(i);
				found = true;
			}
			if (!found) {
				return new int[] {};
			}
		}

		int size = index.size();
		int[] rst = new int[size--];
		for (int i = 0; i <= size; i++) {
			rst[i] = index.get(size - i);
		}
		return rst;
	}

	// 对从 start 位置开始的字串进行stamp操作，返回替换的字符个数
	// 如果无法匹配的话返回0
	private int doStamp(String stamp, int start, String replace) {
		int changed = stamp.length();
		for (int i = 0; i < stamp.length(); i++) {
			if (myTarget.charAt(start + i) == '*') {
				changed--;
			} else if (myTarget.charAt(start + i) != stamp.charAt(i)) {
				return 0;
			}
		}

		if (changed != 0) {
			myTarget = myTarget.substring(0, start) + replace + myTarget.substring(start + stamp.length());
		}
		return changed;
	}

	/**
	 * Accepted --- 3ms 37.2 MB 100%
	 * 
	 * Greedy (best)
	 * 
	 * https://massivealgorithms.blogspot.com/2019/03/leetcode-936-stamping-sequence.html
	 * https://leetcode.com/problems/stamping-the-sequence/discuss/201546/12ms-Java-Solution-Beats-100
	 */
	public int[] movesToStamp_Greedy1(String stamp, String target) {
		char[] S = stamp.toCharArray();
		char[] T = target.toCharArray();
		List<Integer> res = new ArrayList<>();
		boolean[] visited = new boolean[T.length];
		int stars = 0;

		while (stars < T.length) {
			boolean doneReplace = false;
			for (int i = 0; i <= T.length - S.length; i++) {
				if (!visited[i] && canReplace(T, i, S)) {
					stars = doReplace(T, i, S.length, stars);
					doneReplace = true;
					visited[i] = true;
					res.add(i);
					if (stars == T.length) {
						break;
					}
				}
			}

			if (!doneReplace) {
				return new int[0];
			}
		}

		int[] resArray = new int[res.size()];
		for (int i = 0; i < res.size(); i++) {
			resArray[i] = res.get(res.size() - i - 1);
		}
		return resArray;
	}

	// is used to check if any substring from Target is existing to be replaced with
	// *
	private boolean canReplace(char[] T, int p, char[] S) {
		for (int i = 0; i < S.length; i++) {
			if (T[i + p] != '*' && T[i + p] != S[i]) {
				return false;
			}
		}
		return true;
	}

	// used to replace the substring with * and return the total number of * we have
	// now
	private int doReplace(char[] T, int p, int len, int count) {
		for (int i = 0; i < len; i++) {
			if (T[i + p] != '*') {
				T[i + p] = '*';
				count++;
			}
		}
		return count;
	}

	/**
	 * Accepted --- 43ms 37.4 MB 23.53%
	 * 
	 * https://irisjavadiary.blogspot.com/2018/11/936-stamping-sequence.html
	 */
	private int matchedCount;

	public int[] movesToStamp_Greedy2(String stamp, String target) {
		char[] chars = target.toCharArray();
		matchedCount = 0;
		List<Integer> result = new LinkedList<>();
		while (matchedCount != target.length()) {
			if (!match(stamp, target, chars, result))
				break;
		}
		return matchedCount == target.length() ? result.stream().mapToInt(a -> a).toArray() : new int[0];
	}

	private boolean match(String stamp, String target, char[] chars, List<Integer> result) {
		for (int i = 0; i <= target.length() - stamp.length(); i++) {
			int j = 0;
			boolean canMatch = false;
			for (j = 0; j < stamp.length(); j++) {
				if (chars[i + j] == stamp.charAt(j)) {
					canMatch = true;
				} else if (chars[i + j] != '?') {
					break;
				}
			}
			if (canMatch && j == stamp.length()) {
				for (j = 0; j < stamp.length(); j++) {
					if (chars[i + j] != '?') {
						chars[i + j] = '?';
						matchedCount++;
					}
				}
				result.add(0, i);
				return true;
			}
		}
		return false;
	}

	/**
	 * Accepted --- 56ms 44 MB 12.42%
	 * 
	 * Work Backwards
	 * 
	 * https://leetcode.com/articles/stamping-the-sequence/
	 * https://massivealgorithms.blogspot.com/2019/03/leetcode-936-stamping-sequence.html
	 */
	public int[] movesToStamp_workBackwards(String stamp, String target) {
		int M = stamp.length(), N = target.length();
		Queue<Integer> queue = new ArrayDeque<Integer>();
		boolean[] done = new boolean[N];
		Stack<Integer> ans = new Stack<Integer>();
		List<Node> A = new ArrayList<Node>();

		for (int i = 0; i <= N - M; ++i) {
			// For each window [i, i+M), A[i] will contain
			// info on what needs to change before we can
			// reverse stamp at this window.

			Set<Integer> made = new HashSet<Integer>();
			Set<Integer> todo = new HashSet<Integer>();
			for (int j = 0; j < M; ++j) {
				if (target.charAt(i + j) == stamp.charAt(j))
					made.add(i + j);
				else
					todo.add(i + j);
			}

			A.add(new Node(made, todo));

			// If we can reverse stamp at i immediately,
			// enqueue letters from this window.
			if (todo.isEmpty()) {
				ans.push(i);
				for (int j = i; j < i + M; ++j)
					if (!done[j]) {
						queue.add(j);
						done[j] = true;
					}
			}
		}

		// For each enqueued letter (position),
		while (!queue.isEmpty()) {
			int i = queue.poll();

			// For each window that is potentially affected,
			// j: start of window
			for (int j = Math.max(0, i - M + 1); j <= Math.min(N - M, i); ++j) {
				if (A.get(j).todo.contains(i)) { // This window is affected
					A.get(j).todo.remove(i);
					if (A.get(j).todo.isEmpty()) {
						ans.push(j);
						for (int m : A.get(j).made)
							if (!done[m]) {
								queue.add(m);
								done[m] = true;
							}
					}
				}
			}
		}

		for (boolean b : done)
			if (!b)
				return new int[0];

		int[] ret = new int[ans.size()];
		int t = 0;
		while (!ans.isEmpty())
			ret[t++] = ans.pop();

		return ret;
	}

	class Node {
		Set<Integer> made, todo;

		Node(Set<Integer> m, Set<Integer> t) {
			made = m;
			todo = t;
		}
	}

	/**
	 * Wrong Answer
	 * 
	 * TLE(Two-line element set) BFS
	 * 
	 * https://irisjavadiary.blogspot.com/2018/11/936-stamping-sequence.html
	 */
	public int[] movesToStamp_TLE_BFS(String stamp, String target) {
		String mask = "";
		String result = "";
		int stampLen = stamp.length();
		for (int i = 0; i < stampLen; i++)
			mask += "?";
		for (int i = 0; i < target.length(); i++)
			result += "?";
		// key-string after stamped like ababc -> ab???, value-list of indexed where we
		// have put stamps
		Map<String, List<Integer>> map = new HashMap<>();
		// string[0] places we can still put stamp, string[1] target
		Queue<String> que = new LinkedList<>();
		que.offer(target);
		int step = 0;
		while (!que.isEmpty()) {
			step++;
			if (step > 10)
				return new int[0];
			int size = que.size();
			while (size-- > 0) {
				String curr = que.poll();
				List<Integer> stamped = map.getOrDefault(curr, new ArrayList<>());
				// ab???, [2]
				for (int i = 0; i <= target.length() - stampLen; i++) {
					if (stamped.contains(i))
						continue; // skip if we have already stamped on this position
					int j = 0;
					boolean valid = false;
					for (j = 0; j < stampLen; j++) {
						if (stamp.charAt(j) == target.charAt(i + j)) {
							valid = true; // this stamp contributes to our result
						} else if (curr.charAt(i + j) != '?') {
							break;
						}
					}
					if (valid && j == stampLen) { // we can stamp here
						String next = curr.substring(0, i) + mask + curr.substring(i + stampLen);
						List<Integer> nextStamped = new LinkedList<>(stamped);
						nextStamped.add(0, i);
						if (next.equals(result))
							return nextStamped.stream().mapToInt(a -> a).toArray();
						if (!map.containsKey(next)) {
							map.put(next, nextStamped);
							que.offer(next);
						}
					}
				}
			}
		}
		return new int[0];
	}

	/**
	 * Accepted --- 184ms 51.4 MB 5.23%
	 * 
	 * https://irisjavadiary.blogspot.com/2018/11/936-stamping-sequence.html
	 */
	public int[] movesToStamp_similar_topological_sort(String stamp, String target) {
		Deque<Integer> result = new ArrayDeque<>();
		int stampLen = stamp.length();
		int targetLen = target.length();
		Map<Integer, Set<Integer>> todo = new HashMap<>();
		Queue<Integer> done = new LinkedList<>();
		for (int i = 0; i <= targetLen - stampLen; i++) {
			Set<Integer> todoIndex = new HashSet<>();
			for (int j = 0; j < stampLen; j++) {
				if (stamp.charAt(j) != target.charAt(i + j)) {
					todoIndex.add(i + j);
				}
			}
			if (todoIndex.isEmpty()) {
				done.offer(i);
			} else {
				todo.put(i, todoIndex);
			}
		}
		boolean[] visited = new boolean[targetLen];
		while (!done.isEmpty()) {
			// p + stampLen = i
			// p = i - stampLen + 1
			// i (i+stampLen)

			// abca
			// aabcaca
			int curr = done.poll();
			result.addFirst(curr);
			for (int changedPos = curr; changedPos < curr + stampLen; changedPos++) {
				visited[changedPos] = true;

				// check all stamp that might contain changedPos
				for (int stampIndex = Math.max(0, curr - stampLen + 1); stampIndex < Math.min(curr + stampLen,
						targetLen); stampIndex++) {
					Set<Integer> todoIndex = null;
					if ((todoIndex = todo.get(stampIndex)) != null) {
						if (todoIndex.contains(changedPos)) {
							todoIndex.remove(changedPos);
							if (todoIndex.size() == 0) {
								todo.remove(stampIndex);
								for (int p = 0; p < stampLen; p++) {

									if (!visited[stampIndex + p]) {
										done.offer(stampIndex);
										break;
									}
								}
							}
						}
					}

				}
			}
		}
		for (boolean visit : visited) {
			if (!visit) {
				return new int[0];
			}
		}
		return result.stream().mapToInt(i -> i).toArray();
	}

	public static void main(String[] args) {
		H_936_StampingSequence test = new H_936_StampingSequence();

		String stamp = "abc", target = "ababc";
		String stamp2 = "abca", target2 = "aabcaca";
		String stamp3 = "k", target3 = "kkkkkkkkkkkkkkk";

		// third
		System.out.println(Arrays.toString(test.movesToStamp(stamp, target))); // [0,2] OR [1,0,2]
		System.out.println(Arrays.toString(test.movesToStamp(stamp2, target2))); // [3,0,1] OR [0,3,1] OR [2,3,0,1]
		System.out.println(Arrays.toString(test.movesToStamp(stamp3, target3))); // [14,13,12,11,10,9,8,7,6,5,4,3,2,1,0]

		// second best
		System.out.println("");
		System.out.println(Arrays.toString(test.movesToStamp_reverse_simulation(stamp, target)));
		System.out.println(Arrays.toString(test.movesToStamp_reverse_simulation(stamp2, target2)));
		System.out.println(Arrays.toString(test.movesToStamp_reverse_simulation(stamp3, target3)));

		// best
		System.out.println("");
		System.out.println(Arrays.toString(test.movesToStamp_Greedy1(stamp, target)));
		System.out.println(Arrays.toString(test.movesToStamp_Greedy1(stamp2, target2)));
		System.out.println(Arrays.toString(test.movesToStamp_Greedy1(stamp3, target3)));

		// fourth
		System.out.println("");
		System.out.println(Arrays.toString(test.movesToStamp_Greedy2(stamp, target)));
		System.out.println(Arrays.toString(test.movesToStamp_Greedy2(stamp2, target2)));
		System.out.println(Arrays.toString(test.movesToStamp_Greedy2(stamp3, target3)));

		// fifth
		System.out.println("");
		System.out.println(Arrays.toString(test.movesToStamp_workBackwards(stamp, target)));
		System.out.println(Arrays.toString(test.movesToStamp_workBackwards(stamp2, target2)));
		System.out.println(Arrays.toString(test.movesToStamp_workBackwards(stamp3, target3)));

		// Wrong
		System.out.println("");
		System.out.println(Arrays.toString(test.movesToStamp_TLE_BFS(stamp, target)));
		System.out.println(Arrays.toString(test.movesToStamp_TLE_BFS(stamp2, target2)));
		System.out.println(Arrays.toString(test.movesToStamp_TLE_BFS(stamp3, target3))); // Wrong here

		// sixth
		System.out.println("");
		System.out.println(Arrays.toString(test.movesToStamp_similar_topological_sort(stamp, target)));
		System.out.println(Arrays.toString(test.movesToStamp_similar_topological_sort(stamp2, target2)));
		System.out.println(Arrays.toString(test.movesToStamp_similar_topological_sort(stamp3, target3)));

	}

}
