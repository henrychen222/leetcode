/**
 * 1.4 night
 * https://leetcode.com/problems/compare-strings-by-frequency-of-the-smallest-character/
 */
package String;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class E_1170_CompareStringsFrequencySmallestCharacter {

	/**
	 * Accepted --- 2ms 37.9 MB 93.13% https://kknews.cc/code/vlrg6qq.html
	 */
	public int[] numSmallerByFrequency(String[] queries, String[] words) {
		int[] result = new int[queries.length];
		int[] count = new int[11];
		for (String w : words) {
			count[func(w) - 1]++;
		}
		for (int i = 9; i >= 0; i--) {
			count[i] += count[i + 1];
		}
		for (int i = 0; i < result.length; i++) {
			result[i] = count[func(queries[i])];
		}
		return result;
	}

	private int func(String s) {
		int[] count = new int[26];
		for (char c : s.toCharArray()) {
			count[c - 'a']++;
		}
		for (int i : count) {
			if (i > 0) {
				return i;
			}
		}
		return 0;
	}

	/**
	 * Accepted --- 6ms 37.7 MB 55.15%
	 * https://github.com/varunu28/LeetCode-Java-Solutions/blob/master/Easy/Compare%20Strings%20by%20Frequency%20of%20the%20Smallest%20Character.java
	 */
	public int[] numSmallerByFrequency2(String[] queries, String[] words) {
		List<Integer> list = new ArrayList<>();
		for (String word : words) {
			list.add(getMinWordFreq(word));
		}
		Collections.sort(list);
		int[] ans = new int[queries.length];
		for (int i = 0; i < queries.length; i++) {
			int minCount = getMinWordFreq(queries[i]);
			int idx = 0;
			ans[i] = binarySearch(minCount, list);
		}
		return ans;
	}

	private int binarySearch(int target, List<Integer> list) {
		int left = 0;
		int right = list.size() - 1;
		while (left <= right) {
			int mid = (left + right) / 2;
			if (list.get(mid) <= target) {
				left = mid + 1;
			} else {
				right = mid - 1;
			}
		}

		return list.size() - left;
	}

	private int getMinWordFreq(String word) {
		int[] counter = new int[26];
		for (char c : word.toCharArray()) {
			counter[c - 'a']++;
		}
		for (int i = 0; i < 26; i++) {
			if (counter[i] != 0) {
				return counter[i];
			}
		}
		return 0;
	}

	public static void main(String[] args) {
		E_1170_CompareStringsFrequencySmallestCharacter test = new E_1170_CompareStringsFrequencySmallestCharacter();

		String[] queries = new String[] { "cbd" };
		String[] words = new String[] { "zaaaz" };

		String[] queries2 = new String[] { "bbb", "cc" };
		String[] words2 = new String[] { "a", "aa", "aaa", "aaaa" };

		System.out.println(Arrays.toString(test.numSmallerByFrequency(queries, words))); // [1]
		System.out.println(Arrays.toString(test.numSmallerByFrequency(queries2, words2))); // [1,2]
		
		System.out.println("");
		System.out.println(Arrays.toString(test.numSmallerByFrequency2(queries, words))); // [1]
		System.out.println(Arrays.toString(test.numSmallerByFrequency2(queries2, words2))); // [1,2]
	}

}
