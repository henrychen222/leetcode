/**
 * 1.10 night created
 * https://leetcode.com/problems/masking-personal-information/
 * 
 * 2.2 evening
 */
package String;

public class M_831_MaskingPersonal_Information {

	/**
	 * Accepted --- 6ms 38.3 MB 21.77%
	 * 
	 * https://blog.csdn.net/androidchanhao/article/details/81535602
	 * https://juejin.im/post/5b6bd8bee51d45190869bf87 (same)
	 */
	private String prefix[] = { "", "+*-", "+**-", "+***-" }; // 电话的前缀

	public String maskPII(String S) {
		int at = S.indexOf('@');
		if (at != -1) {
			S = S.toLowerCase();
			S = S.substring(0, 1) + "*****" + S.substring(at - 1);
		} else {
			S = S.replaceAll("[^0-9]", "");// 去掉非数字字符
			int n = S.length();
			S = prefix[n - 10] + "***-***-" + S.substring(n - 4);
		}
		return S;
	}

	/**
	 * Accepted --- 0ms 36.9 MB 100.00%
	 * 
	 * https://gist.github.com/BiruLyu/eac277e6651aae5423c4d538cfb88dc4
	 */
	public String maskPII2(String S) {
		StringBuilder res = new StringBuilder();
		int idx = S.indexOf('@');
		int len = S.length();
		if (idx > 0) {
			res.append(Character.toLowerCase(S.charAt(0)));
			res.append("*****");
			res.append(S.substring(idx - 1).toLowerCase());
		} else {
			int lastFour = 0;
			for (int i = len - 1; i >= 0; i--) {
				if (Character.isDigit(S.charAt(i))) {
					if (lastFour < 4) {
						res.insert(0, S.charAt(i));
						lastFour++;
					} else {
						res.insert(0, '*');
					}
				}
			}
			int cntOfChar = res.length();
			if (cntOfChar - 10 > 0) {
				res.insert(0, '+');
				cntOfChar += 1;
				res.insert(cntOfChar - 10, '-');
				cntOfChar += 1;
			}
			res.insert(cntOfChar - 4, '-');
			res.insert(cntOfChar - 7, '-');
		}
		return res.toString();
	}

	public static void main(String[] args) {

		M_831_MaskingPersonal_Information test = new M_831_MaskingPersonal_Information();
		String email = "LeetCode@LeetCode.com";
		String email2 = "AB@qq.com";
		String phone = "1(234)567-890";
		String phone2 = "86-(10)12345678";

		System.out.println(test.maskPII(email)); // "l*****e@leetcode.com"
		System.out.println(test.maskPII(email2)); // "a*****b@qq.com"
		System.out.println(test.maskPII(phone)); // "***-***-7890"
		System.out.println(test.maskPII(phone2)); // "+**-***-***-5678"

		System.out.println("");
		System.out.println(test.maskPII(email));
		System.out.println(test.maskPII(email2));
		System.out.println(test.maskPII(phone));
		System.out.println(test.maskPII(phone2));

	}

}
