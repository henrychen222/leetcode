/**
 * 1.3 night 2020
 * https://leetcode.com/problems/defanging-an-ip-address/
 */
package String;

public class E_1108_DefangingIPAddress {

	/**
	 * Accepted --- 2ms 34.3 MB 46.98%
	 * https://github.com/Maecenas/LeetCode/blob/master/src/main/java/LeetCode/_1108_DefangingAnIPAddress.java
	 */
	public String defangIPaddr(String address) {
		return address.replace(".", "[.]");
	}

	/**
	 * Accepted --- 2ms 34.4 MB 46.98%
	 * http://www.noteanddata.com/leetcode-1108-Defanging-an-IP-Address-java-go-solution-note.html
	 */
	public String defangIPaddr2(String address) {
		return address.replaceAll("\\.", "[.]");
	}

	/**
	 * Accepted --- 1ms 34.2 MB 53.98%
	 * https://blog.csdn.net/YuanTheCoder/article/details/95253091
	 */
	public String defangIPaddr3(String address) {
		String[] stringSplit = address.split("\\.");
		String defang = "[.]";
		String ans = "";
		int len = stringSplit.length;
		for (int i = 0; i < len - 1; i++) {
			ans += stringSplit[i];
			ans += defang;
		}
		ans += stringSplit[len - 1];
		return ans;
	}

	/**
	 * Accepted --- 0ms 34.2 MB 100%
	 * https://blog.csdn.net/YuanTheCoder/article/details/95253091
	 * https://www.cnblogs.com/Dylan-Java-NYC/p/12078982.html
	 */
	public String defangIPaddr4(String address) {
		if (address == null || address.length() == 0) {
			return address;
		}

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < address.length(); i++) {
			if (address.charAt(i) == '.') {
				sb.append("[.]");
			} else {
				sb.append(address.charAt(i));
			}
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		E_1108_DefangingIPAddress test = new E_1108_DefangingIPAddress();

		String address = "1.1.1.1";
		String address2 = "255.100.50.0";

		System.out.println(test.defangIPaddr(address)); // "1[.]1[.]1[.]1"
		System.out.println(test.defangIPaddr(address2)); // "255[.]100[.]50[.]0"

		System.out.println(test.defangIPaddr2(address));
		System.out.println(test.defangIPaddr2(address2));

		System.out.println(test.defangIPaddr3(address));
		System.out.println(test.defangIPaddr3(address2));

		System.out.println(test.defangIPaddr4(address));
		System.out.println(test.defangIPaddr4(address2));

	}

}
