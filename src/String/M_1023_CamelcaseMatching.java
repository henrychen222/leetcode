/**
 * 1.3 night
 * https://leetcode.com/problems/camelcase-matching/
 */
package String;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class M_1023_CamelcaseMatching {

	/**
	 * Accepted --- 0ms 34.5 MB 100% Accepted --- 0ms 34.6 MB 100% (uncomment use
	 * another if) https://www.cnblogs.com/Dylan-Java-NYC/p/11856023.html
	 * https://xingxingpark.com/Leetcode-1023-Camelcase-Matching/ (same)
	 */
	public List<Boolean> camelMatch(String[] queries, String pattern) {
		List<Boolean> res = new ArrayList<>();
		for (String query : queries) {
			res.add(isMatch(query, pattern));
		}
		return res;
	}

	private boolean isMatch(String query, String pattern) {
		int i = 0;
		for (char c : query.toCharArray()) {
			if (i < pattern.length() && pattern.charAt(i) == c) {
				i++;
			} else if (c >= 'A' && c <= 'Z') {
				return false;
			}
			// if (i < pattern.length() && c == pattern.charAt(i))
			// i++;
			// else if (c < 'a')
			// return false;
		}
		return i == pattern.length();
	}

	/**
	 * Accepted --- 0ms 34.5 MB 100% (Same to camelMatch())
	 * https://massivealgorithms.blogspot.com/2019/04/leetcode-1023-camelcase-matching.html
	 */
	public List<Boolean> camelMatch2(String[] queries, String pattern) {
		List<Boolean> res = new ArrayList<>();
		for (String query : queries) {
			res.add(isMatch2(query.toCharArray(), pattern.toCharArray()));
		}
		return res;
	}

	private boolean isMatch2(char[] query, char[] pattern) {
		int j = 0;
		for (int i = 0; i < query.length; i++) {
			if (j < pattern.length && query[i] == pattern[j]) {
				j++;
			} else if (query[i] >= 'A' && query[i] <= 'Z') {
				return false;
			}
		}
		return j == pattern.length;
	}

	/**
	 * Accepted --- 8ms 36.5 MB 6.99%
	 * https://massivealgorithms.blogspot.com/2019/04/leetcode-1023-camelcase-matching.html
	 */
	public List<Boolean> camelMatch3(String[] queries, String pattern) {
		String newPattern = "[a-z]*" + String.join("[a-z]*", pattern.split("")) + "[a-z]*";
		List<Boolean> ans = new ArrayList<>();
		for (String q : queries) {
			ans.add(q.matches(newPattern));
		}
		return ans;
	}

	/**
	 * Accepted --- 15ms 37 MB 6.99%
	 * https://massivealgorithms.blogspot.com/2019/04/leetcode-1023-camelcase-matching.html
	 */
	public List<Boolean> camelMatch4(String[] queries, String pattern) {
		String newPattern = "[a-z]*" + String.join("[a-z]*", pattern.split("")) + "[a-z]*";
		return Arrays.stream(queries).map(q -> q.matches(newPattern)).collect(Collectors.toList());
	}

	/**
	 * Accepted --- 1ms 34.9 MB 29.48%
	 * https://blog.csdn.net/katrina95/article/details/89206853
	 */
	public List<Boolean> camelMatch5(String[] queries, String pattern) {
		List<Boolean> list = new ArrayList<>();
		List<String> p = split(pattern);
		for (int i = 0; i < queries.length; i++) {
			List<String> q = split(queries[i]);
			if (p.size() != q.size()) {
				list.add(false);
				continue;
			}
			boolean Match = true;
			for (int j = 0; j < p.size(); j++) {
				String s1 = p.get(j);
				String s2 = q.get(j);
				int p1 = 0;
				int p2 = 0;
				while (p1 < s1.length() && p2 < s2.length()) {
					if (s1.charAt(p1) == s2.charAt(p2)) {
						p1++;
						p2++;
					} else {
						p2++;
					}
				}
				if (p1 < s1.length()) {
					Match = false;
					break;
				}
			}
			list.add(Match);
		}
		return list;
	}

	public List<String> split(String s) {
		List<String> res = new ArrayList<>();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (Character.isUpperCase(c)) {
				StringBuilder sb = new StringBuilder(c);
				i++;
				while (i < s.length() && Character.isLowerCase(s.charAt(i))) {
					sb.append(s.charAt(i++));
				}
				res.add(sb.toString());
				i--;
			}
		}
		return res;
	}

	public static void main(String[] args) {
		M_1023_CamelcaseMatching test = new M_1023_CamelcaseMatching();

		String[] queries1 = new String[] { "FooBar", "FooBarTest", "FootBall", "FrameBuffer", "ForceFeedBack" };
		String pattern1 = "FB";

		String[] queries2 = new String[] { "FooBar", "FooBarTest", "FootBall", "FrameBuffer", "ForceFeedBack" };
		String pattern2 = "FoBa";

		String[] queries3 = new String[] { "FooBar", "FooBarTest", "FootBall", "FrameBuffer", "ForceFeedBack" };
		String pattern3 = "FoBaT";

		System.out.println(test.camelMatch(queries1, pattern1)); // [true,false,true,true,false]
		System.out.println(test.camelMatch(queries2, pattern2)); // [true,false,true,false,false]
		System.out.println(test.camelMatch(queries3, pattern3)); // [false,true,false,false,false]

		System.out.println("");
		System.out.println(test.camelMatch2(queries1, pattern1));
		System.out.println(test.camelMatch2(queries2, pattern2));
		System.out.println(test.camelMatch2(queries3, pattern3));

		System.out.println("");
		System.out.println(test.camelMatch3(queries1, pattern1));
		System.out.println(test.camelMatch3(queries2, pattern2));
		System.out.println(test.camelMatch3(queries3, pattern3));

		System.out.println("");
		System.out.println(test.camelMatch4(queries1, pattern1));
		System.out.println(test.camelMatch4(queries2, pattern2));
		System.out.println(test.camelMatch4(queries3, pattern3));

		System.out.println("");
		System.out.println(test.camelMatch5(queries1, pattern1));
		System.out.println(test.camelMatch5(queries2, pattern2));
		System.out.println(test.camelMatch5(queries3, pattern3));

	}

}
