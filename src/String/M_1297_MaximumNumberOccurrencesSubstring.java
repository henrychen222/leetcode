/**
 * 12.22 evening
 * https://leetcode.com/problems/maximum-number-of-occurrences-of-a-substring/
 */
package String;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class M_1297_MaximumNumberOccurrencesSubstring {

	public int maxFreq(String s, int maxLetters, int minSize, int maxSize) {
		Map<String, Integer> result = new HashMap<String, Integer>();
		int count = 0;
		if (minSize > s.length()) {
			return 0;
		}

		for (int i = 0; i < s.length(); i++) {
			Map<Character, Integer> map = new HashMap<Character, Integer>();
			int distNum = 0;
			for (int j = 0; j < maxSize; j++) {
				if (i + j >= s.length()) {
					break;
				}
				map.put(s.charAt(i + j), map.getOrDefault(s.charAt(i + j), 0) + 1);
				if (map.get(s.charAt(i + j)) == 1) {
					distNum++;
				}
				if (distNum > maxLetters) {
					break;
				}
				if (j >= minSize - 1) {
					result.put(s.substring(i, i + j + 1), result.getOrDefault(s.substring(i, i + j + 1), 0) + 1);
				}
			}
		}

		for (String str : result.keySet()) {
			if (result.get(str) > count) {
				count = result.get(str);
			}
		}

		return count;
	}

	@Test
	public static void main(String[] args) {
		M_1297_MaximumNumberOccurrencesSubstring test = new M_1297_MaximumNumberOccurrencesSubstring();

		String s1 = "aababcaab";
		int maxLetters1 = 2;
		int minSize1 = 3;
		int maxSize1 = 4;
		System.out.println(test.maxFreq(s1, maxLetters1, minSize1, maxSize1)); // 2
		assertEquals(test.maxFreq(s1, maxLetters1, minSize1, maxSize1), 2);

		String s2 = "aaaa";
		int maxLetters2 = 1;
		int minSize2 = 3;
		int maxSize2 = 3;
		System.out.println(test.maxFreq(s2, maxLetters2, minSize2, maxSize2)); // 2
		assertEquals(test.maxFreq(s2, maxLetters2, minSize2, maxSize2), 2);

		String s3 = "aabcabcab";
		int maxLetters3 = 2;
		int minSize3 = 2;
		int maxSize3 = 3;
		System.out.println(test.maxFreq(s3, maxLetters3, minSize3, maxSize3)); // 3
		assertEquals(test.maxFreq(s3, maxLetters3, minSize3, maxSize3), 3);

		String s4 = "abcde";
		int maxLetters4 = 2;
		int minSize4 = 3;
		int maxSize4 = 3;
		System.out.println(test.maxFreq(s4, maxLetters4, minSize4, maxSize4)); // 0
		assertEquals(test.maxFreq(s4, maxLetters4, minSize4, maxSize4), 0);

	}

}
