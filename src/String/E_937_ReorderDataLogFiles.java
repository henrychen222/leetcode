/**
 * 1.10 night created
 * 1.11 evening
 * https://leetcode.com/problems/reorder-data-in-log-files/
 */
package String;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class E_937_ReorderDataLogFiles {

	/**
	 * Accepted --- 2ms 39.2 MB 99.08%
	 * 
	 * Collection.sort(letter_log, Comparator)
	 * http://www.noteanddata.com/leetcode-937-Reorder-Log-Files-java-solution-note.html
	 */
	public String[] reorderLogFiles_sortLetterLogs1(String[] logs) {
		List<String> llogs = new ArrayList<>(); // letter logs
		List<String> dlogs = new ArrayList<>(); // digit logs

		// 先把两类字符串分开
		for (String s : logs) {
			int i = s.indexOf(" ");
			char ch = s.charAt(i + 1);
			if (ch >= '0' && ch <= '9') {
				dlogs.add(s);
			} else {
				llogs.add(s);
			}
		}

		// 然后对纯字母(letter_log)的那些进行排序
		Collections.sort(llogs, new Comparator<String>() {
			@Override
			public int compare(String s1, String s2) {
				int index1 = s1.indexOf(" ");
				String id1 = s1.substring(0, index1);
				String letter1 = s1.substring(index1 + 1);

				int index2 = s2.indexOf(" ");
				String id2 = s2.substring(0, index2);
				String letter2 = s2.substring(index2 + 1);
				int v1 = letter1.compareTo(letter2);
				if (v1 != 0)
					return v1;
				int v2 = id1.compareTo(id2);
				return v2;
			}
		});

		// 最后合并结果
		String[] result = new String[llogs.size() + dlogs.size()];
		int i = 0;
		for (String s : llogs) {
			result[i++] = s;
		}
		for (String s : dlogs) {
			result[i++] = s;
		}
		return result;
	}

	/**
	 * Wrong Answer
	 * 
	 * Collection.sort(letter_log, Comparator)
	 * https://github.com/Cee/Leetcode/blob/master/937%20-%20Reorder%20Log%20Files.java
	 */
	public String[] reorderLogFiles_sortLetterLogs2(String[] logs) {
		if (logs == null || logs.length == 0) {
			return logs;
		}
		String[] result = new String[logs.length];
		ArrayList<Log> letters_log = new ArrayList<>();
		ArrayList<String> digits_log = new ArrayList<>();

		// 先把两类字符串分开
		for (String log : logs) {
			int space = log.indexOf(" ");
			String id = log.substring(0, space);
			String l = log.substring(space + 1);
			if (l.charAt(0) >= '0' && l.charAt(0) <= '9') {
				digits_log.add(log);
			} else {
				letters_log.add(new Log(id, l));
			}
		}

		// 然后对纯字母(letter_log)的那些进行排序
		Collections.sort(letters_log, (a, b) -> (a.log.compareTo(b.log)));

		// 最后合并结果
		int i = 0;
		for (Log l : letters_log) {
			result[i++] = l.id + " " + l.log;
		}
		for (String s : digits_log) {
			result[i++] = s;
		}
		return result;
	}

	class Log {
		String id;
		String log;

		Log(String id, String log) {
			this.id = id;
			this.log = log;
		}
	}

	/**
	 * Wrong Answer
	 * 
	 * Collection.sort(letter_log)
	 * https://blog.csdn.net/iwts_24/article/details/83990375
	 */
	public String[] reorderLogFiles_sortLetterLogs3(String[] logs) {
		HashMap<String, String> map = new HashMap<>();
		ArrayList<String> llogs = new ArrayList<>(); // letter logs
		ArrayList<String> dlogs = new ArrayList<>(); // digit logs

		// 先把两类字符串分开
		for (int i = 0; i < logs.length; i++) {
			for (int j = 0; j < logs[i].length(); j++) {
				if (logs[i].charAt(j) == ' ') {
					String flag = logs[i].substring(0, j);
					String key = logs[i].substring(j + 1, logs[i].length());
					if (key.charAt(0) >= '0' && key.charAt(0) <= '9') {
						dlogs.add(key);
					} else {
						llogs.add(key);
					}
					map.put(key, flag);
					break;
				}
			}
		}

		// 然后对纯字母(letter_log)的那些进行排序
		Collections.sort(llogs);

		// 最后合并结果 StringBuilder
		String[] result = new String[logs.length];
		int i = 0;
		for (i = 0; i < llogs.size(); i++) {
			String value = llogs.get(i);
			StringBuilder sb = new StringBuilder(map.get(value));
			sb.append(" " + value);
			result[i] = sb.toString();
		}
		for (int j = 0; j < dlogs.size(); j++, i++) {
			String value = dlogs.get(j);
			StringBuilder sb = new StringBuilder(map.get(value));
			sb.append(" " + value);
			result[i] = sb.toString();
		}
		return result;
	}

	/**
	 * Accepted --- 8ms 39.4 MB 40.25%
	 * 
	 * Array.sort(logs, Lambda expressions/Function), same to 2
	 * https://massivealgorithms.blogspot.com/2018/11/leetcode-937-reorder-log-files.html
	 */
	public String[] reorderLogFiles_sortLogs1(String[] logs) {
		Arrays.sort(logs, (log1, log2) -> {
			String[] split1 = log1.split(" ", 2);
			String[] split2 = log2.split(" ", 2);
			boolean isDigit1 = Character.isDigit(split1[1].charAt(0));
			boolean isDigit2 = Character.isDigit(split2[1].charAt(0));
			if (!isDigit1 && !isDigit2) {
				int cmp = split1[1].compareTo(split2[1]);
				if (cmp != 0)
					return cmp;
				return split1[0].compareTo(split2[0]);
			}
			return isDigit1 ? (isDigit2 ? 0 : 1) : -1;
		});
		return logs;
	}

	/**
	 * Accepted --- 7ms 38.9 MB 59.73%
	 * 
	 * Array.sort(logs, Comparator), same to 1
	 * https://jeffchern.gitbook.io/java-notes/string/937.-reorder-log-files
	 */
	public String[] reorderLogFiles_sortLogs2(String[] logs) {
		Comparator<String> myCom = new Comparator<String>() {
			@Override
			public int compare(String s1, String s2) {
				String[] split1 = s1.split(" ", 2);
				String[] split2 = s2.split(" ", 2);
				boolean isDigit1 = Character.isDigit(split1[1].charAt(0));
				boolean isDigit2 = Character.isDigit(split2[1].charAt(0));

				if (!isDigit1 && !isDigit2) {
					int comp = split1[1].compareTo(split2[1]);
					if (comp != 0)
						return comp;
					return split1[0].compareTo(split2[0]);
				}

				return isDigit1 ? (isDigit2 ? 0 : 1) : -1;
			}
		};
		Arrays.sort(logs, myCom);
		return logs;
	}

	public static void main(String[] args) {
		E_937_ReorderDataLogFiles test = new E_937_ReorderDataLogFiles();

		String[] logs = new String[] { "dig1 8 1 5 1", "let1 art can", "dig2 3 6", "let2 own kit dig",
				"let3 art zero" };

		/*
		 * Output:
		 * ["let1 art can","let3 art zero","let2 own kit dig","dig1 8 1 5 1","dig2 3 6"]
		 */
		System.out.println(Arrays.toString(test.reorderLogFiles_sortLetterLogs1(logs)));
		System.out.println(Arrays.toString(test.reorderLogFiles_sortLetterLogs2(logs)));
		System.out.println(Arrays.toString(test.reorderLogFiles_sortLetterLogs3(logs)));
		System.out.println(Arrays.toString(test.reorderLogFiles_sortLogs1(logs)));
		System.out.println(Arrays.toString(test.reorderLogFiles_sortLogs2(logs)));

		/*
		 * Output:
		 * ["a2 act car","g1 act car","a8 act zoo","ab1 off key dog","a1 9 2 3 1","zo4 4 7"]
		 */
		System.out.println("");
		String[] logs2 = new String[] { "a1 9 2 3 1", "g1 act car", "zo4 4 7", "ab1 off key dog", "a8 act zoo",
				"a2 act car" };
		System.out.println(Arrays.toString(test.reorderLogFiles_sortLetterLogs1(logs2)));
		System.out.println(Arrays.toString(test.reorderLogFiles_sortLetterLogs2(logs2)));  // wrong
		System.out.println(Arrays.toString(test.reorderLogFiles_sortLetterLogs3(logs2)));  // wrong
		System.out.println(Arrays.toString(test.reorderLogFiles_sortLogs1(logs2)));
		System.out.println(Arrays.toString(test.reorderLogFiles_sortLogs2(logs2)));

	}

}
