/**
 * 1.10 night created
 * https://leetcode.com/problems/unique-email-addresses/
 * 
 * 1.12 evening
 */
package String;

import java.util.HashSet;
import java.util.Set;

public class E_929_UniqueEmailAddresses {

	/**
	 * Accepted --- 24ms 38.8 MB 29.13%
	 * 
	 * 把@前面的部分对+和"." 分别做处理，然后放到set里面
	 * http://www.noteanddata.com/leetcode-929-Unique-Email-Addresses-java-solution-note.html
	 */
	public int numUniqueEmails(String[] emails) {
		Set<String> set = new HashSet<>();
		for (String email : emails) {
			String[] arr = email.split("@");
			String local = arr[0];
			int index = local.indexOf("+");
			if (index >= 0) {
				local = local.substring(0, index);
			}
			local = local.replaceAll("\\.", "");
			set.add(local + "@" + arr[1]);
		}
		return set.size();
	}

	/**
	 * Accepted --- 21ms 38.3 MB 45.04%
	 * 
	 * https://www.cnblogs.com/lightwindy/p/10683316.html
	 */
	public int numUniqueEmails2(String[] emails) {
		Set<String> normalized = new HashSet<>();
		for (String email : emails) {
			String[] parts = email.split("@"); // split into local and domain parts.
			String[] local = parts[0].split("\\+"); // split local by '+'.
			// remove all '.', and concatenate '@' and domain.
			normalized.add(local[0].replace(".", "") + "@" + parts[1]);
		}
		return normalized.size();
	}

	/**
	 * Wrong Answer ---
	 * 
	 * https://zhuanlan.zhihu.com/p/56205368
	 */
	public int numUniqueEmails3(String[] emails) {
		HashSet<String> set = new HashSet<String>();
		int count = 0;
		for (String email : emails) {
			String[] parts = email.split("@");
			String pattern = parts[0].replace(".", "").split("\\+")[0] + parts[1];
			if (!set.contains(pattern)) {
				count++;
			}
			set.add(pattern);
		}
		return count;
	}

	/**
	 * Accepted --- 9ms 38.7 MB 87.30%
	 * 
	 * https://github.com/Cee/Leetcode/blob/master/929%20-%20Unique%20Email%20Addresses.java
	 */
	public int numUniqueEmails4(String[] emails) {
		if (emails == null || emails.length == 0) {
			return 0;
		}
		Set<String> mails = new HashSet<>();
		for (String email : emails) {
			String[] splits = email.split("@");
			StringBuilder sb = new StringBuilder();
			for (char c : splits[0].toCharArray()) {
				if (c == '+') {
					break;
				}
				if (c == '.') {
					continue;
				}
				sb.append(c);
			}
			sb.append("@").append(splits[1]);
			mails.add(sb.toString());
		}
		return mails.size();
	}

	/**
	 * Accepted --- 25ms 38.6 MB 25.91%
	 * 
	 * https://github.com/varunu28/LeetCode-Java-Solutions/blob/master/Contests/Weekly%20Contest%20108/Unique%20Email%20Addresses.java
	 */
	public int numUniqueEmails5(String[] emails) {
		Set<String> set = new HashSet<>();
		for (String email : emails) {
			String localName = email.split("@")[0];
			String domainName = email.split("@")[1];
			localName = localName.replace(".", "");
			int idx = localName.indexOf('+');
			if (idx != -1) {
				localName = localName.substring(0, idx);
			}
			set.add(localName + "@" + domainName);
		}
		return set.size();
	}

	/**
	 * Wrong Answer
	 * 
	 * https://blog.csdn.net/qq_38959715/article/details/83998951
	 */
	public int numUniqueEmails_csdn1(String[] emails) {
		Set<String> set = new HashSet<>();
		int res = 0;
		String[] str = new String[2];
		for (int i = 0; i < emails.length; i++) {
			str = emails[i].split("@");
			String temp = str[0].replaceAll("\\.", "");
			int end = temp.indexOf("+");
			if (end != -1)
				str[0] = temp.substring(0, end);
			if (!set.contains(str[0] + "@" + str[1])) {
				res++;
				set.add(str[0] + "@" + str[1]);
			}
		}
		return res;
	}

	/**
	 * Accepted --- 19ms 38.7 MB 53.73%
	 * 
	 * https://blog.csdn.net/qq_38959715/article/details/83998951
	 */
	public int numUniqueEmails_csdn2(String[] emails) {
		Set<String> set = new HashSet<>();
		int res = 0;
		String[] str = new String[2];
		for (int i = 0; i < emails.length; i++) {
			str = emails[i].split("@");
			String temp = "";
			int end = str[0].indexOf("+");
			if (end != -1)
				temp = str[0].substring(0, end);
			else
				temp = str[0];
			str[0] = "";
			char[] ch = temp.toCharArray();
			for (int j = 0; j < ch.length; j++) {
				if (ch[j] != '.')
					str[0] += ch[j];
			}
			if (!set.contains(str[0] + "@" + str[1])) {
				res++;
				set.add(str[0] + "@" + str[1]);
			}
		}
		return res;
	}

	/**
	 * Accepted --- 8ms 37.8 MB 92.49%
	 * 
	 * https://blog.csdn.net/qq_38959715/article/details/83998951
	 */
	public int numUniqueEmails_csdn3(String[] emails) {
		Set<String> set = new HashSet<>();
		String last = "";
		String first = "";
		for (String email : emails) {
			last = email.substring(email.indexOf("@"));
			first = email.substring(0, email.indexOf("@"));
			char[] arr = new char[first.length()];
			arr = first.toCharArray();
			int j = 0;
			char[] arr1 = new char[100];
			for (int i = 0; i < arr.length; i++) {
				if (arr[i] == '.') {
					continue;
				}
				if (arr[i] == '+') {
					break;
				}
				arr1[j] = arr[i];
				j++;
			}
			set.add(String.valueOf(arr1).trim() + last);
		}
		return set.size();
	}

	/**
	 * Wrong Answer
	 * 
	 * https://blog.csdn.net/qq_38959715/article/details/83998951
	 */
	public int numUniqueEmails_csdn4(String[] emails) {
		Set<String> set = new HashSet<>();
		for (String str : emails) {
			int n = str.indexOf('@');
			String temp = str.substring(n);
			set.add(temp);
		}
		return set.size();
	}

	public static void main(String[] args) {
		E_929_UniqueEmailAddresses test = new E_929_UniqueEmailAddresses();

		String[] emails = new String[] { "test.email+alex@leetcode.com", "test.e.mail+bob.cathy@leetcode.com",
				"testemail+david@lee.tcode.com" };

		System.out.println(test.numUniqueEmails(emails)); // 2
		System.out.println(test.numUniqueEmails2(emails));
		System.out.println(test.numUniqueEmails3(emails));
		System.out.println(test.numUniqueEmails4(emails));
		System.out.println(test.numUniqueEmails5(emails));
		System.out.println(test.numUniqueEmails_csdn1(emails));
		System.out.println(test.numUniqueEmails_csdn2(emails));
		System.out.println(test.numUniqueEmails_csdn3(emails));
		System.out.println(test.numUniqueEmails_csdn4(emails));

		String[] emails2 = new String[] { "test.email+alex@leetcode.com", "test.email.leet+alex@code.com" };

		System.out.println("");
		System.out.println(test.numUniqueEmails(emails2)); // 2
		System.out.println(test.numUniqueEmails2(emails2));
		System.out.println(test.numUniqueEmails3(emails2)); // wrong
		System.out.println(test.numUniqueEmails4(emails2));
		System.out.println(test.numUniqueEmails5(emails2));
		System.out.println(test.numUniqueEmails_csdn1(emails2));
		System.out.println(test.numUniqueEmails_csdn2(emails2));
		System.out.println(test.numUniqueEmails_csdn3(emails2));
		System.out.println(test.numUniqueEmails_csdn4(emails2));

		String[] emails3 = new String[] { "test.email+alex@leetcode.com", "test.email@leetcode.com" };

		System.out.println("");
		System.out.println(test.numUniqueEmails(emails3)); // 1
		System.out.println(test.numUniqueEmails2(emails3));
		System.out.println(test.numUniqueEmails3(emails3));
		System.out.println(test.numUniqueEmails4(emails3));
		System.out.println(test.numUniqueEmails5(emails3));
		System.out.println(test.numUniqueEmails_csdn1(emails3)); // wrong
		System.out.println(test.numUniqueEmails_csdn2(emails3));
		System.out.println(test.numUniqueEmails_csdn3(emails3));
		System.out.println(test.numUniqueEmails_csdn4(emails3));

		String[] emails4 = new String[] { "testemail@leetcode.com", "testemail1@leetcode.com", "testemail+david@lee.tcode.com" };

		System.out.println("");
		System.out.println(test.numUniqueEmails(emails4)); // 3
		System.out.println(test.numUniqueEmails2(emails4));
		System.out.println(test.numUniqueEmails3(emails4));
		System.out.println(test.numUniqueEmails4(emails4));
		System.out.println(test.numUniqueEmails5(emails4));
		System.out.println(test.numUniqueEmails_csdn1(emails4));
		System.out.println(test.numUniqueEmails_csdn2(emails4));
		System.out.println(test.numUniqueEmails_csdn3(emails4));
		System.out.println(test.numUniqueEmails_csdn4(emails4)); // wrong
	}

}
