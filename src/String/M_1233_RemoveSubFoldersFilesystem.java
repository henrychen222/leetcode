/**
 * 12.30 evening
 * https://leetcode.com/problems/remove-sub-folders-from-the-filesystem
 */
package String;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class M_1233_RemoveSubFoldersFilesystem {

	/**
	 * Accepted --- 42ms 53.9 MB 85.31%
	 * https://leetcode.jp/leetcode-1233-remove-sub-folders-from-the-filesystem%E8%A7%A3%E9%A2%98%E6%80%9D%E8%B7%AF%E5%88%86%E6%9E%90/
	 */
	public List<String> removeSubfolders(String[] folder) {
		Arrays.sort(folder);
		List<String> res = new ArrayList<>();

		// 将第一个元素加上斜杠作为根文件夹
		String root = folder[0] + "/";
		// 将根文件夹放入返回结果
		res.add(folder[0]);

		for (int i = 1; i < folder.length; i++) {
			// 当前文件夹
			String f = folder[i];
			/** 如果当前文件夹长度小于根文件夹，或者根文件夹不是其前缀子字符串, 说明当前文件夹不是子文件夹 **/
			if (f.length() < root.length() || !f.startsWith(root)) {
				// 将当前文件夹设为根文件夹
				root = f + "/";
				// 将当前文件夹加入返回结果
				res.add(f);
			}
		}
		return res;
	}

	/**
	 * Accepted --- 56ms, 53.7 MB 61.31%
	 * https://github.com/charles-wangkai/leetcode/blob/master/remove-sub-folders-from-the-filesystem/Solution.java
	 */
	public List<String> removeSubfolders2(String[] folder) {
		Arrays.sort(folder);
		List<String> result = new ArrayList<>();

		for (String f : folder) {
			if (result.isEmpty() || !(f + "/").startsWith(result.get(result.size() - 1) + "/")) {
				result.add(f);
			}
		}
		return result;
	}

	public static void main(String[] args) {
		M_1233_RemoveSubFoldersFilesystem test = new M_1233_RemoveSubFoldersFilesystem();

		String[] folder1 = new String[] { "/a", "/a/b", "/c/d", "/c/d/e", "/c/f" };
		String[] folder2 = new String[] { "/a", "/a/b/c", "/a/b/d" };
		String[] folder3 = new String[] { "/a/b/c", "/a/b/ca", "/a/b/d" };

		System.out.println(test.removeSubfolders(folder1)); // ["/a","/c/d","/c/f"]
		System.out.println(test.removeSubfolders(folder2)); // ["/a"]
		System.out.println(test.removeSubfolders(folder3)); // ["/a/b/c","/a/b/ca","/a/b/d"]

		System.out.println("");
		System.out.println(test.removeSubfolders2(folder1)); // ["/a","/c/d","/c/f"]
		System.out.println(test.removeSubfolders2(folder2)); // ["/a"]
		System.out.println(test.removeSubfolders2(folder3)); // ["/a/b/c","/a/b/ca","/a/b/d"]

	}

}
