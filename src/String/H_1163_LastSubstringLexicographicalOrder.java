/**
 * 1.3 night
 * https://leetcode.com/problems/last-substring-in-lexicographical-order
 */
package String;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class H_1163_LastSubstringLexicographicalOrder {

	/**
	 * Accepted --- 66ms 53.4 MB 12.99%
	 * https://github.com/alberyan/Leetcode-Solution/blob/master/1163.%20Last%20Substring%20in%20Lexicographical%20Order.java
	 */
	public String lastSubstring(String s) {
		List<List<Integer>> list = new ArrayList<>();
		for (int i = 0; i < 26; i++) {
			list.add(new ArrayList<>());
		}
		for (int i = 0; i < s.length(); i++) {
			int digit = s.charAt(i) - 'a';
			list.get(digit).add(i);
		}
		int maxDigit = 26;
		for (int i = 25; i >= 0; i--) {
			if (list.get(i).size() > 0) {
				maxDigit = i;
				break;
			}
		}
		if (maxDigit == 0)
			return s;
		return s.substring(helper(list.get(maxDigit), s));
	}

	int helper(List<Integer> list, String s) {
		if (list.size() == 1)
			return list.get(0);
		List<Integer> next = new ArrayList<>();
		char maxCh = 'a';
		for (int loc : list) {
			if (loc + 1 >= s.length())
				continue;
			char cur = s.charAt(loc + 1);
			if (cur > maxCh) {
				maxCh = cur;
				next = new ArrayList<>();
				next.add(loc + 1);
			} else if (cur == maxCh) {
				next.add(loc + 1);
			}
		}
		return helper(next, s) - 1;
	}

	/**
	 * Accepted --- 15ms 39.4 MB 75.14%
	 * https://www.codeleading.com/article/43021984672/
	 */
	public String lastSubstring2(String s) {
		int i = 0, j = 1, offset = 0, len = s.length();
		while (i + offset < len && j + offset < len) {
			char c = s.charAt(i + offset), d = s.charAt(j + offset);
			if (c == d) {
				++offset;
			} else {
				// chars in [i, ..., i + offset] <= charAt(i) == charAt(j)
				if (c < d) {
					i += offset + 1;
				} else { // c > d, chars in [j, ..., j + offset] <= charAt(i) == charAt(j)
					j += offset + 1;
				}
				// avoid duplicate start indices.
				if (i == j) {
					++i;
				}
				offset = 0;
			}
		}
		return s.substring(Math.min(i, j));
	}

	/**
	 * Accepted --- 71ms 44.4 MB 11.30%
	 * https://www.codeleading.com/article/43021984672/
	 */
	public String lastSubstring3(String s) {
		TreeSet<Character> ts = new TreeSet<>();
		for (int i = 0; i < s.length(); ++i)
			ts.add(s.charAt(i));
		int radix = ts.size(), lo = 0;
		double max = 0d, cur = 0d;
		for (int i = s.length() - 1; i >= 0; --i) {
			cur = ts.headSet(s.charAt(i)).size() + cur / radix;
			if (max <= cur) {
				max = cur;
				lo = i;
			}
		}
		return s.substring(lo);
	}

	/**
	 * Accepted --- 70ms 48.3 MB 11.86%
	 * https://www.codeleading.com/article/51811975574/
	 * https://blog.csdn.net/katrina95/article/details/99950587 (same)
	 */
	public String lastSubstring4(String s) {
		if (s == null || s.length() <= 1)
			return s;
		char max = 'a';
		char sam = s.charAt(0);
		boolean same = true;
		for (int i = 1; i < s.length(); i++)
			if (s.charAt(i) != sam)
				same = false;
		if (same)
			return s;
		for (int i = 0; i < s.length(); i++)
			if (s.charAt(i) > max)
				max = s.charAt(i);
		List<node> list = new ArrayList<>();
		for (int i = 0; i < s.length(); i++)
			if (s.charAt(i) == max)
				list.add(new node(i, i));
		if (list.size() == 1)
			return s.substring(list.get(0).original);
		while (true) {
			char x = 'a';
			for (node n : list) {
				n.index += 1;
				if (n.index < s.length() && s.charAt(n.index) > x)
					x = s.charAt(n.index);
			}
			List<node> newList = new ArrayList<>();
			for (node n : list) {
				if (n.index < s.length() && s.charAt(n.index) == x)
					newList.add(n);
			}
			if (newList.size() == 1)
				return s.substring(newList.get(0).original);
			list = newList;
		}
	}

	public class node {
		int index;
		int original;

		public node(int index, int original) {
			this.index = index;
			this.original = original;
		}
	}

	public static void main(String[] args) {
		String s1 = "abab";
		String s2 = "leetcode";

		H_1163_LastSubstringLexicographicalOrder test = new H_1163_LastSubstringLexicographicalOrder();
		System.out.println(test.lastSubstring(s1)); // "bab"
		System.out.println(test.lastSubstring(s2)); // "tcode"

		System.out.println(test.lastSubstring2(s1));
		System.out.println(test.lastSubstring2(s2));

		System.out.println(test.lastSubstring3(s1));
		System.out.println(test.lastSubstring3(s2));

		System.out.println(test.lastSubstring4(s1));
		System.out.println(test.lastSubstring4(s2));

	}

}
