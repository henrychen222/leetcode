/**
 * 2.2 evening
 * https://leetcode.com/problems/expressive-words/
 */
package String;

import java.util.ArrayList;
import java.util.List;

public class M_809_ExpressiveWords {

	/**
	 * Accepted --- 782ms 37.9 MB 5.38%
	 * 
	 * https://www.programcreek.com/2013/03/leetcode-expressive-words-java/
	 */
	public int expressiveWords(String S, String[] words) {
		int count = 0;
		for (String s : words) {
			if (isMatch(S, 0, s, 0)) {
				count++;
			}
		}

		return count;
	}

	private boolean isMatch(String s1, int i, String s2, int j) {
		if (i >= s1.length() && j >= s2.length()) {
			return true;
		} else if (i >= s1.length() || j >= s2.length()) {
			return false;
		}

		if (s1.charAt(i) != s2.charAt(j)) {
			return false;
		}

		boolean res = false;

		if (i + 2 < s1.length() && s1.charAt(i + 1) == s2.charAt(j) && s1.charAt(i + 2) == s2.charAt(j)) {
			int m = i + 2;
			int n = j + 1;
			while (m < s1.length() && s1.charAt(m) == s2.charAt(j)) {
				m++;
			}
			while (n < s2.length() && s2.charAt(n) == s2.charAt(n - 1)) {
				n++;
			}
			if (n - j > m - i) {
				return false;
			}

			res = isMatch(s1, m, s2, n);
		}

		res = res | isMatch(s1, i + 1, s2, j + 1);

		return res;
	}

	/**
	 * Accepted --- Wrong Answer
	 * 
	 * http://buttercola.blogspot.com/2018/04/leetcode-809-expressive-words.html
	 */
	public int expressiveWords2(String S, String[] words) {
		int ans = 0;
		for (String word : words) {
			if (isStretchy(S, word)) {
				ans++;
			}
		}

		return ans;
	}

	private boolean isStretchy(String s, String word) {
		int i = 0;
		int j = 0;
		int m = s.length();
		int n = word.length();

		while (i < m && j < n) {
			// Step 1: move j to the next group
			while (i < m && j < n && s.charAt(i) == word.charAt(j)) {
				i++;
				j++;
			}

			// Step 2: move i to the next group
			while (i > 0 && i < m && s.charAt(i) == s.charAt(i - 1)) {
				i++;
			}

			// Step 3: check if it's not stretchy
			if (i < 3 || s.charAt(i - 1) != s.charAt(i - 2) || s.charAt(i - 2) != s.charAt(i - 3)) {
				break;
			}
		}

		return i == m && j == n;
	}

	/**
	 * Accepted --- 4ms 40.6 MB 48.02%
	 * 
	 * https://github.com/Cee/Leetcode/blob/master/809%20-%20Expressive%20Words.java
	 */
	public int expressiveWords3(String S, String[] words) {
		List<Group> repS = representation(S);

		int ans = 0;
		for (String word : words) {
			if (word.length() > S.length()) {
				continue;
			}
			if (word.length() == S.length()) {
				if (word.equals(S)) {
					ans += 1;
				}
				continue;
			}
			List<Group> repW = representation(word);
			if (repW.size() != repS.size()) {
				continue;
			}
			boolean same = true;
			for (int i = 0; i < repW.size(); i++) {
				Group w = repW.get(i);
				Group s = repS.get(i);
				// System.out.println(w.c); // if write this will 50ms 41.3 MB 5.38%
				if (w.c != s.c) {
					same = false;
					break;
				}
				if (w.count > s.count) {
					same = false;
					break;
				}
				if (w.count == s.count) {
					continue;
				}
				if (s.count <= 2) {
					if (w.count != s.count) {
						same = false;
						break;
					}
				}
			}
			ans += same ? 1 : 0;
		}
		return ans;
	}

	class Group {
		char c;
		int count;

		Group(char ch, int cnt) {
			c = ch;
			count = cnt;
		}
	}

	private List<Group> representation(String s) {
		List<Group> rep = new ArrayList<>();
		if (s == null || s.length() == 0) {
			return rep;
		}
		int n = s.length();
		int i = 0;
		while (i < n) {
			int count = 0;
			int lo = i;
			while (i < n && s.charAt(i) == s.charAt(lo)) {
				i++;
				count++;
			}
			rep.add(new Group(s.charAt(lo), count));
		}
		return rep;
	}

	public static void main(String[] args) {
		M_809_ExpressiveWords test = new M_809_ExpressiveWords();

		String S = "heeellooo";
		String[] words = new String[] { "hello", "hi", "helo" };

		String S2 = "lee";
		String[] words2 = new String[] { "le" };

		System.out.println(test.expressiveWords(S, words)); // 1
		System.out.println(test.expressiveWords(S2, words2)); // 0

		System.out.println("");
		System.out.println(test.expressiveWords2(S, words));
		System.out.println(test.expressiveWords2(S2, words2)); // wrong

		System.out.println("");
		System.out.println(test.expressiveWords3(S, words));
		System.out.println(test.expressiveWords3(S2, words2));

	}

}
