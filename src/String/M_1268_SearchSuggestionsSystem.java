/**
 * 12.25 evening
 * https://leetcode.com/problems/search-suggestions-system/
 */
package String;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class M_1268_SearchSuggestionsSystem {

	public List<List<String>> suggestedProducts(String[] products, String searchWord) {
		M_1268_Trie trie = new M_1268_Trie();
		trie.insert(products);
		return trie.searchWord(searchWord);
	}

	@Test
	public static void main(String[] args) {
		M_1268_SearchSuggestionsSystem test = new M_1268_SearchSuggestionsSystem();

		String[] products = new String[] { "mobile", "mouse", "moneypot", "monitor", "mousepad" };
		String searchWord = "mouse";

		String[] products2 = new String[] { "havana" };
		String searchWord2 = "havana";

		String[] products3 = new String[] { "bags", "baggage", "banner", "box", "cloths" };
		String searchWord3 = "bags";

		String[] products4 = new String[] { "havana" };
		String searchWord4 = "tatiana";

		System.out.println(test.suggestedProducts(products, searchWord));
		System.out.println(test.suggestedProducts(products2, searchWord2));
		System.out.println(test.suggestedProducts(products3, searchWord3));
		System.out.println(test.suggestedProducts(products4, searchWord4));

//		String[][] productsExpectResult = new String[][] { { "mobile", "moneypot", "monitor" },
//				{ "mobile", "moneypot", "monitor" }, { "mouse", "mousepad" }, { "mouse", "mousepad" },
//				{ "mouse", "mousepad" } };
//		System.out.println(Arrays.deepToString(productsExpectResult));
//		System.out.println(Arrays.asList(productsExpectResult));
//		
//		assertEquals(test.suggestedProducts(products, searchWord), Arrays.deepToString(productsExpectResult));
	}

}
