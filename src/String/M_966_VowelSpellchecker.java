/**
 * 1.10 night created
 * 1.11 evening
 * https://leetcode.com/problems/vowel-spellchecker/
 */
package String;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class M_966_VowelSpellchecker {

	Set<String> words_perfect;
	Map<String, String> words_cap;
	Map<String, String> words_vow;

	/**
	 * Accepted --- 18ms 42.2 MB  86.42% (second best)
	 * 
	 * https://leetcode.com/articles/vowel-spellchecker/
	 * analyze the 3 cases that the algorithm needs to consider:
	 * (1) the query is an exact match
	 *     hold a set of words to efficiently test whether our query is in the set
	 * (2) the query is a match up to capitalization: 
	 *     hold a hash table that converts the word from its lowerCase version to the original word (with correct capitalization)
	 * (3) the query is a match up to vowel errors
	 *     hold a hash table that converts the word from its lowerCase version with the vowels masked out, to the original word
	 */
	public String[] spellchecker(String[] wordlist, String[] queries) {
		words_perfect = new HashSet<String>();
		words_cap = new HashMap<String, String>();
		words_vow = new HashMap<String, String>();

		for (String word : wordlist) {
			words_perfect.add(word);

			String wordlow = word.toLowerCase();
			words_cap.putIfAbsent(wordlow, word);

			String wordlowDV = devowel(wordlow);
			words_vow.putIfAbsent(wordlowDV, word);
		}

		String[] ans = new String[queries.length];
		int t = 0;
		for (String query : queries)
			ans[t++] = solve(query);
		return ans;
	}

	public String solve(String query) {
		if (words_perfect.contains(query))
			return query;

		String queryL = query.toLowerCase();
		if (words_cap.containsKey(queryL))
			return words_cap.get(queryL);

		String queryLV = devowel(queryL);
		if (words_vow.containsKey(queryLV))
			return words_vow.get(queryLV);

		return "";
	}

	public String devowel(String word) {
		StringBuilder ans = new StringBuilder();
		for (char c : word.toCharArray())
			ans.append(isVowel(c) ? '*' : c);
		return ans.toString();
	}

	public boolean isVowel(char c) {
		return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u');
	}
	
	/**
	 * Accepted --- 47ms 46.4 MB 42.96%
	 * https://massivealgorithms.blogspot.com/2019/03/leetcode-966-vowel-spellchecker.html
	 * https://leetcode.com/problems/vowel-spellchecker/discuss/211189/JavaC%2B%2BPython-Two-HashMap
	 */
	public String[] spellchecker_twoHashMap(String[] wordlist, String[] queries) {
		Set<String> words = new HashSet<>(Arrays.asList(wordlist));
		HashMap<String, String> cap = new HashMap<>();
		HashMap<String, String> vowel = new HashMap<>();
		for (String w : wordlist) {
			String lower = w.toLowerCase(), devowel = lower.replaceAll("[aeiou]", "#");
			cap.putIfAbsent(lower, w);
			vowel.putIfAbsent(devowel, w);
		}
		for (int i = 0; i < queries.length; ++i) {
			if (words.contains(queries[i]))
				continue;
			String lower = queries[i].toLowerCase(), devowel = lower.replaceAll("[aeiou]", "#");
			if (cap.containsKey(lower)) {
				queries[i] = cap.get(lower);
			} else if (vowel.containsKey(devowel)) {
				queries[i] = vowel.get(devowel);
			} else {
				queries[i] = "";
			}
		}
		return queries;
	}
	
	/**
	 * Accepted --- 18ms 40.3 MB  86.42%  (best)
	 * https://github.com/varunu28/LeetCode-Java-Solutions/blob/master/Medium/Vowel%20Spellchecker.java
	 */
	public String[] spellchecker2(String[] wordlist, String[] queries) {
		Map<String, String> caseMap = new HashMap<>();
		Set<String> set = new HashSet<>();

		// Case Part
		for (String word : wordlist) {
			if (!caseMap.containsKey(word.toLowerCase())) {
				caseMap.put(word.toLowerCase(), word);
			}
			set.add(word);
		}

		// Vowel Part
		Map<String, String> vowelMap = new HashMap<>();
		for (String word : wordlist) {
			String genericVal = makeGenericVowel(word);
			if (!vowelMap.containsKey(genericVal)) {
				vowelMap.put(genericVal, word);
			}
		}

		String[] ans = new String[queries.length];
		for (int i = 0; i < queries.length; i++) {
			if (set.contains(queries[i])) {
				ans[i] = queries[i];
			} else if (caseMap.containsKey(queries[i].toLowerCase())) {
				ans[i] = caseMap.get(queries[i].toLowerCase());
			} else if (vowelMap.containsKey(makeGenericVowel(queries[i]))) {
				ans[i] = vowelMap.get(makeGenericVowel(queries[i]));
			} else {
				ans[i] = "";
			}
		}
		return ans;
	}

	private String makeGenericVowel(String s) {
		String vowel = "aeiou";
		char[] ch = s.toLowerCase().toCharArray();
		for (int i = 0; i < ch.length; i++) {
			if (vowel.indexOf(ch[i]) != -1) {
				ch[i] = '#';
			}
		}
		return String.valueOf(ch);
	}

	public static void main(String[] args) {
		M_966_VowelSpellchecker test = new M_966_VowelSpellchecker();

		String[] wordlist = new String[] { "KiTe", "kite", "hare", "Hare" };
		String[] queries = new String[] { "kite", "Kite", "KiTe", "Hare", "HARE", "Hear", "hear", "keti", "keet",
				"keto" };

		System.out.println(Arrays.toString(test.spellchecker(wordlist, queries))); // ["kite","KiTe","KiTe","Hare","hare","","","KiTe","","KiTe"]
		System.out.println(Arrays.toString(test.spellchecker_twoHashMap(wordlist, queries)));
		System.out.println(Arrays.toString(test.spellchecker2(wordlist, queries)));
	}

}
