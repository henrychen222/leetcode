package String;

import java.util.Arrays;

/**
 * 4.5 evening https://leetcode.com/articles/complex-number-multiplication/#
 */
public class M_537_ComplexNumberMultiplication {

	/**
	 * https://leetcode.com/articles/complex-number-multiplication/#
	 * 
	 * Accepted --- 13ms 39.6 MB 5.11%
	 */
	public String complexNumberMultiply(String a, String b) {
		String[] x = a.split("\\+|i");
		String[] y = b.split("\\+|i");

		// System.out.println(Arrays.asList(a));
		// System.out.println(Arrays.asList(b));

		int a_real = Integer.parseInt(x[0]);
		int a_img = Integer.parseInt(x[1]);
		int b_real = Integer.parseInt(y[0]);
		int b_img = Integer.parseInt(y[1]);

		// System.out.println(a_real);
		// System.out.println(a_img);
		// System.out.println(b_real);
		// System.out.println(b_img);

		return (a_real * b_real - a_img * b_img) + "+" + (a_real * b_img + a_img * b_real) + "i";

	}

	public static void main(String[] args) {
		M_537_ComplexNumberMultiplication test = new M_537_ComplexNumberMultiplication();
		String a = "1+1i";
		String b = "1+1i";
		System.out.println(test.complexNumberMultiply(a, b));

	}
}