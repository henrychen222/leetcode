/**
 * 1.10 night created  
 * 1.11 evening
 * https://leetcode.com/problems/check-if-word-is-valid-after-substitutions/
 */
package String;

import java.util.Stack;

public class M_1003_CheckIfWordIsValidAfterSubstitutions {

	/**
	 * Accepted --- 18ms 38.7 MB 54.40% stack
	 * http://www.noteanddata.com/leetcode-1003-Check-If-Word-Is-Valid-After-Substitutions-java-solution-note.html
	 */
	public boolean isValid_stack1(String S) {
		Stack<Character> stack = new Stack<>();
		for (char ch : S.toCharArray()) {
			// 遇到a入栈
			if (ch == 'a') {
				stack.push(ch);
			} else if (ch == 'b') { // 遇到b判断现在是否有一个a在栈顶
				if (stack.isEmpty() || stack.peek() != 'a') {
					return false;
				}
				stack.push(ch);
			} else { // 遇到c判断现在栈顶是否有a和b
				if (stack.isEmpty() || stack.pop() != 'b') {
					return false;
				}
				if (stack.isEmpty() || stack.pop() != 'a') {
					return false;
				}
			}
		}
		return stack.isEmpty();
	}

	/**
	 * Accepted --- 16ms 38.5 MB 68.10% stack: 只对字母c做处理 whenever meet 'c',pop a and
	 * b at the end of stack. Similar to isValid_stack1()
	 * https://massivealgorithms.blogspot.com/2019/03/leetcode-1003-check-if-word-is-valid.html
	 */
	public boolean isValid_stack3(String s) {
		Stack<Character> stack = new Stack<>();
		for (char c : s.toCharArray()) {
			if (c == 'c') {
				if (stack.isEmpty() || stack.pop() != 'b')
					return false;
				if (stack.isEmpty() || stack.pop() != 'a')
					return false;
			} else {
				stack.push(c);
			}
		}
		return stack.isEmpty();
	}

	/**
	 * Accepted --- 15ms 39.1 MB 75.26% stack 只对字母c做处理
	 * http://www.noteanddata.com/leetcode-1003-Check-If-Word-Is-Valid-After-Substitutions-java-solution-note.html
	 * https://www.cnblogs.com/lz87/p/10468580.html
	 */
	public boolean isValid_stack2(String S) {
		Stack<Character> stack = new Stack<>();
		for (char ch : S.toCharArray()) {
			if (ch == 'c') {
				if (stack.size() < 2 || stack.pop() != 'b' || stack.pop() != 'a') {
					return false;
				}
			} else {
				stack.push(ch);
			}
		}
		return stack.isEmpty();
	}

	/**
	 * Accepted --- 13ms 38.5 MB 81.39% StringBuilder 字符串拼接 similar to stack 2
	 * http://www.noteanddata.com/leetcode-1003-Check-If-Word-Is-Valid-After-Substitutions-java-solution-note.html
	 */
	public boolean isValid_StringBuilder(String S) {
		if (S.length() < 3) {
			return false;
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < S.length(); ++i) {
			char ch = S.charAt(i);
			if (ch == 'c') {
				if (sb.length() < 2) {
					return false;
				}
				if (sb.charAt(sb.length() - 1) != 'b' || sb.charAt(sb.length() - 2) != 'a') {
					return false;
				}
				sb.deleteCharAt(sb.length() - 1);
				sb.deleteCharAt(sb.length() - 1);
			} else {
				sb.append(S.charAt(i));
			}
		}
		return sb.length() == 0;
	}

	/**
	 * Accepted --- 21ms 38.5MB 33.54% brute force 暴力解
	 * http://www.noteanddata.com/leetcode-1003-Check-If-Word-Is-Valid-After-Substitutions-java-solution-note.html
	 * https://blog.csdn.net/fuxuemingzhu/article/details/88089110
	 */
	public boolean isValid_brute_force(String S) {
		while (S.contains("abc")) {
			S = S.replaceAll("abc", "");
		}
		return S.length() == 0;
	}

	/**
	 * Accepted --- 21ms 38.6 MB 33.54% brute force
	 * https://www.cnblogs.com/lz87/p/10468580.html
	 */
	public boolean isValid_brute_force2(String S) {
		String before = S, after = null;
		while (!before.equals("")) {
			after = before.replaceAll("abc", "");
			if (after.length() == before.length()) {
				return false;
			}
			before = after;
		}
		return true;
	}

	/**
	 * Accepted --- 23ms 38.1 MB 23.31% 递归 brute force
	 * http://www.noteanddata.com/leetcode-1003-Check-If-Word-Is-Valid-After-Substitutions-java-solution-note.html
	 */
	public boolean isValid_brute_force_recursion(String S) {
		if (S.length() < 3)
			return false;
		String t = S.replaceAll("abc", "");
		return t.length() == 0 || (t.length() != S.length() && isValid_brute_force_recursion(t));
	}

	public static void main(String[] args) {
		M_1003_CheckIfWordIsValidAfterSubstitutions test = new M_1003_CheckIfWordIsValidAfterSubstitutions();

		String s1 = "aabcbc";
		String s2 = "abcabcababcc";
		String s3 = "abccba";
		String s4 = "cababc";

		System.out.println(test.isValid_stack1(s1)); // true "a" + "abc" + "bc" = "aabcbc"
		System.out.println(test.isValid_stack1(s2)); // true "abcabcab" + "abc" + "c" = "abcabcababcc"
		System.out.println(test.isValid_stack1(s3)); // false
		System.out.println(test.isValid_stack1(s4)); // false

		// second best
		System.out.println("");
		System.out.println(test.isValid_stack2(s1));
		System.out.println(test.isValid_stack2(s2));
		System.out.println(test.isValid_stack2(s3));
		System.out.println(test.isValid_stack2(s4));

		System.out.println("");
		System.out.println(test.isValid_stack3(s1));
		System.out.println(test.isValid_stack3(s2));
		System.out.println(test.isValid_stack3(s3));
		System.out.println(test.isValid_stack3(s4));

		// best
		System.out.println("");
		System.out.println(test.isValid_StringBuilder(s1));
		System.out.println(test.isValid_StringBuilder(s2));
		System.out.println(test.isValid_StringBuilder(s3));
		System.out.println(test.isValid_StringBuilder(s4));

		System.out.println("");
		System.out.println(test.isValid_brute_force(s1));
		System.out.println(test.isValid_brute_force(s2));
		System.out.println(test.isValid_brute_force(s3));
		System.out.println(test.isValid_brute_force(s4));

		System.out.println("");
		System.out.println(test.isValid_brute_force2(s1));
		System.out.println(test.isValid_brute_force2(s2));
		System.out.println(test.isValid_brute_force2(s3));
		System.out.println(test.isValid_brute_force2(s4));

		System.out.println("");
		System.out.println(test.isValid_brute_force_recursion(s1));
		System.out.println(test.isValid_brute_force_recursion(s2));
		System.out.println(test.isValid_brute_force_recursion(s3));
		System.out.println(test.isValid_brute_force_recursion(s4));

	}

}
