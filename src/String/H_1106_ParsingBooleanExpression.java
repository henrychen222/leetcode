/**
 * 1.4 evening
 * https://leetcode.com/problems/parsing-a-boolean-expression/
 */
package String;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Stack;

public class H_1106_ParsingBooleanExpression {

	private int s;
	private char[] exp;

	/**
	 * Accepted --- 0ms 37.7 MB 100.00%
	 * https://zxi.mytechroad.com/blog/recursion/leetcode-1106-parsing-a-boolean-expression/
	 */
	public boolean parseBoolExpr(String expression) {
		exp = expression.toCharArray();
		s = 0;
		return parse() == 1;
	}

	private int parse() {
		char ch = exp[s++];
		if (ch == 't')
			return 1;
		if (ch == 'f')
			return 0;
		if (ch == '!') {
			++s;
			int ans = 1 - parse();
			++s;
			return ans;
		}
		boolean is_and = (ch == '&');
		int ans = is_and ? 1 : 0;
		++s;
		while (true) {
			if (is_and)
				ans &= parse();
			else
				ans |= parse();
			if (exp[s++] == ')')
				break;
		}
		return ans;
	}

	/////////////////////////////////// Recursion /////////////////////////////////
	/**
	 * Accepted --- 1ms 38 MB 96.23%
	 * https://www.robberphex.com/parsing-a-boolean-expression/
	 * https://www.codercto.com/a/91441.html (same)
	 * 
	 * 直接递归(Recursion)解释表达式就好，每层负责解释上述的一个规则就可以 优化：实现表达式惰性求值.
	 */
	public boolean parseBoolExpr_recursion1(String expression) {
		return parseBoolExpr(expression, 0).res;
	}

	class Result {
		boolean res; // 表达式结果
		int end; // 下次解析的起始位置

		Result(boolean res, int end) {
			this.res = res;
			this.end = end;
		}
	}

	private Result parseBoolExpr(String expression, int start) {
		if (expression.charAt(start) == 'f') {
			return new Result(false, start + 1);
		} else if (expression.charAt(start) == 't') {
			return new Result(true, start + 1);
		} else if (expression.charAt(start) == '!') {
			Result result = parseBoolExpr(expression, start + 2);
			result.res = !result.res;
			result.end++;
			return result;
		} else if (expression.charAt(start) == '&') {
			Result finalResult = new Result(true, 0), result;
			start++;
			do {
				result = parseBoolExpr(expression, start + 1);
				if (!result.res) {
					// 在这儿可以实现惰性求值
					finalResult.res = false;
				}
				start = result.end;
				// 如果是逗号，继续解析
			} while (expression.charAt(result.end) == ',');
			finalResult.end = result.end + 1;
			return finalResult;
		} else if (expression.charAt(start) == '|') {
			Result finalResult = new Result(false, 0), result;
			start++;
			do {
				result = parseBoolExpr(expression, start + 1);
				if (result.res) {
					// 在这儿可以实现惰性求值
					finalResult.res = true;
				}
				start = result.end;
			} while (expression.charAt(result.end) == ',');
			finalResult.end = result.end + 1;
			return finalResult;
		} else {
			throw new RuntimeException();
		}
	}

	/**
	 * Accepted --- 5ms 37.8MB 76.40%
	 * https://leetcode.com/problems/parsing-a-boolean-expression/discuss/455770/Java%3A-simple-recursion-with-queue.-Also-the-pattern-explained
	 * Recursion with Queue offer()/add(), poll()/remove(), peek()/element() ---
	 * mostly use, offer(), poll(), peek()
	 */
	public boolean parseBoolExpr_recursion2(String expression) {
		if (expression.length() == 1) {
			return expression.equals("t");
		}
		LinkedList<Character> queue = new LinkedList<>();
		for (char c : expression.toCharArray()) {
			queue.offer(c);
		}
		return process(queue, queue.poll());
	}

	boolean process(LinkedList<Character> queue, char operator) {
		boolean result = operator == '|' ? false : true;
		while (!queue.isEmpty()) {
			char c = queue.poll();
			if (c == ',' || c == '(') { // ignore not meaningful characters
				continue;
			} else if (c == ')') {
				return operator != '!' ? result : !result;
			} else {
				boolean r;
				if (c == '!' || c == '&' || c == '|') {
					r = process(queue, c);
				} else { // 't' and 'f'
					r = c == 't';
				}
				if (operator == '&')
					result &= r;
				else if (operator == '|')
					result |= r;
				else
					result = r;
			}
		}
		return result; // unreachable
	}

	/**
	 * Accepted --- 4ms 38.5 MB 81.65%
	 * https://leetcode.com/problems/parsing-a-boolean-expression/discuss/444982/Java-Recursive-Solution-Beats-81
	 */
	public boolean parseBoolExpr_recursion3(String expression) {
		return parse(expression, ' ');
	}

	private boolean parse(String expr, char op) {
		boolean val = false;
		for (int i = 0; i < expr.length(); i++) {
			if (expr.charAt(i) == '!') {
				StringBuilder sb = new StringBuilder();
				i++;
				int d = 0;
				do {
					char c = expr.charAt(i++);
					if (c == '(')
						d++;
					else if (c == ')')
						d--;
					sb.append(c);
				} while (d != 0);
				val = parse(sb.toString(), '!');
				if (op == '|' && val)
					return true;
				if (op == '&' && !val)
					return false;
				if (op == '!')
					return !val;
			} else if (expr.charAt(i) == '&') {
				StringBuilder sb = new StringBuilder();
				i++;
				int d = 0;
				do {
					char c = expr.charAt(i++);
					if (c == '(')
						d++;
					else if (c == ')')
						d--;
					sb.append(c);
				} while (d != 0);
				val = parse(sb.toString(), '&');
				if (op == '|' && val)
					return true;
				if (op == '&' && !val)
					return false;
				if (op == '!')
					return !val;
			} else if (expr.charAt(i) == '|') {
				StringBuilder sb = new StringBuilder();
				i++;
				int d = 0;
				do {
					char c = expr.charAt(i++);
					if (c == '(')
						d++;
					else if (c == ')')
						d--;
					sb.append(c);
				} while (d != 0);
				val = parse(sb.toString(), '|');
				if (op == '|' && val)
					return true;
				if (op == '&' && !val)
					return false;
				if (op == '!')
					return !val;
			} else if (expr.charAt(i) == 't') {
				if (op == '|')
					return true;
				if (op == '!')
					return false;
				if (!(op == '&'))
					return true;
			} else if (expr.charAt(i) == 'f') {
				if (op == '&')
					return false;
				if (op == '!')
					return true;
				if (!(op == '|'))
					return false;
			}
		}
		if (op == '&')
			return true;
		if (op == '|')
			return false;
		return val;
	}

	/////////////////////////////////// Stack /////////////////////////////////
	/**
	 * Accepted --- 13ms 37.1 MB 53.18%
	 * 
	 * https://leetcode.com/problems/parsing-a-boolean-expression/discuss/466489/Dijkstra's-Two-Stack-Algorithm
	 * Dijkstra's Two-Stack Algorithm
	 * 
	 * https://docs.oracle.com/javase/7/docs/api/java/util/Stack.html (push(),pop(),
	 * peek())
	 */
	public boolean parseBoolExpr_twoStack_Dijkstra(String expression) {
		Stack<Character> operators = new Stack<>();
		Stack<Character> operands = new Stack<>();

		for (char c : expression.toCharArray()) {
			if (c == 't' || c == 'f' || c == '(') {
				operands.push(c);
			} else if (c == '!' || c == '&' || c == '|') {
				operators.push(c);
			} else if (c == ')') {
				char operator = operators.pop();
				boolean result = operands.pop() == 't' ? true : false;
				if (operator == '!') {
					result = !result;
				}
				while (operands.peek() != '(') {
					boolean operand = operands.pop() == 't' ? true : false;
					if (operator == '&') {
						result = result && operand;
					} else {
						result = result || operand;
					}
				}
				operands.pop();
				operands.push(result == true ? 't' : 'f');
			}
		}
		return operands.peek() == 't' ? true : false;
	}

	/**
	 * Accepted --- 12ms 37.7 MB 55.81%
	 * https://leetcode.com/problems/parsing-a-boolean-expression/discuss/448401/Java-straightforward-solution-using-stack
	 */
	public boolean parseBoolExpr_stack2(String expression) {
		Stack<Character> stack = new Stack<>();
		char[] arr = expression.toCharArray();
		for (int i = 0; i < arr.length; i++) {
			char cur = arr[i];
			if (isOperator(cur) || cur == 't' || cur == 'f') {
				stack.push(cur);
			} else if (cur == ')') {
				int[] vals = new int[2];
				while (!isOperator(stack.peek())) {
					char v = stack.pop();
					if (v == 't') {
						vals[0]++;
					} else {
						vals[1]++;
					}
				}
				char opr = stack.pop();
				char res = evaluateExp(opr, vals);
				stack.push(res);
			}
		}
		return stack.peek() == 't' ? true : false;
	}

	private char evaluateExp(char opr, int[] vals) {
		if (opr == '|') {
			return vals[0] > 0 ? 't' : 'f';
		}
		if (opr == '!') {
			return vals[1] > 0 ? 't' : 'f';
		}
		return vals[1] > 0 ? 'f' : 't';
	}

	private boolean isOperator(char cur) {
		return cur == '!' || cur == '&' || cur == '|';
	}

	/**
	 * Accepted --- 17ms 36.7 MB 32.21%
	 * https://leetcode.com/problems/parsing-a-boolean-expression/discuss/443031/Java-Stack-Solution
	 */
	public boolean parseBoolExpr_stack3(String expression) {
		Stack<Character> s = new Stack<>();
		HashSet<Character> hs = new HashSet<>();
		for (char c : expression.toCharArray()) {
			if (c == ')') {
				hs.clear();
				while (s.peek() != '(') {
					hs.add(s.pop());
				}
				s.pop();
				char op = s.pop();
				switch (op) {
				case '!':
					s.push(hs.contains('f') ? 't' : 'f');
					break;
				case '|':
					s.push(hs.contains('t') ? 't' : 'f');
					break;
				case '&':
					s.push(hs.contains('f') ? 'f' : 't');
					break;
				}
			} else {
				s.push(c);
			}
		}
		return s.pop() == 't';
	}

	public static void main(String[] args) {
		H_1106_ParsingBooleanExpression test = new H_1106_ParsingBooleanExpression();

		String expression1 = "!(f)";
		String expression2 = "|(f,t)";
		String expression3 = "&(t,f)";
		String expression4 = "|(&(t,f,t),!(t))";

		System.out.println(test.parseBoolExpr(expression1)); // true
		System.out.println(test.parseBoolExpr(expression2)); // true
		System.out.println(test.parseBoolExpr(expression3)); // false
		System.out.println(test.parseBoolExpr(expression4)); // false

		System.out.println("\n------ Recursion 1 ------ ");
		System.out.println(test.parseBoolExpr_recursion1(expression1));
		System.out.println(test.parseBoolExpr_recursion1(expression2));
		System.out.println(test.parseBoolExpr_recursion1(expression3));
		System.out.println(test.parseBoolExpr_recursion1(expression4));

		System.out.println("\n------ Recursion 2 ------ ");
		System.out.println(test.parseBoolExpr_recursion2(expression1));
		System.out.println(test.parseBoolExpr_recursion2(expression2));
		System.out.println(test.parseBoolExpr_recursion2(expression3));
		System.out.println(test.parseBoolExpr_recursion2(expression4));

		System.out.println("\n------ Recursion 3 ------ ");
		System.out.println(test.parseBoolExpr_recursion3(expression1));
		System.out.println(test.parseBoolExpr_recursion3(expression2));
		System.out.println(test.parseBoolExpr_recursion3(expression3));
		System.out.println(test.parseBoolExpr_recursion3(expression4));

		System.out.println("\n------ Two Stack Dijkstra ------ ");
		System.out.println(test.parseBoolExpr_twoStack_Dijkstra(expression1));
		System.out.println(test.parseBoolExpr_twoStack_Dijkstra(expression2));
		System.out.println(test.parseBoolExpr_twoStack_Dijkstra(expression3));
		System.out.println(test.parseBoolExpr_twoStack_Dijkstra(expression4));

		System.out.println("\n------ Stack 2 ------ ");
		System.out.println(test.parseBoolExpr_stack2(expression1));
		System.out.println(test.parseBoolExpr_stack2(expression2));
		System.out.println(test.parseBoolExpr_stack2(expression3));
		System.out.println(test.parseBoolExpr_stack2(expression4));

		System.out.println("\n------ Stack 3 ------ ");
		System.out.println(test.parseBoolExpr_stack3(expression1));
		System.out.println(test.parseBoolExpr_stack3(expression2));
		System.out.println(test.parseBoolExpr_stack3(expression3));
		System.out.println(test.parseBoolExpr_stack3(expression4));

	}

}
