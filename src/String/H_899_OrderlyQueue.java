/**
 * 1.10 night created
 * 
 * 1.19 evening
 * https://leetcode.com/problems/orderly-queue/
 */
package String;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.TreeSet;

public class H_899_OrderlyQueue {

	/**
	 * Accepted --- 10ms 45.1 MB 5.59%
	 * 
	 * https://www.codeleading.com/article/24951602709/
	 * https://www.bbsmax.com/A/l1dyqGYgde/
	 * https://leetcode.com/problems/orderly-queue/discuss/165857/Java-Simple-Solution-12-ms
	 */
	public String orderlyQueue(String S, int K) {
		// When K >= 2, you can swap any two character in the string
		if (K >= 2) {
			char[] arr = S.toCharArray();
			Arrays.sort(arr);
			return new String(arr);
		}

		// When K = 1 slideString() in method 2
		String res = S;
		for (int i = 0; i < S.length(); ++i) {
			String cur = S.substring(1) + S.charAt(0);
			if (cur.compareTo(res) < 0)
				res = cur;
			S = cur;
		}
		return res;
	}

	/**
	 * Accepted --- 9ms 45 MB 5.59%
	 * 
	 * https://massivealgorithms.blogspot.com/2018/11/leetcode-899-orderly-queue.html
	 * https://leetcode.com/problems/orderly-queue/discuss/165857/Java-Simple-Solution-12-ms
	 */
	public String orderlyQueue2(String S, int K) {
		if (K == 1)
			return slideString(S);

		// When K >= 2, you can swap any two character in the string
		char[] arr = S.toCharArray();
		Arrays.sort(arr);
		return new String(arr);
	}

	public String slideString(String S) {
		String res = S, cur = S;
		for (int i = 0; i < S.length(); i++) {
			cur = cur.substring(1) + cur.charAt(0);
			if (cur.compareTo(res) < 0)
				res = cur;
		}
		return res;
	}

	/**
	 * Accepted --- 3ms 44.9MB 16.78%
	 * 
	 * https://massivealgorithms.blogspot.com/2018/11/leetcode-899-orderly-queue.html
	 * https://leetcode.com/problems/orderly-queue/discuss/165878/C%2B%2BJavaPython-Sort-String-or-Rotate-String
	 */
	public String orderlyQueue3(String S, int K) {
		// K = 1, only rotations of S are possible, the answer is the smallest rotation.
		if (K == 1) {
			String ans = S;
			for (int i = 0; i < S.length(); ++i) {
				String T = S.substring(i) + S.substring(0, i);
				if (T.compareTo(ans) < 0)
					ans = T;
			}
			return ans;
		} else {
			// K > 1 , any permutation of S is possible, the answer is the letters of S
			// written in lexicographic order.
			char[] ca = S.toCharArray();
			Arrays.sort(ca);
			return new String(ca);
		}
	}

	/**
	 * Time Limit Exceeded
	 * 
	 * https://github.com/Cee/Leetcode/blob/master/899%20-%20Orderly%20Queue.java
	 */
	TreeSet<String> set;

	public String orderlyQueue4(String S, int K) {
		int n = S.length();
		if (K > n / 2) {
			char[] tempArray = S.toCharArray();
			Arrays.sort(tempArray);
			return new String(tempArray);
		}

		set = new TreeSet<>();
		Deque<String> q = new ArrayDeque<>();
		q.add(S);
		while (!q.isEmpty()) {
			String s = q.poll();
			for (int i = 0; i < K; i++) {
				String t = s.substring(0, i) + s.substring(i + 1) + s.charAt(i);
				if (!set.contains(t)) {
					set.add(t);
					q.add(t);
				}
			}
		}
		return set.first();
	}

	public static void main(String[] args) {
		H_899_OrderlyQueue test = new H_899_OrderlyQueue();

		String S = "cba";
		int K = 1;
		String S2 = "baaca";
		int K2 = 3;

		System.out.println(test.orderlyQueue(S, K)); // "acb"
		System.out.println(test.orderlyQueue(S2, K2)); // "aaabc"

		System.out.println(test.orderlyQueue2(S, K));
		System.out.println(test.orderlyQueue2(S2, K2));
		
		System.out.println(test.orderlyQueue3(S, K));
		System.out.println(test.orderlyQueue3(S2, K2));
		
		System.out.println(test.orderlyQueue2(S, K));
		System.out.println(test.orderlyQueue2(S2, K2));

	}

}
