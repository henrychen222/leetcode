/**
 * 1.4 evening
 * https://leetcode.com/problems/binary-string-with-substrings-representing-1-to-n/
 */
package String;

import java.util.HashSet;
import java.util.Set;

public class M_1016_BinaryStringWithSubstringsRepresenting1ToN {

	/**
	 * Accepted --- 0ms 34.3 MB 100.00%
	 * https://blog.csdn.net/M_sdn/article/details/88774652
	 */
	public boolean queryString(String S, int N) {
		for (int i = 1; i <= N; i++) {
			if (!S.contains(Integer.toBinaryString(i))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Accepted --- 0ms 34.1 MB 100.00%
	 * https://xingxingpark.com/Leetcode-1016-Binary-String-With-Substrings-Representing-1-To-N/
	 */
	public boolean queryString2(String S, int N) {
		for (int i = N; i > N / 2; --i)
			if (!S.contains(Integer.toBinaryString(i)))
				return false;
		return true;
	}

	/**
	 * Accepted --- 32ms 37.1 MB 5.45%
	 * https://massivealgorithms.blogspot.com/2019/04/leetcode-1016-binary-string-with.html
	 */
	public boolean queryString3(String S, int N) {
		if (N > S.length() * 32)
			return false;
		Set<Integer> set = new HashSet<>();
		for (int i = 0; i < S.length(); ++i) {
			for (int len = 1; len <= 32; ++len) {
				if (i + len <= S.length()) {
					String sub = S.substring(i, i + len);
					try {
						int value = Integer.parseInt(sub, 2);
						set.add(value);
					} catch (Exception e) {
					}
				}
			}
		}
		for (int i = 1; i <= N; ++i) {
			if (!set.contains(i)) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		M_1016_BinaryStringWithSubstringsRepresenting1ToN test = new M_1016_BinaryStringWithSubstringsRepresenting1ToN();

		String S1 = "0110";
		int N1 = 3;
		String S2 = "0110";
		int N2 = 4;

		System.out.println(test.queryString(S1, N1)); // true
		System.out.println(test.queryString(S2, N2)); // false

		System.out.println("");
		System.out.println(test.queryString(S1, N1));
		System.out.println(test.queryString(S2, N2));

		System.out.println("");
		System.out.println(test.queryString3(S1, N1));
		System.out.println(test.queryString3(S2, N2));

	}

}
