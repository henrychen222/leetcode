/**
 * 12.28 evening 
 * https://leetcode.com/problems/replace-the-substring-for-balanced-string/
 */
package String;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class M_1234_ReplaceSubstringBalancedString {

	// Accepted --- 14ms, 37.9MB, 61.35%
	/**
	 * sliding window--double pointer
	 * https://www.cnblogs.com/Dylan-Java-NYC/p/11968679.html
	 * https://leetcode.jp/leetcode-1234-replace-the-substring-for-balanced-string-%E8%A7%A3%E9%A2%98%E6%80%9D%E8%B7%AF%E5%88%86%E6%9E%90/
	 */
	public int balancedString(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}

		int n = s.length();
		int k = n / 4;
		int[] count = new int[256];
		for (char c : s.toCharArray()) {
			count[c]++;
		}

		int walker = 0;
		int runner = 0;
		int res = n;
		while (runner < n) {
			count[s.charAt(runner++)]--;
			while (walker < n && count['Q'] <= k && count['W'] <= k && count['R'] <= k && count['E'] <= k) {
				res = Math.min(res, runner - walker);
				count[s.charAt(walker++)]++;
			}
		}

		return res;
	}

	// Accepted --- 52ms, 36.1MB, 12.70%.
	/**
	 * Binary Search O(N * log N) runtime, N is the input string's length
	 * https://www.cnblogs.com/lz87/p/11711860.html
	 * http://www.voidcn.com/article/p-qolhjauk-bza.html
	 */
	public int balancedString2(String s) {
		idxMap.put('Q', 0);
		idxMap.put('W', 1);
		idxMap.put('E', 2);
		idxMap.put('R', 3);
		int[] cnt = new int[4];
		for (int i = 0; i < s.length(); i++) {
			cnt[idxMap.get(s.charAt(i))]++;
		}
		for (int i = 0; i < cnt.length; i++) {
			cnt[i] -= (s.length() / 4);
		}
		int left = 0, right = s.length();
		while (left < right - 1) {
			int mid = left + (right - left) / 2;
			int[] cntCopy = Arrays.copyOf(cnt, cnt.length);
			if (check(s, cntCopy, mid)) {
				right = mid;
			} else {
				left = mid + 1;
			}
		}
		int[] cntCopy = Arrays.copyOf(cnt, cnt.length);
		if (check(s, cntCopy, left)) {
			return left;
		}
		return right;
	}

	private Map<Character, Integer> idxMap = new HashMap<>();

	private boolean check(String s, int[] cnt, int window) {
		for (int i = 0; i < window; i++) {
			cnt[idxMap.get(s.charAt(i))]--;
		}
		if (cnt[0] <= 0 && cnt[1] <= 0 && cnt[2] <= 0 && cnt[3] <= 0) {
			return true;
		}
		for (int i = window; i < s.length(); i++) {
			cnt[idxMap.get(s.charAt(i - window))]++;
			cnt[idxMap.get(s.charAt(i))]--;
			if (cnt[0] <= 0 && cnt[1] <= 0 && cnt[2] <= 0 && cnt[3] <= 0) {
				return true;
			}
		}
		return false;
	}

	// Accepted --- 18 ms 36.9 MB 42.00%
	/**
	 * Sliding window + two pointers, O(N) runtime
	 * https://www.cnblogs.com/lz87/p/11711860.html
	 * http://www.voidcn.com/article/p-qolhjauk-bza.html
	 */
	public int balancedString3(String s) {
		Map<Character, Integer> idxMap = new HashMap<>();
		idxMap.put('Q', 0);
		idxMap.put('W', 1);
		idxMap.put('E', 2);
		idxMap.put('R', 3);
		int[] cnt = new int[4];
		Arrays.fill(cnt, -s.length() / 4);

		for (int i = 0; i < s.length(); i++) {
			cnt[idxMap.get(s.charAt(i))]++;
		}

		if (cnt[0] == 0 && cnt[1] == 0 && cnt[2] == 0 && cnt[3] == 0) {
			return 0;
		}

		int l = 0, r = 0, res = s.length();
		while (r < s.length()) {
			while (r < s.length() && (cnt[0] > 0 || cnt[1] > 0 || cnt[2] > 0 || cnt[3] > 0)) {
				cnt[idxMap.get(s.charAt(r))]--;
				r++;
			}
			while (l <= r && cnt[0] <= 0 && cnt[1] <= 0 && cnt[2] <= 0 && cnt[3] <= 0) {
				cnt[idxMap.get(s.charAt(l))]++;
				l++;
			}
			res = Math.min(res, r - l + 1);
		}
		return res;
	}

	public static void main(String[] args) {
		M_1234_ReplaceSubstringBalancedString test = new M_1234_ReplaceSubstringBalancedString();

		String s1 = "QWER";
		String s2 = "QQWE";
		String s3 = "QQQW";
		String s4 = "QQQQ";
		System.out.println(test.balancedString(s1)); // 0
		System.out.println(test.balancedString(s2)); // 1
		System.out.println(test.balancedString(s3)); // 2
		System.out.println(test.balancedString(s4)); // 3

		System.out.println("");
		System.out.println(test.balancedString2(s1)); // 0
		System.out.println(test.balancedString2(s2)); // 1
		System.out.println(test.balancedString2(s3)); // 2
		System.out.println(test.balancedString2(s4)); // 3

		System.out.println("");
		System.out.println(test.balancedString3(s1)); // 0
		System.out.println(test.balancedString3(s2)); // 1
		System.out.println(test.balancedString3(s3)); // 2
		System.out.println(test.balancedString3(s4)); // 3

	}

}
