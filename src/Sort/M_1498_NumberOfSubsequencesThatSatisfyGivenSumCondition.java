/**
 * 7.8 morning
 * https://leetcode.com/problems/number-of-subsequences-that-satisfy-the-given-sum-condition/
 */

package Sort;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class M_1498_NumberOfSubsequencesThatSatisfyGivenSumCondition {

    // time limit exceed
    public int numSubseq(int[] nums, int target) {
        int cnt = 0;
        int n = nums.length;
        int N = (int) Math.pow(2, n);
        for (int i = 1; i < N; i++) {
            int min = Integer.MAX_VALUE;
            int max = Integer.MIN_VALUE;
            for (int j = 0; j < n; j++) {
                if (BigInteger.valueOf(i).testBit(j)) {
                    min = Math.min(min, nums[j]);
                    max = Math.max(max, nums[j]);
                }
            }
            if ((min + max) <= target) {
                cnt++;
            }
        }
        return cnt % 1000000007;
    }

    public int numSubseq2(int[] nums, int target) {
        int cnt = 0;
        int n = nums.length;
        int N = (int) Math.pow(2, n);
        for (int i = 1; i < N; i++) {
            List<Integer> data = new ArrayList<>();
            for (int j = 0; j < n; j++) {
                if (BigInteger.valueOf(i).testBit(j)) {
                    data.add(nums[j]);
                }
            }
            Collections.sort(data);
            // System.out.println(data);
            if (data.get(0) + data.get(data.size() - 1) <= target) {
                cnt++;
            }
        }
        return cnt % 1000000007;
    }

    public static void main(String[] args) {
        int[] nums = new int[] { 3, 5, 6, 7 };
        int target = 9;
        int[] nums2 = new int[] { 3, 3, 6, 8 };
        int target2 = 10;
        int[] nums3 = new int[] { 2, 3, 3, 4, 6, 7 };
        int target3 = 12;
        int[] nums4 = new int[] { 5, 2, 4, 1, 7, 6, 8 };
        int target4 = 16;
        int[] nums_debug1 = new int[] { 7, 10, 7, 3, 7, 5, 4 };
        int target_debug1 = 12;
        int[] nums_debug2 = new int[] { 14, 4, 6, 6, 20, 8, 5, 6, 8, 12, 6, 10, 14, 9, 17, 16, 9, 7, 14, 11, 14, 15, 13,
                11, 10, 18, 13, 17, 17, 14, 17, 7, 9, 5, 10, 13, 8, 5, 18, 20, 7, 5, 5, 15, 19, 14 };
        int target_debug2 = 22;
        M_1498_NumberOfSubsequencesThatSatisfyGivenSumCondition test = new M_1498_NumberOfSubsequencesThatSatisfyGivenSumCondition();
        System.out.println(test.numSubseq(nums, target)); // 4
        System.out.println(test.numSubseq(nums2, target2)); // 6
        System.out.println(test.numSubseq(nums3, target3)); // 61
        System.out.println(test.numSubseq(nums4, target4)); // 127
        System.out.println(test.numSubseq(nums_debug1, target_debug1)); // 56
        System.out.println(test.numSubseq(nums_debug2, target_debug2)); // 56

    }

}