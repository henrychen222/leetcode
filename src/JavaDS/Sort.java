package JavaDS;

public class Sort {

	public static int[] bubble_sort(int array[]) {
		int n = array.length;
		for (int i = 0; i < n - 1; i++) {
			for (int j = 0; j < n - i - 1; j++) {
				if (array[j] > array[j + 1]) {
					int temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
				}
			}
		}
		return array;
	}

	public static void printArray(int arr[]) {
		int n = arr.length;
		for (int i = 0; i < n; i++)
			System.out.print(arr[i] + " ");
	}
     
	public static void main(String[] args) {
		int[] new_arr = new int[] { 19, 2, 31, 45, 6, 11, 121, 27 };
		bubble_sort(new_arr);
//		System.out.println(new_arr);
		printArray(new_arr);

	}

}
