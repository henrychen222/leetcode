/**
 * 5.1 night
 * https://leetcode.com/problems/last-stone-weight/
 */
package Greedy;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;

public class E_1046_LastStoneWeight {

    /**
     * http://gaozhipeng.me/posts/1046-Last-Stone-Weight/ Accepted --- 0ms 36.9 MB
     * 100.00%
     */
    public int lastStoneWeight(int[] stones) {
        while (stones.length != 1) {
            Arrays.sort(stones);
            stones[stones.length - 2] = stones[stones.length - 1] - stones[stones.length - 2];
            stones = Arrays.copyOf(stones, stones.length - 1);
        }
        return stones[0];
    }

    /**
     * https://www.cnblogs.com/Dylan-Java-NYC/p/11785085.html Accepted --- 1ms 37MB
     * 86.78%
     */
    public int lastStoneWeight_cnblog(int[] stones) {
        if (stones == null || stones.length == 0) {
            return 0;
        }
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(Collections.reverseOrder());
        for (int stone : stones) {
            maxHeap.add(stone);
        }
        while (maxHeap.size() > 1) {
            int x = maxHeap.poll();
            int y = maxHeap.poll();
            int diff = x - y;
            if (diff > 0) {
                maxHeap.add(diff);
            }
        }
        return maxHeap.isEmpty() ? 0 : maxHeap.peek();
    }

    /**
     * https://massivealgorithms.blogspot.com/2019/06/leetcode-1046-last-stone-weight.html
     * Accepted --- 1ms 36.5 MB 86.78%
     */
    public int lastStoneWeight_massivealgorithms1(int[] stones) {
        PriorityQueue<Integer> pq = new PriorityQueue<>((a, b) -> b - a);
        for (int stone : stones) {
            pq.offer(stone);
        }
        for (int i = 0; i < stones.length - 1; ++i) {
            pq.offer(pq.poll() - pq.poll());
        }
        return pq.poll();
    }

    /**
     * https://massivealgorithms.blogspot.com/2019/06/leetcode-1046-last-stone-weight.html
     * Accepted --- 1ms 36.7MB 86.78%
     */
    public int lastStoneWeight_massivealgorithms2(int[] stones) {
        PriorityQueue<Integer> pq = new PriorityQueue<>(Comparator.reverseOrder());
        for (int stone : stones) {
            pq.offer(stone);
        }
        while (pq.size() > 1) {
            pq.offer(pq.poll() - pq.poll());
        }
        return pq.peek();
    }

    /**
     * https://blog.csdn.net/fuxuemingzhu/article/details/91348483 Accepted --- 1ms
     * 37.1 MB 86.78%
     */
    public int lastStoneWeight_csdn(int[] stones) {
        PriorityQueue<Integer> pq = new PriorityQueue<>((a, b) -> b - a);
        // PriorityQueue<Integer> pq = new PriorityQueue<>(Comparator.reverseOrder());
        for (int stone : stones) {
            pq.offer(stone); // c++ push()
        }
        while (pq.size() >= 2) {
            int a = pq.peek();
            pq.poll(); // c++ pop()
            if (!pq.isEmpty()) {
                int b = pq.peek(); // c++ top()
                pq.poll();
                if (a != b) {
                    pq.offer(Math.abs(a - b));
                }
            }
        }
        return pq.isEmpty() ? 0 : pq.peek();
    }

    public static void main(String[] args) {
        E_1046_LastStoneWeight test = new E_1046_LastStoneWeight();
        int[] stones = new int[] { 2, 7, 4, 1, 8, 1 };
        int[] debug1 = new int[] { 2, 2 };
        System.out.println(test.lastStoneWeight(stones)); // 1
        System.out.println(test.lastStoneWeight(debug1)); // 0

        System.out.println("");
        System.out.println(test.lastStoneWeight_cnblog(stones)); // 1
        System.out.println(test.lastStoneWeight_cnblog(debug1)); // 2 ???

        System.out.println("");
        System.out.println(test.lastStoneWeight_massivealgorithms1(stones)); // 1
        System.out.println(test.lastStoneWeight_massivealgorithms1(debug1)); // 2 ???

        System.out.println("");
        System.out.println(test.lastStoneWeight_massivealgorithms2(stones)); // 1
        System.out.println(test.lastStoneWeight_massivealgorithms2(debug1)); // 2 ???

        System.out.println("");
        System.out.println(test.lastStoneWeight_csdn(stones)); // 1
        System.out.println(test.lastStoneWeight_csdn(debug1)); // 2 ???
    }

}
