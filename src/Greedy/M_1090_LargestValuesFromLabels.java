// 5.24 night 5.27 night debug
package Greedy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class M_1090_LargestValuesFromLabels {

    void print(List<int[]> list) {
        for (int[] is : list) {
            System.out.print(Arrays.toString(is) + " ");
        }
        System.out.println("");
    }

    // http://www.noteanddata.com/leetcode-1090-Largest-Values-From-Labels-java-solution-note.html
    public int largestValsFromLabels(int[] values, int[] labels, int num_wanted, int use_limit) {
        List<int[]> list = new ArrayList<>();
        for (int i = 0; i < values.length; ++i) {
            list.add(new int[] { values[i], labels[i] });
        }
        // print(list);
        Collections.sort(list, new Comparator<int[]>() {
            public int compare(int[] a, int[] b) {
                return b[0] - a[0];
            }
        });
        // print(list);

        Map<Integer, Integer> map = new HashMap<>();
        int sum = 0;
        for (int i = 0; i < list.size() && num_wanted > 0; ++i) {
            int[] item = list.get(i);
            // System.out.println(Arrays.toString(item));
            int value = item[0];
            int label = item[1];
            // System.out.println("value: " + value);
            // System.out.println("label: " + label);
            int labelCount = map.getOrDefault(label, 0);
            // System.out.println(labelCount);
            if (labelCount < use_limit) {
                sum += value;
                num_wanted--;
                map.put(label, labelCount + 1);
            }
        }
        return sum;
    }

    // https://github.com/mickey0524/leetcode/blob/master/1090.Largest-Values-From-Labels.java
    public int largestValsFromLabels2(int[] values, int[] labels, int num_wanted, int use_limit) {
        int length = values.length;
        int[][] nums = new int[length][2];
        for (int i = 0; i < length; i++) {
            nums[i][0] = values[i];
            nums[i][1] = labels[i];
        }
        Arrays.sort(nums, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                return o2[0] - o1[0];
            }
        });

        int res = 0;
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        for (int i = 0; i < length; i++) {
            int curUse = hashMap.getOrDefault(nums[i][1], 0);
            if (curUse < use_limit) {
                res += nums[i][0];
                hashMap.put(nums[i][1], curUse + 1);
                num_wanted--;
                if (num_wanted == 0) {
                    break;
                }
            }
        }
        return res;
    }

    // https://github.com/alberyan/Leetcode-Solution/blob/master/1090.%20Largest%20Values%20From%20Labels.java
    public int largestValsFromLabels3(int[] values, int[] labels, int num_wanted, int use_limit) {
        Map<Integer, Integer> visited = new HashMap<>();
        PriorityQueue<Node> pq = new PriorityQueue<>((Node n1, Node n2) -> n2.value - n1.value);
        for (int i = 0; i < values.length; i++) {
            pq.offer(new Node(values[i], labels[i]));
        }
        int sum = 0;
        int count = 0;
        while (count < num_wanted && !pq.isEmpty()) {
            Node node = pq.poll();
            int num = visited.getOrDefault(node.label, 0);
            if (num >= use_limit)
                continue;
            count++;
            sum += node.value;
            visited.put(node.label, num + 1);
        }
        return sum;
    }

    public static void main(String[] args) {
        int[] values = new int[] { 5, 4, 3, 2, 1 };
        int[] labels = new int[] { 1, 1, 2, 2, 3 };
        int num_wanted = 3;
        int use_limit = 1;

        int[] values2 = new int[] { 5, 4, 3, 2, 1 };
        int[] labels2 = new int[] { 1, 3, 3, 3, 2 };
        int num_wanted2 = 3;
        int use_limit2 = 2;

        int[] values3 = new int[] { 9, 8, 8, 7, 6 };
        int[] labels3 = new int[] { 0, 0, 0, 1, 1 };
        int num_wanted3 = 3;
        int use_limit3 = 1;

        int[] values4 = new int[] { 9, 8, 8, 7, 6 };
        int[] labels4 = new int[] { 0, 0, 0, 1, 1 };
        int num_wanted4 = 3;
        int use_limit4 = 2;

        M_1090_LargestValuesFromLabels test = new M_1090_LargestValuesFromLabels();
        System.out.println(test.largestValsFromLabels(values, labels, num_wanted, use_limit)); // 9
        System.out.println(test.largestValsFromLabels(values2, labels2, num_wanted2, use_limit2)); // 12
        System.out.println(test.largestValsFromLabels(values3, labels3, num_wanted3, use_limit3)); // 16
        System.out.println(test.largestValsFromLabels(values4, labels4, num_wanted4, use_limit4)); // 24

        System.out.println("");
        System.out.println(test.largestValsFromLabels2(values, labels, num_wanted, use_limit));
        System.out.println(test.largestValsFromLabels2(values2, labels2, num_wanted2, use_limit2));
        System.out.println(test.largestValsFromLabels2(values3, labels3, num_wanted3, use_limit3));
        System.out.println(test.largestValsFromLabels2(values4, labels4, num_wanted4, use_limit4));

        System.out.println("");
        System.out.println(test.largestValsFromLabels3(values, labels, num_wanted, use_limit));
        System.out.println(test.largestValsFromLabels3(values2, labels2, num_wanted2, use_limit2));
        System.out.println(test.largestValsFromLabels3(values3, labels3, num_wanted3, use_limit3));
        System.out.println(test.largestValsFromLabels3(values4, labels4, num_wanted4, use_limit4));
    }

}

class Node {
    int value;
    int label;

    public Node(int value, int label) {
        this.value = value;
        this.label = label;
    }
}