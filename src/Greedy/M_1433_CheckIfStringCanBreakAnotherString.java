/**
 * 5.15 night
 * https://leetcode.jp/leetcode-1433-check-if-a-string-can-break-another-string-%E8%A7%A3%E9%A2%98%E6%80%9D%E8%B7%AF%E5%88%86%E6%9E%90/
 */
package Greedy;

import java.util.Arrays;

public class M_1433_CheckIfStringCanBreakAnotherString {

    // wrong code
    public boolean checkIfCanBreak(String s1, String s2) {
        char[] arrS1 = s1.toCharArray();
        char[] arrS2 = s2.toCharArray();
        int[] count1 = new int[26];
        int[] count2 = new int[26];
        for (int i = 0; i < s1.length(); i++) {
            count1[arrS1[i] - 'a']++;
            count2[arrS2[i] - 'a']++;
        }
        return isValid(count1, count2) || isValid(count2, count1);
    }

    boolean isValid(int[] arr1, int[] arr2) {
        int[] count1 = Arrays.copyOf(arr1, arr1.length);
        int[] count2 = Arrays.copyOf(arr2, arr2.length);
        for (int i = 0; i < 26; i++) {
            int c1 = count1[i];
            if (c1 == 0) {
                continue;
            }
            while (c1 > 0) {
                boolean hasSmall = false;
                for (int j = 0; j <= i; j++) {
                    if (count2[j] > 0) {
                        count2[j]--;
                        hasSmall = true;
                        break;
                    }
                }
                if (!hasSmall) {
                    return false;
                }
                count1[i]--;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        String s1 = "abc", s2 = "xya";
        String s1_example2 = "abe", s2_example2 = "acd";
        String s1_example3 = "leetcodee", s2_example3 = "interview";

        M_1433_CheckIfStringCanBreakAnotherString test = new M_1433_CheckIfStringCanBreakAnotherString();
        System.out.println(test.checkIfCanBreak(s1, s2));
        System.out.println(test.checkIfCanBreak(s1_example2, s2_example2));
        System.out.println(test.checkIfCanBreak(s1_example3, s2_example3));
    }

}