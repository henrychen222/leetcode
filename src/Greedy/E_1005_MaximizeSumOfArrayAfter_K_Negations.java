/**
 * 5.3 evening
 * http://www.noteanddata.com/leetcode-1005-Maximize-Sum-Of-Array-After-K-Negations-java-solution-note.html
 */
package Greedy;

import java.util.Arrays;

public class E_1005_MaximizeSumOfArrayAfter_K_Negations {

	public int largestSumAfterKNegations_noteanddata(int[] A, int K) {
		Arrays.sort(A);
		System.out.println(Arrays.toString(A));
		int sum = 0;
		for (int v : A) {
			sum += v;
		}
		int count = 0;
		int minabs = Math.abs(A[0]);
		for (int v : A) {
			minabs = Math.min(minabs, Math.abs(v));
			if (v < 0) {
				sum += 2 * Math.abs(v);
				count++;
				if (count >= K) {
					break;
				}
			} else if (v == 0) {
				break;
			} else {
				int remain = K - count;
				if (remain % 2 == 1) {
					sum -= 2 * minabs;
				}
				break;
			}
		}
		return sum;
	}

	public static void main(String[] args) {
		E_1005_MaximizeSumOfArrayAfter_K_Negations test = new E_1005_MaximizeSumOfArrayAfter_K_Negations();
		int[] A = new int[] {4, 2, 3};
		int K = 1;
		
		int[] A2 = new int[] {3, -1, 0, 2};
		int K2 = 3;
		
		int[] A3 = new int[] {2, -3, -1, 5, -4};
		int K3 = 2;
	    
		System.out.println(test.largestSumAfterKNegations_noteanddata(A, K));
		System.out.println(test.largestSumAfterKNegations_noteanddata(A2, K2));
		System.out.println(test.largestSumAfterKNegations_noteanddata(A3, K3));


	}

}
