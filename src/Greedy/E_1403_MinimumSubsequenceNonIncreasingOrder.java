/**
 * https://www.cnblogs.com/qinduanyinghua/p/12677849.html
 */
package Greedy;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

class E_1403_MinimumSubsequenceNonIncreasingOrder {

	public int accumulate(int[] arr) {
		int sum = 0;
		for (Integer i : arr) {
			sum += i;
		}
		return sum;
	}

	public List<Integer> minSubsequence_cnblog_pq(int[] nums) {
		List<Integer> result = new ArrayList<>();
		int sub_sum = 0;
		int half_sum = accumulate(nums) / 2;
		PriorityQueue<Integer> pq = new PriorityQueue<>();
		while (sub_sum <= half_sum) {
			result.add(pq.peek());
			sub_sum += result.get(result.size() - 1);
			pq.poll();
		}
		return result;
	}

	public static void main(String[] args) {
		E_1403_MinimumSubsequenceNonIncreasingOrder test = new E_1403_MinimumSubsequenceNonIncreasingOrder();

		int[] nums = new int[] { 4, 3, 10, 9, 8 };
		int[] nums2 = new int[] { 4, 4, 7, 6, 7 };
		int[] nums3 = new int[] { 6 };

		System.out.println(test.minSubsequence_cnblog_pq(nums));
		System.out.println(test.minSubsequence_cnblog_pq(nums2));
		System.out.println(test.minSubsequence_cnblog_pq(nums3));

	}
}