/**
 * 5.10 night 5.28 night debug
 * https://leetcode.com/problems/group-the-people-given-the-group-size-they-belong-to/
 */
package Greedy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class M_1282_GroupPeopleGivenGroupSizeTheyBelongTo {
    public List<List<Integer>> groupThePeople(int[] groupSizes) {
        List<List<Integer>> res = new ArrayList<>();
        Map<Integer, ArrayList<Integer>> map = new HashMap<>();
//        for (int i = 0; i < groupSizes.length; i++) {
//            int g = groupSizes[i];
//            if (!map.containsKey(g)) {
//                map.put(g, new ArrayList<>());
//            }
//            System.out.println(map);
//        }
        
        for (int i = 0; i < groupSizes.length; i++) {
            int g = groupSizes[i];
            if (!map.containsKey(g)) {
                map.put(g, new ArrayList<>());
            }
            // System.out.println(map);
            ArrayList<Integer> l = map.get(g);
            // System.out.println("l is: " + l);
            l.add(i);
            if (l.size() == g) {
                // System.out.println(l);
                res.add(new ArrayList<>(l));
                // System.out.println(res);
                l.clear();
            }
        }
        return res;
    }

    // public List<List<Integer>> groupThePeople_cnblog(int[] groupSizes) {
    //     List<List<Integer>> res = new ArrayList<List<Integer>>();
    //     Map<Integer, List<Integer>> map = new HashMap<Integer, List<Integer>>();

    //     for (int i = 0; i < groupSizes.length; i++) {
    //         List<Integer> list = new ArrayList<>();
    //         list.add(i);
    //         map.put(groupSizes[i], list);
    //     }
    //     return null;
    // }

    public static void main(String[] args) {
        int[] groupSizes = new int[] {3,3,3,3,3,1,3};
        int[] groupSizes2 = new int[] {2, 1, 3, 3, 3, 2};
        M_1282_GroupPeopleGivenGroupSizeTheyBelongTo test = new M_1282_GroupPeopleGivenGroupSizeTheyBelongTo();
        System.out.println(test.groupThePeople(groupSizes));
        System.out.println(test.groupThePeople(groupSizes2));
    }
}