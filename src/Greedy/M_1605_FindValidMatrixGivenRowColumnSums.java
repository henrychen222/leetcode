/**
 * 10.3 afternoon
 * https://leetcode.com/problems/find-valid-matrix-given-row-and-column-sums/
 */

package Greedy;

import java.util.Arrays;

public class M_1605_FindValidMatrixGivenRowColumnSums {

	// reference: https://leetcode.com/contest/biweekly-contest-36/ranking/1/
	// Accepted --- 7ms 66.67%
	public int[][] restoreMatrix(int[] rowSum, int[] colSum) {
		int m = rowSum.length;
		int n = colSum.length;
		int[][] res = new int[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				res[i][j] = Math.min(rowSum[i], colSum[j]);
				rowSum[i] -= res[i][j];
				colSum[j] -= res[i][j];
			}
		}
		return res;
	}

	public static void main(String[] args) {
		int[] rowSum = new int[] { 3, 8 };
		int[] colSum = new int[] { 4, 7 };

		int[] rowSum2 = new int[] { 5, 7, 10 };
		int[] colSum2 = new int[] { 8, 6, 8 };

		int[] rowSum3 = new int[] { 14, 9 };
		int[] colSum3 = new int[] { 6, 9, 8 };

		int[] rowSum4 = new int[] { 1, 0 };
		int[] colSum4 = new int[] { 1 };

		int[] rowSum5 = new int[] { 0 };
		int[] colSum5 = new int[] { 0 };

		M_1605_FindValidMatrixGivenRowColumnSums test = new M_1605_FindValidMatrixGivenRowColumnSums();
		System.out.println(Arrays.deepToString(test.restoreMatrix(rowSum, colSum)));
		System.out.println(Arrays.deepToString(test.restoreMatrix(rowSum2, colSum2)));
		System.out.println(Arrays.deepToString(test.restoreMatrix(rowSum3, colSum3)));
		System.out.println(Arrays.deepToString(test.restoreMatrix(rowSum4, colSum4)));
		System.out.println(Arrays.deepToString(test.restoreMatrix(rowSum5, colSum5)));
	}

}
