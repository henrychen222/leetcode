/**
 * 5.14 night
 * https://leetcode.jp/leetcode-1338-reduce-array-size-to-the-half-%E8%A7%A3%E9%A2%98%E6%80%9D%E8%B7%AF%E5%88%86%E6%9E%90/
 */
package Greedy;

import java.util.Arrays;

class M_1338_ReduceArraySizeToTheHalf {
    public int minSetSize(int[] arr) {
        int halfLength = arr.length / 2;
        int[] count = new int[100001];
        for (int n : arr) {
            count[n]++;
        }
        Arrays.sort(count);
        int deleteCount = 0;
        for (int i = 100000; i >= 0; i--) {
            deleteCount += count[i];
            if (deleteCount >= halfLength) {
                return 100001 - i;
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        int[] arr = new int[] { 3, 3, 3, 3, 5, 5, 5, 2, 2, 7 };
        int[] arr2 = new int[] { 7, 7, 7, 7, 7, 7 };
        int[] arr3 = new int[] { 1, 9 };
        int[] arr4 = new int[] { 1000, 1000, 3, 7 };
        int[] arr5 = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        int[] debug1 = new int[] { 9, 77, 63, 22, 92, 9, 14, 54, 8, 38, 18, 19, 38, 68, 58, 19 };
        M_1338_ReduceArraySizeToTheHalf test = new M_1338_ReduceArraySizeToTheHalf();
        System.out.println(test.minSetSize(arr)); // 2
        System.out.println(test.minSetSize(arr2)); // 1
        System.out.println(test.minSetSize(arr3)); // 1
        System.out.println(test.minSetSize(arr4)); // 1
        System.out.println(test.minSetSize(arr5)); // 5
        System.out.println(test.minSetSize(debug1)); // 5
    }
}