// 03/14/21 night 03/15/21 afternoon finally debug it out.
// https://leetcode.com/problems/maximum-score-of-a-good-subarray/
// reference: xiaowuc1

package Contest.wc_232.four;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class MaximumScoreGoodSubarray {

	// Accepted --- 264ms 05/03/21 evening
	public int maximumScore(int[] a, int k) {
		int n = a.length;
		int[][] m = new int[n][];
		for (int i = 0; i < n; i++) {
			m[i] = new int[] { a[i], i };
		}
		Arrays.sort(m, (x, y) -> x[0] - y[0]);
		int res = 0;
		TreeSet<Integer> se = new TreeSet<>(Arrays.asList(-1, n));
		for (int[] d : m) {
			int r = se.ceiling(d[1]); // difference, lower_bound is ceiling
			int l = se.lower(r);
			if (k >= r || k <= l)
				continue;
			res = Math.max(res, (r - l - 1) * (d[0]));
			se.add(d[1]);
		}
		return res;
	}
	
	/////////////////////////////////////////////////////////////
	// Accepted --- 303ms
	public int maximumScore3(int[] a, int k) {
		int n = a.length;
		int[][] m = new int[n][];
		for (int i = 0; i < n; i++) {
			m[i] = new int[] { a[i], i };
		}
		Arrays.sort(m, (x, y) -> x[0] - y[0]);
		int res = 0;
		// diff
		TreeSet<Integer> se = new TreeSet<Integer>() {
			{
				add(-1);
				add(n);
			}
		};
		// TreeSet<Integer> se = new TreeSet<>(Arrays.asList(-1, n)); // Accepted --- 302ms
		for (int[] d : m) {
			int r = lower_bound(se, d[1]);
			int l = se.lower(r);
			if (k >= r || k <= l)
				continue;
			res = Math.max(res, (r - l - 1) * (d[0]));
			se.add(d[1]);
		}
		return res;
	}

	// Accepted --- 297ms
	public int maximumScore2(int[] a, int k) {
		int n = a.length;
		int[][] m = new int[n][];
		for (int i = 0; i < n; i++) {
			m[i] = new int[] { a[i], i };
		}
		Arrays.sort(m, (x, y) -> x[0] - y[0]);
		int res = 0;
		TreeSet<Integer> se = new TreeSet<>();
		se.add(-1);
		se.add(n);
		for (int[] d : m) {
			int r = lower_bound(se, d[1]);
			int l = se.lower(r);
			if (k >= r || k <= l)
				continue;
			res = Math.max(res, (r - l - 1) * (d[0]));
			se.add(d[1]);
		}
		return res;
	}

	public int lower_bound(TreeSet se, int t) {
		int res = -1;
		if (se.contains(t)) {
			res = t;
		} else {
			res = (int) se.higher(t);
		}
		return res;
	};
	
	// Better, not warning. Accepted --- 305ms
//	public int lower_bound(TreeSet<Integer> se, int t) {
//		int res = -1;
//		if (se.contains(t)) {
//			res = t;
//		} else {
//			res = se.higher(t);
//		}
//		return res;
//	};

	///////////////////////////////////////////////////////////////////////////
	// Accepted --- 295ms
	public int maximumScore1(int[] a, int k) {
		int n = a.length;
		int[][] m = new int[n][];
		for (int i = 0; i < n; i++) {
			m[i] = new int[] { a[i], i };
		}
		// System.out.println(Arrays.deepToString(m));
		Arrays.sort(m, (x, y) -> x[0] - y[0]);
		// System.out.println(Arrays.deepToString(m));
		int res = 0;
		TreeSet<Integer> se = new TreeSet<>();
		se.add(-1);
		se.add(n);
		for (int[] d : m) {
			System.out.println(se);
//			int[] tmp = SetToIntArray(se);
//			int[] lb = lower_bound(tmp, d[1]);
//			int r = lb[0];
//			int l = tmp[lb[1] - 1];
			int[] lb = lower_bound2(se, d[1]);
			int r = lb[0];
			int l = lb[1];
			System.out.println(l + " " + r);
			if (k >= r || k <= l)
				continue;
			res = Math.max(res, (r - l - 1) * (d[0]));
			se.add(d[1]);
		}
		return res;
	}

	public int[] lower_bound2(TreeSet se, int t) {
		int res = -1;
		if (se.contains(t)) {
			res = t;
		} else {
			res = (int) se.higher(t);
		}
		int pre = (int) se.lower(res);
		return new int[] { res, pre };
	};

	// TLE 65/72
	public int[] lower_bound1(TreeSet se, int t) {
		int res = -1;
		int pre = -1;
		Iterator it = se.iterator();
		List<Integer> re = new ArrayList<>();
		while (it.hasNext()) {
			int v = (int) it.next();
			if (v >= t) {
				res = v;
				pre = re.get(0).intValue();
				break;
			}
			re.add(0, v);
		}
		return new int[] { res, pre };
	};

	// TLE 65/72
	public int[] lower_bound(int[] a, int t) {
		int res = -1;
		int idx = -1;
		int n = a.length;
		for (int i = 0; i < n; i++) {
			if (a[i] >= t) {
				res = a[i];
				idx = i;
				break;
			}
		}
		return new int[] { res, idx };
	};

	public int[] SetToIntArray(Set se) {
		Object[] a = se.toArray();
		int n = a.length;
		int[] res = new int[n];
		for (int i = 0; i < n; i++)
			res[i] = (int) a[i];
		return res;
	};

	public static void main(String[] args) {
		int[] nums = { 1, 4, 3, 7, 4, 5 };
		int k = 3;
		int[] nums2 = { 5, 5, 4, 5, 4, 1, 1, 1 };
		int k2 = 0;
		int[] nums_debug1 = { 6569, 9667, 3148, 7698, 1622, 2194, 793, 9041, 1670, 1872 };
		int k_debug1 = 5;
		MaximumScoreGoodSubarray t = new MaximumScoreGoodSubarray();
		System.out.println(t.maximumScore(nums, k));
		System.out.println(t.maximumScore(nums2, k2));
		System.out.println(t.maximumScore(nums_debug1, k_debug1));
	}

}
