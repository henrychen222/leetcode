/**
 * 05/01/21 afternoon
 * https://leetcode.com/contest/biweekly-contest-51/problems/closest-room/
 * 
 * reference:
 * https://leetcode.com/contest/biweekly-contest-51/ranking/1/ Tlatoani
 */

package Contest.bwc_51.four;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.TreeSet;

public class ClosestRoom {

	static PrintWriter pw;
	
	// Accepted --- 81ms 66.67%
	public int[] closestRoom(int[][] rooms, int[][] queries) {
		Arrays.sort(rooms, (x, y) -> y[1] - x[1]);
		int qn = queries.length;
		for (int i = 0; i < qn; i++) {
			queries[i] = new int[] { i, queries[i][0], queries[i][1] };
		}
		Arrays.sort(queries, (x, y) -> y[2] - x[2]);
//		pw.println(Arrays.deepToString(rooms));
//		pw.println(Arrays.deepToString(queries));
		int[] res = new int[qn];
		TreeSet<Integer> ts = new TreeSet<>();
		int ri = 0;
		for (int[] e : queries) {
			int qi = e[0];
			int qid = e[1];
			int qmize = e[2];
			while (ri < rooms.length && rooms[ri][1] >= qmize) {
				ts.add(rooms[ri][0]);
				ri++;
				// pw.println("TreeSet: " + ts);
			}
			if (ts.isEmpty()) {
				res[qi] = -1;
			} else {
//				int lower = -1000000000;
//				int higher = 1000000000;
//				if (ts.floor(qid) != null) lower = ts.floor(qid);
//				if (ts.ceiling(qid) != null) higher = ts.ceiling(qid);

				// Accepted --- 72ms
				Integer lower = ts.floor(qid);
				Integer higher = ts.ceiling(qid);
				if (lower == null) lower = -1000000000;
				if (higher == null) higher = 1000000000;

				// pw.println(lower + " " + higher);
				if (qid - lower <= higher - qid) {
					res[qi] = lower;
				} else {
					res[qi] = higher;
				}
			}
		}
		return res;
	}

	public void run() {
		int[][] rooms = { { 2, 2 }, { 1, 2 }, { 3, 2 } };
		int[][] queries = { { 3, 1 }, { 3, 3 }, { 5, 2 } };
		int[][] rooms2 = { { 1, 4 }, { 2, 3 }, { 3, 5 }, { 4, 1 }, { 5, 2 } };
		int[][] queries2 = { { 2, 3 }, { 2, 4 }, { 2, 5 } };
		pw.println(Arrays.toString(closestRoom(rooms, queries)));
		pw.println(Arrays.toString(closestRoom(rooms2, queries2)));
	}

	public static void main(String[] args) {
		pw = new PrintWriter(System.out);
		new ClosestRoom().run();
		pw.close();
	}

}
