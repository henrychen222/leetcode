// 04/20/20 night

package Contest.wc_236.four;

import java.util.*;
import java.io.*;
import static java.lang.System.out;

class MKAverage {
	Deque<Node> def;
	Node root = null;
	int m, k;

	public MKAverage(int m, int k) {
		def = new ArrayDeque<>();
		this.m = m;
		this.k = k;
	}

	public void addElement(int num) {
		Node x = new Node(num, num);
		def.add(x);
		root = insertb(root, x);
		if (def.size() > m) {
			root = erase(root, index(def.poll()));
		}
	}

	public int calculateMKAverage() {
		if (def.size() < m)
			return -1;
		return (int) (sum(root, k, m - k) / (m - 2 * k));
	}

	public Random gen = new Random();

	public class Node {
		public int id;
		public long v; // value
		public long priority;
		public Node left, right, parent;

		public int count;

		public long sum;

		public Node(int id, int v) {
			this.id = id;
			this.v = v;
			priority = gen.nextLong();
			update(this);
		}
	}

	public Node update(Node a) {
		out.println(a.id + "" + a.v + " " + a.priority + " " + a.count + " " + a.sum);
		if (a == null)
			return null;
		a.count = 1;
		if (a.left != null)
			a.count += a.left.count;
		if (a.right != null)
			a.count += a.right.count;

		a.sum = a.v;
		if (a.left != null)
			a.sum += a.left.sum;
		if (a.right != null)
			a.sum += a.right.sum;
		return a;
	}

	public Node disconnect(Node a) {
		if (a == null)
			return null;
		a.left = a.right = a.parent = null;
		return update(a);
	}

	public Node root(Node x) {
		if (x == null)
			return null;
		while (x.parent != null)
			x = x.parent;
		return x;
	}

	public int count(Node a) {
		return a == null ? 0 : a.count;
	}

	public void setParent(Node a, Node par) {
		if (a != null)
			a.parent = par;
	}

	public long sum(Node a, int L, int R) {
		if (a == null || L >= R || L >= count(a) || R <= 0)
			return 0L;
		if (L <= 0 && R >= count(a)) {
			return a.sum;
		} else {
			long ret = 0;
			ret += sum(a.left, L, R);
			ret += sum(a.right, L - count(a.left) - 1, R - count(a.left) - 1);
			if (L <= count(a.left) && count(a.left) < R)
				ret += a.v;
			return ret;
		}
	}

	public Node merge(Node a, Node b) {
		if (b == null)
			return a;
		if (a == null)
			return b;
		if (a.priority > b.priority) {
			setParent(a.right, null);
			setParent(b, null);
			a.right = merge(a.right, b);
			setParent(a.right, a);
			return update(a);
		} else {
			setParent(a, null);
			setParent(b.left, null);
			b.left = merge(a, b.left);
			setParent(b.left, b);
			return update(b);
		}
	}

	// [0,K),[K,N)
	public Node[] split(Node a, int K) {
		if (a == null)
			return new Node[] { null, null };
		if (K <= count(a.left)) {
			setParent(a.left, null);
			Node[] s = split(a.left, K);
			a.left = s[1];
			setParent(a.left, a);
			s[1] = update(a);
			return s;
		} else {
			setParent(a.right, null);
			Node[] s = split(a.right, K - count(a.left) - 1);
			a.right = s[0];
			setParent(a.right, a);
			s[0] = update(a);
			return s;
		}
	}

	public Node insert(Node a, int K, Node b) {
		if (a == null)
			return b;
		if (b.priority < a.priority) {
			if (K <= count(a.left)) {
				a.left = insert(a.left, K, b);
				setParent(a.left, a);
			} else {
				a.right = insert(a.right, K - count(a.left) - 1, b);
				setParent(a.right, a);
			}
			return update(a);
		} else {
			Node[] ch = split(a, K);
			b.left = ch[0];
			b.right = ch[1];
			setParent(b.left, b);
			setParent(b.right, b);
			return update(b);
		}
	}

	public Node insertb(Node root, Node x) {
		int ind = search(root, x.id);
		if (ind < 0) {
			// insert
			ind = -ind - 1;
			return insert(root, ind, x);
		} else {
			return insert(root, ind, x);
		}
	}

	// delete K-th
	public Node erase(Node a, int K) {
		if (a == null)
			return null;
		if (K < count(a.left)) {
			a.left = erase(a.left, K);
			setParent(a.left, a);
			return update(a);
		} else if (K == count(a.left)) {
			setParent(a.left, null);
			setParent(a.right, null);
			Node aa = merge(a.left, a.right);
			disconnect(a);
			return aa;
		} else {
			a.right = erase(a.right, K - count(a.left) - 1);
			setParent(a.right, a);
			return update(a);
		}
	}

	public Node get(Node a, int K) {
		while (a != null) {
			if (K < count(a.left)) {
				a = a.left;
			} else if (K == count(a.left)) {
				break;
			} else {
				K = K - count(a.left) - 1;
				a = a.right;
			}
		}
		return a;
	}

	public int lowerBound(Node a, int q) {
		int lcount = 0;
		while (a != null) {
			if (a.v >= q) {
				a = a.left;
			} else {
				lcount += count(a.left) + 1;
				a = a.right;
			}
		}
		return lcount;
	}

	public int search(Node a, int q) {
		int lcount = 0;
		while (a != null) {
			if (a.id == q) {
				lcount += count(a.left);
				break;
			}
			if (q < a.id) {
				a = a.left;
			} else {
				lcount += count(a.left) + 1;
				a = a.right;
			}
		}
		return a == null ? -(lcount + 1) : lcount;
	}

	public int index(Node a) {
		if (a == null)
			return -1;
		int ind = count(a.left);
		while (a != null) {
			Node par = a.parent;
			if (par != null && par.right == a) {
				ind += count(par.left) + 1;
			}
			a = par;
		}
		return ind;
	}

	public Node[] nodes(Node a) {
		return nodes(a, new Node[count(a)], 0, count(a));
	}

	public Node[] nodes(Node a, Node[] ns, int L, int R) {
		if (a == null)
			return ns;
		nodes(a.left, ns, L, L + count(a.left));
		ns[L + count(a.left)] = a;
		nodes(a.right, ns, R - count(a.right), R);
		return ns;
	}

	public static void main(String args[]) {
		MKAverage obj = new MKAverage(3, 1);
		obj.addElement(3);
		obj.addElement(1);
		out.println(obj.calculateMKAverage()); // -1
		obj.addElement(10);
		out.println(obj.calculateMKAverage()); // 3
		obj.addElement(5);
		obj.addElement(5);
		obj.addElement(5);
		out.println(obj.calculateMKAverage()); // 5
	}

}