/**
 * 05/29/21 night
 * https://leetcode.com/problems/process-tasks-using-servers/
 */

package Contest.wc_243_three;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

public class ProcessTasksUsingServers {

	static PrintWriter pw;

	// uwi https://leetcode.com/contest/weekly-contest-243/ranking/1/
	public int[] assignTasks(int[] servers, int[] tasks) {
		int m = tasks.length;
		PriorityQueue<long[]> wait = new PriorityQueue<>((x, y) -> {
			if (x[1] != y[1])
				return Long.compare(x[1], y[1]);
			return Long.compare(x[2], y[2]);
		});
		PriorityQueue<long[]> pq = new PriorityQueue<>((x, y) -> {
			if (x[0] != y[0])
				return Long.compare(x[0], y[0]);
			if (x[1] != y[1])
				return Long.compare(x[1], y[1]);
			return Long.compare(x[2], y[2]);
		});
		for (int i = 0; i < servers.length; i++) {
			wait.add(new long[] { 0, servers[i], i });
		}
        pw.println(Arrays.deepToString(wait.toArray())); // order is wrong
//		while(!wait.isEmpty()) {
//            System.out.println("1111" + " " + Arrays.toString(wait.poll()));
//        }
        pqdebug(wait);
		int[] ret = new int[m];
		long time = 0;
		for (int i = 0; i < m; i++) {
			time = Math.max(time, i);
			while (!pq.isEmpty()) {
				long[] ne = pq.peek();
				if (ne[0] <= time) {
					wait.add(pq.poll());
				} else {
					break;
				}
			}
			tr(pq.peek(), wait.peek());
			if (wait.isEmpty()) {
				long[] ne = pq.peek();
				time = ne[0];
				wait.add(pq.poll());
			}

			long[] cur = wait.poll();
			ret[i] = (int) cur[2];
			cur[0] = time + tasks[i];
			pq.add(cur);
			pqdebug(wait);
		}
		return ret;
	}

	private void pqdebug(PriorityQueue<long[]> pq) {
		PriorityQueue<long[]> copy = new PriorityQueue<>((x, y) -> {
			if (x[1] != y[1])
				return Long.compare(x[1], y[1]);
			return Long.compare(x[2], y[2]);
		});
		for (long[] e : pq)
			copy.add(e);
		long[][] show  = new long[pq.size()][];
		int i = 0;
		while (!copy.isEmpty()) {
			show[i] = copy.poll();
			i++;
		}
		tr(show);
	}

	public void run() {
		int[] servers = { 3, 3, 2 };
		int[] tasks = { 1, 2, 3, 2, 1, 2 };
		pw.println(Arrays.toString(assignTasks(servers, tasks)));
	}

	public static void main(String[] args) {
		pw = new PrintWriter(System.out);
		new ProcessTasksUsingServers().run();
		pw.close();
	}

	void tr(Object... o) {
		pw.println(Arrays.deepToString(o));
	}
}