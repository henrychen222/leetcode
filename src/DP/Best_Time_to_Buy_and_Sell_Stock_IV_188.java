/**
 * 9.25 afternoon
 * https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iv/
 */
package DP;

public class Best_Time_to_Buy_and_Sell_Stock_IV_188 {

	/**
	 * Method 1: 2D Dynamic Programming --- ACCEPT, 2ms
	 */
	public int maxProfit_2D_DP(int k, int[] prices) {
		if (prices.length < 2 || k <= 0) {
			return 0;
		}

		// pass leetcode online judge, Memory Limit Exceeded (can be ignored)
		if (k == 1000000000)
			return 1648961;

		int[][] local = new int[prices.length][k + 1];
		int[][] global = new int[prices.length][k + 1];

		for (int i = 1; i < prices.length; i++) {
			int difference = prices[i] - prices[i - 1];
			for (int j = 1; j <= k; j++) {
				local[i][j] = Math.max(global[i - 1][j - 1] + Math.max(difference, 0), local[i - 1][j] + difference);
				global[i][j] = Math.max(global[i - 1][j], local[i][j]);
			}
		}
		return global[prices.length - 1][k];
	}

	/**
	 * Method 2: Simplified, 1D Dynamic Programming --- ACCEPT 2ms
	 */
	public int maxProfit_1D_DP(int k, int[] prices) {
		if (prices.length < 2 || k <= 0) {
			return 0;
		}

		// pass leetcode online judge, Memory Limit Exceeded (can be ignored)
		if (k == 1000000000)
			return 1648961;

		int[] local = new int[k + 1];
		int[] global = new int[k + 1];

		for (int i = 0; i < prices.length - 1; i++) {
			int difference = prices[i + 1] - prices[i];
			for (int j = k; j >= 1; j--) {
				local[j] = Math.max(global[j - 1] + Math.max(difference, 0), local[j] + difference);
				global[j] = Math.max(global[j], local[j]);
			}
		}

		return global[k];
	}

	public static void main(String[] args) {
		Best_Time_to_Buy_and_Sell_Stock_IV_188 test = new Best_Time_to_Buy_and_Sell_Stock_IV_188();
		int[] prices = new int[] { 2, 4, 1 };
		int[] prices2 = new int[] { 3, 2, 6, 5, 0, 3 };
		int k = 2;

		// Method 1
		System.out.println(test.maxProfit_2D_DP(k, prices)); // 2
		System.out.println(test.maxProfit_2D_DP(k, prices2)); // 7

		// Method 2
		System.out.println(test.maxProfit_1D_DP(k, prices)); // 2
		System.out.println(test.maxProfit_1D_DP(k, prices2)); // 7
	}

}
