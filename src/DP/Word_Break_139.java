/**
 * 9.20 night
 * https://leetcode.com/problems/word-break/
 */
package DP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Word_Break_139 {

	/**
	 * Method 1: Naive Approach TC: O(n^2) --- Correct, But Time Limit Exceeded,
	 * Fail
	 */
	public boolean wordBreak_Native(String s, List<String> wordDict) {
		return helper(s, wordDict, 0);
	}

	public boolean helper(String s, List<String> dict, int begin) {
		if (begin == s.length()) {
			return true;
		}
		for (String a : dict) {
			int len = a.length();
			int end = begin + len;
			if (end > s.length()) {
				continue;
			}
			if (s.substring(begin, begin + len).equals(a)) {
				if (helper(s, dict, begin + len)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Method 2: Dynamic Programming TC: O(string length * dict size) --- ACCEPT 3ms
	 */
	public boolean wordBreak_DP(String s, List<String> wordDict) {
		// an array, making dp[i]==true => 0-(i-1) can be segmented using dictionary
		boolean[] dp = new boolean[s.length() + 1];
		// set initial state
		dp[0] = true;

		for (int i = 0; i < s.length(); i++) {
			if (!dp[i]) {
				continue;
			}
			for (String a : wordDict) {
				int len = a.length();
				int end = i + len;
				if (end > s.length()) {
					continue;
				}
				if (dp[end]) {
					continue;
				}
				if (s.substring(i, end).equals(a)) {
					dp[end] = true;
				}
			}
		}
		return dp[s.length()];
	}

	/**
	 * Method 3: Simple and Efficient TC: O(n^2) --- ACCEPT 4ms
	 */
	public boolean wordBreak_simple_efficient(String s, List<String> wordDict) {
		int[] position = new int[s.length() + 1];
		Arrays.fill(position, -2); // assign a value to each element
		position[0] = 0;

		for (int i = 0; i < s.length(); i++) {
			if (position[i] != -2) {
				for (int j = i + 1; j <= s.length(); j++) {
					if (wordDict.contains(s.substring(i, j))) {
						position[j] = i;
					}
				}
			}
		}
		return position[s.length()] != -2;
	}

	public static void main(String[] args) {
		Word_Break_139 test = new Word_Break_139();
		String s1 = "leetcode";
		List<String> dict1 = new ArrayList<String>();
		dict1.add("leet");
		dict1.add("code");

		String s2 = "applepenapple";
		List<String> dict2 = new ArrayList<String>();
		dict2.add("apple");
		dict2.add("pen");

		String s3 = "catsandog";
		List<String> dict3 = new ArrayList<String>();
		dict3.add("cats");
		dict3.add("dog");
		dict3.add("sand");
		dict3.add("and");

		System.out.println(dict1);
		System.out.println(dict2);
		System.out.println(dict3);

		System.out.println("---Method 1: Native-----");
		System.out.println(test.wordBreak_Native(s1, dict1)); // true
		System.out.println(test.wordBreak_Native(s2, dict2)); // true
		System.out.println(test.wordBreak_Native(s3, dict3)); // false

		System.out.println("---Method 2: DP -----");
		System.out.println(test.wordBreak_DP(s1, dict1)); // true
		System.out.println(test.wordBreak_DP(s2, dict2)); // true
		System.out.println(test.wordBreak_DP(s3, dict3)); // false

		System.out.println("---Method 3: Simple Efficient -----");
		System.out.println(test.wordBreak_simple_efficient(s1, dict1)); // true
		System.out.println(test.wordBreak_simple_efficient(s2, dict2)); // true
		System.out.println(test.wordBreak_simple_efficient(s3, dict3)); // false

	}

}
