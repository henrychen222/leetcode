//9.9 https://www.programcreek.com/2014/06/leetcode-longest-valid-parentheses-java/
// https://leetcode.com/problems/longest-valid-parentheses/
package DP;

import java.util.Stack;

public class Longest_Valid_Parentheses_32 {

	/**
	 * A stack can be used to track and reduce pairs
	 * Since the problem asks for length, we can put the index into the stack along with the character. 
	 * For coding simplicity purpose, we can use 0 to represent '(' and 1 to represent ')'.
	 * ---ACCEPT
	 */
	public int longestValidParentheses(String s) {
		Stack<int[]> stack = new Stack<>();
		int result = 0;

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c == ')') {
				if (!stack.isEmpty() && stack.peek()[0] == 0) {
					stack.pop();
					result = Math.max(result, i - (stack.isEmpty() ? -1 : stack.peek()[1]));
				} else {
					stack.push(new int[] { 1, i });
				}
			} else {
				stack.push(new int[] { 0, i });
			}

		}
		return result;
	}

	public static void main(String[] args) {

		Longest_Valid_Parentheses_32 test = new Longest_Valid_Parentheses_32();
		String s1 = "(()";
		String s2 = ")()())";
		System.out.println(test.longestValidParentheses(s1));
		System.out.println(test.longestValidParentheses(s2));

	}

}
