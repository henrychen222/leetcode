/**
 * https://leetcode.com/problems/distinct-subsequences
 * 9.18 night 9.19 evening 9.19 night
 */
package DP;

import java.util.ArrayList;
import java.util.HashMap;

public class Distinct_Subsequences_115 {

	/**
	 * --- ACCEPT, 6ms
	 */
	public int numDistinct(String s, String t) {
		int[][] dp = new int[s.length() + 1][t.length() + 1];

		for (int i = 0; i < s.length(); i++) {
			dp[i][0] = 1;
		}

		for (int i = 1; i <= s.length(); i++) {
			for (int j = 1; j <= t.length(); j++) {
				if (s.charAt(i - 1) == t.charAt(j - 1)) {
					dp[i][j] += dp[i - 1][j] + dp[i - 1][j - 1];
				} else {
					dp[i][j] += dp[i - 1][j];
				}
			}
		}
		return dp[s.length()][t.length()];
	}

	/**
	 * Method 2: use HashMap, complicated --- ACCEPT(3ms), faster than Method 1
	 */
	public int numDistinct2(String s, String t) {
		HashMap<Character, ArrayList<Integer>> map = new HashMap<Character, ArrayList<Integer>>();
		int[] result = new int[t.length() + 1];
		result[0] = 1;

		for (int i = 0; i < t.length(); i++) {
			if (map.containsKey(t.charAt(i))) {
				map.get(t.charAt(i)).add(i);
			} else {
				ArrayList<Integer> temp = new ArrayList<Integer>();
				temp.add(i);
				map.put(t.charAt(i), temp);
			}
		}

		for (int i = 0; i < s.length(); i++) {
			if (map.containsKey(s.charAt(i))) {
				ArrayList<Integer> temp = map.get(s.charAt(i));
				int[] old = new int[temp.size()];

				// j < temp.size() also works
				for (int j = 0; j < old.length; j++) {
					old[j] = result[temp.get(j)];
				}

				// the relation
				for (int j = 0; j < old.length; j++) {
					result[temp.get(j) + 1] = result[temp.get(j) + 1] + old[j];
				}
			}
		}

		return result[t.length()];
	}

	public static void main(String[] args) {
		Distinct_Subsequences_115 test = new Distinct_Subsequences_115();
		String s1 = "rabbbit";
		String t1 = "rabbit";
		String s2 = "babgbag";
		String t2 = "bag";

		// Method 1
		System.out.println(test.numDistinct(s1, t1)); // 3
		System.out.println(test.numDistinct(s2, t2)); // 5

		// Method 2
		System.out.println(test.numDistinct2(s1, t1)); // 3
		System.out.println(test.numDistinct2(s2, t2)); // 5
	}

}
