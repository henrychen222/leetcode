/** 
 * 9.15 night
 * https://leetcode.com/problems/minimum-path-sum/
 */
package DP;

//Compare with 62 63
public class Minimum_Path_Sum_64 {

	/**
	 * Method 1: DFS
	 * --- Correct, but Time Limit Exceeded, FAIL
	 */
	public int minPathSum_DFS(int[][] grid) {
		return DFS(0, 0, grid);
	}

	public int DFS(int i, int j, int[][] grid) {
		if (i == grid.length - 1 && j == grid[0].length - 1) {
			return grid[i][j];
		}

		if (i < grid.length - 1 && j < grid[0].length - 1) {
			int route1 = grid[i][j] + DFS(i + 1, j, grid);
			int route2 = grid[i][j] + DFS(i, j + 1, grid);
			return Math.min(route1, route2);
		}

		if (i < grid.length - 1) {
			return grid[i][j] + DFS(i + 1, j, grid);
		}

		if (j < grid[0].length - 1) {
			return grid[i][j] + DFS(i, j + 1, grid);
		}
		return 0;
	}

	/**
	 * Method 2: DP
	 * ---ACCEPT
	 */
	public int minPathSum_DP(int[][] grid) {
		if (grid == null || grid.length == 0) {
			return 0;
		}

		int m = grid.length;
		int n = grid[0].length;

		int[][] dp = new int[m][n];
		dp[0][0] = grid[0][0];

		// initialize left column
		for (int i = 1; i < m; i++) {
			dp[i][0] = dp[i - 1][0] + grid[i][0];
		}

		// initialize top row
		for (int i = 1; i < n; i++) {
			dp[0][i] = dp[0][i - 1] + grid[0][i];
		}

		// fill up the table
		for (int i = 1; i < m; i++) {
			for (int j = 1; j < n; j++) {
				if (dp[i - 1][j] > dp[i][j - 1]) {
					dp[i][j] = dp[i][j - 1] + grid[i][j];
				} else {
					dp[i][j] = dp[i - 1][j] + grid[i][j];
				}
			}
		}

		return dp[m - 1][n - 1];
	}

	public static void main(String[] args) {
		Minimum_Path_Sum_64 test = new Minimum_Path_Sum_64();
		int[][] grid = { { 1, 3, 1 }, { 1, 5, 1 }, { 4, 2, 1 } };
		System.out.println(test.minPathSum_DFS(grid));
		System.out.println(test.minPathSum_DP(grid));

	}

}
