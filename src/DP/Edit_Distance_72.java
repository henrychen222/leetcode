/**
 * 9.16 morning
 * https://leetcode.com/problems/edit-distance
 */
package DP;

import java.util.Arrays;

public class Edit_Distance_72 {

	/**
	 * Method 1: DP --- ACCEPT
	 */
	public int minDistance(String word1, String word2) {
		int m = word1.length();
		int n = word2.length();

		// m+1. n+1. because finally return [m][n]
		int[][] dp = new int[m + 1][n + 1];

		// initialize based on m and n
		for (int i = 0; i <= m; i++) {
			dp[i][0] = i;
		}
		for (int i = 0; i <= n; i++) {
			dp[0][i] = i;
		}

		// iterate though, and check last char
		for (int i = 0; i < m; i++) {
			char c1 = word1.charAt(i);
			for (int j = 0; j < n; j++) {
				char c2 = word2.charAt(j);

				// c1 and c2 represent the last character of word1 and word2
				if (c1 == c2) {
					//// update dp value for +1 length
					dp[i + 1][j + 1] = dp[i][j];
				} else {
					int replace = dp[i][j] + 1;
					int insert = dp[i][j + 1] + 1;
					int delete = dp[i + 1][j] + 1;

					// get the minimum number of operations
					int min = replace > insert ? insert : replace;
					min = delete < min ? delete : min;
					dp[i + 1][j + 1] = min;
				}
			}
		}
		return dp[m][n];
	}

	/**
	 * Method 2: Recursion
	 * ---ACCEPT
	 */
	public int minDistance_Recursion(String word1, String word2) {
		int m = word1.length();
		int n = word2.length();
		int[][] mem = new int[m][n];
		for (int[] arr : mem) {
			Arrays.fill(arr, -1);
		}
		return calDistance(word1, word2, mem, m - 1, n - 1);
	}

	private int calDistance(String word1, String word2, int[][] mem, int i, int j) {
		if (i < 0) {
			return j + 1;
		} else if (j < 0) {
			return i + 1;
		}

		if (mem[i][j] != -1) {
			return mem[i][j];
		}

		if (word1.charAt(i) == word2.charAt(j)) {
			mem[i][j] = calDistance(word1, word2, mem, i - 1, j - 1);
		} else {
			int prevMin = Math.min(calDistance(word1, word2, mem, i - 1, j), calDistance(word1, word2, mem, i, j - 1));
			prevMin = Math.min(prevMin, calDistance(word1, word2, mem, i - 1, j - 1));
			mem[i][j] = prevMin + 1;
		}
		return mem[i][j];
	}

	public static void main(String[] args) {

		Edit_Distance_72 test = new Edit_Distance_72();
		String word1 = "horse";
		String word2 = "ros";
		String word3 = "intention";
		String word4 = "execution";
		System.out.println(test.minDistance(word1, word2)); // 3
		System.out.println(test.minDistance(word3, word4)); // 5

		System.out.println(test.minDistance_Recursion(word1, word2)); // 3
		System.out.println(test.minDistance_Recursion(word3, word4)); // 5

	}

}
