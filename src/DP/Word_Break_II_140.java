/**
 * 9.20 night 9.21 afternoon
 * https://leetcode.com/problems/word-break-ii
 */
package DP;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Word_Break_II_140 {

	/**
	 * Method 1: DP --- ACCEPT 9ms
	 */
	public List<String> wordBreak(String s, List<String> wordDict) {
		// create an array of ArrayList<String>
		List<String>[] dp = new ArrayList[s.length() + 1];
		dp[0] = new ArrayList<String>();

		for (int i = 0; i < s.length(); i++) {
			if (dp[i] == null) {
				continue;
			}
			for (String a : wordDict) {
				int len = a.length();
				int end = i + len;
				if (end > s.length()) {
					continue;
				}
				if (s.substring(i, end).equals(a)) {
					if (dp[end] == null) {
						dp[end] = new ArrayList<String>();
					}
					dp[end].add(a);
				}
			}
		}

		List<String> res = new LinkedList<String>();
		ArrayList<String> temp = new ArrayList<String>();
		if (dp[s.length()] == null) {
			return res;
		}
		dfs(dp, s.length(), res, temp);
		return res;
	}

	public static void dfs(List<String> dp[], int end, List<String> result, ArrayList<String> temp) {
		if (end <= 0) {
			String path = temp.get(temp.size() - 1);
			for (int i = temp.size() - 2; i >= 0; i--) {
				path += " " + temp.get(i);
			}
			result.add(path);
			return;
		}
		for (String a : dp[end]) {
			temp.add(a);
			dfs(dp, end - a.length(), result, temp);
			temp.remove(temp.size() - 1);
		}
	}

	/**
	 * Method 2: Simplified --- ACCEPT 14ms
	 */
	public List<String> wordBreak_simplified(String s, List<String> wordDict) {
		List<String>[] position = new ArrayList[s.length() + 1];
		position[0] = new ArrayList<String>();

		for (int i = 0; i < s.length(); i++) {
			if (position[i] != null) {
				for (int j = i + 1; j <= s.length(); j++) {
					String sub = s.substring(i, j);
					if (wordDict.contains(sub)) {
						if (position[j] == null) {
							ArrayList<String> list = new ArrayList<String>();
							list.add(sub);
							position[j] = list;
						} else {
							position[j].add(sub);
						}
					}
				}
			}
		}

		if (position[s.length()] == null) {
			return new ArrayList<String>();
		} else {
			ArrayList<String> res = new ArrayList<String>();
			dfs_simplfied(position, res, "", s.length());
			return res;
		}
	}

	public void dfs_simplfied(List<String>[] position, List<String> res, String current, int i) {
		if (i == 0) {
			res.add(current.trim());
			return;
		}

		for (String s : position[i]) {
			String combined = s + " " + current;
			dfs_simplfied(position, res, combined, i - s.length());
		}
	}

	public static void main(String[] args) {
		Word_Break_II_140 test = new Word_Break_II_140();
		String s1 = "catsanddog";
		List<String> dict1 = new ArrayList<String>();
		dict1.add("cat");
		dict1.add("cats");
		dict1.add("and");
		dict1.add("sand");
		dict1.add("dog");

		String s2 = "pineapplepenapple";
		List<String> dict2 = new ArrayList<String>();
		dict2.add("apple");
		dict2.add("pen");
		dict2.add("applepen");
		dict2.add("pine");
		dict2.add("pineapple");

		String s3 = "catsandog";
		List<String> dict3 = new ArrayList<String>();
		dict3.add("cats");
		dict3.add("dog");
		dict3.add("sand");
		dict3.add("and");
		dict3.add("cat");

		System.out.println(test.wordBreak(s1, dict1));
		System.out.println(test.wordBreak(s2, dict2));
		System.out.println(test.wordBreak(s3, dict3));

		System.out.println(test.wordBreak_simplified(s1, dict1));
		System.out.println(test.wordBreak_simplified(s2, dict2));
		System.out.println(test.wordBreak_simplified(s3, dict3));

	}

}
