//9.15 evening
//https://leetcode.com/problems/unique-paths/
package DP;

public class Unique_Paths_62 {

	/**
	 * Method 1: DFS
	 * --- Correct, but Time Limit Exceeded, FAIL
	 */
	public int uniquePaths_DFS(int m, int n) {
		return DFS(0, 0, m, n);
	}

	public int DFS(int i, int j, int m, int n) {
		if (i == m - 1 && j == n - 1) {
			return 1;
		}

		if (i < m - 1 && j < n - 1) {
			return DFS(i + 1, j, m, n) + DFS(i, j + 1, m, n);
		}

		if (i < m - 1) {
			return DFS(i + 1, j, m, n);
		}

		if (j < n - 1) {
			return DFS(i, j + 1, m, n);
		}

		return 0;
	}

	/**
	 * Method 2: Dynamic Programming
	 * ---ACCEPT
	 */
	public int uniquePaths_DP(int m, int n) {
		if (m == 0 || n == 0) {
			return 0;
		}
		if (m == 1 || n == 1) {
			return 1;
		}

		int[][] dp = new int[m][n];

		// left column
		for (int i = 0; i < m; i++) {
			dp[i][0] = 1;
		}

		// top row
		for (int j = 0; j < n; j++) {
			dp[0][j] = 1;
		}

		// fill up the dp table
		for (int i = 1; i < m; i++) {
			for (int j = 1; j < n; j++) {
				dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
			}
		}

		return dp[m - 1][n - 1];
	}

	/**
	 * Method 3: Dynamic Programming with Memorization
	 * ---ACCEPT
	 */
	public int uniquePaths_DP_Memorization(int m, int n) {
		int memory[][] = new int[m][n];

		// init with -1 value
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				memory[i][j] = -1;
			}
		}
		return helper(memory, m - 1, n - 1);
	}

	private int helper(int[][] memory, int m, int n) {
		if (m == 0 || n == 0) {
			memory[m][n] = 1;
			return 1;
		}

		if (memory[m][n] != -1) {
			return memory[m][n];
		}

		memory[m][n] = helper(memory, m, n - 1) + helper(memory, m - 1, n);

		return memory[m][n];
	}

	public static void main(String[] args) {
		Unique_Paths_62 test = new Unique_Paths_62();
		int m = 7;
		int n = 3;
		System.out.println(test.uniquePaths_DFS(m, n));
		System.out.println(test.uniquePaths_DP(m, n));
		System.out.println(test.uniquePaths_DP_Memorization(m, n));

	}

}
