/**
 * 9.26 afternoon
 * https://leetcode.com/problems/maximal-square/
 */
package DP;

public class Maximal_Square_221 {

	/**
	 * --- ACCEPT
	 */
	public int maximalSquare(char[][] matrix) {
		if (matrix.length == 0 || matrix == null) {
			return 0;
		}

		int m = matrix.length;
		int n = matrix[0].length;
		int result = 0;
		int[][] dp = new int[m][n];

		// left column
		for (int i = 0; i < m; i++) {
			dp[i][0] = matrix[i][0] - '0';
			result = Math.max(dp[i][0], result);
		}

		// top row
		for (int j = 0; j < n; j++) {
			dp[0][j] = matrix[0][j] - '0';
			result = Math.max(dp[0][j], result);
		}

		// dp table
		for (int i = 1; i < m; i++) {
			for (int j = 1; j < n; j++) {
				if (matrix[i][j] == '1') {
					int min = Math.min(dp[i][j - 1], dp[i - 1][j]);
					min = Math.min(min, dp[i - 1][j - 1]);
					dp[i][j] = min + 1;
					result = Math.max(min + 1, result);
				} else {
					dp[i][j] = 0;
				}
			}
		}

		return result * result;
	}

	public static void main(String[] args) {
		Maximal_Square_221 test = new Maximal_Square_221();
		char[][] matrix = new char[][] { { '1', '0', '1', '0', '0' }, { '1', '0', '1', '1', '1' },
				{ '1', '1', '1', '1', '1' }, { '1', '0', '0', '1', '0' } };
		System.out.println(test.maximalSquare(matrix)); // 4
	}

}
