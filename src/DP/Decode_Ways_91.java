/**
 * 9.18 evening 
 * https://leetcode.com/problems/decode-ways/
 */
package DP;

public class Decode_Ways_91 {

	/**
	 *  --- ACCEPT
	 */
	public int numDecodings(String s) {
		// first character is 0, no way to decode
		if (s.charAt(0) == '0') {
			return 0;
		}

		int[] dp = new int[s.length() + 1];
		dp[0] = 1;
		dp[1] = 1;

		for (int i = 1; i < s.length(); i++) {
			char right = s.charAt(i); // second char
			char left = s.charAt(i - 1); // begin char

			if (right == '0' && (left == '0' || left > '2')) {
				return 0;
			}

			if (left == '0') {
				dp[i + 1] = dp[i];
			} else if (left == '1') {
				if (right == '0') {
					dp[i + 1] = dp[i - 1];
				} else {
					dp[i + 1] = dp[i - 1] + dp[i];
				}
			} else if (left == '2') {
				if (right == '0') {
					dp[i + 1] = dp[i - 1];
				} else if (right <= '6') {
					dp[i + 1] = dp[i - 1] + dp[i];
				} else {
					dp[i + 1] = dp[i];
				}
			} else {
				dp[i + 1] = dp[i];
			}

		}
		return dp[s.length()];

	}

	public static void main(String[] args) {
		Decode_Ways_91 test = new Decode_Ways_91();
		String s1 = "12";
		String s2 = "226";
		System.out.println(test.numDecodings(s1)); // 2
		System.out.println(test.numDecodings(s2)); // 3
	}

}
