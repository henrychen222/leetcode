//9.8
// https://www.programcreek.com/2013/12/leetcode-solution-of-longest-palindromic-substring-java/
//https://leetcode.com/problems/longest-palindromic-substring/
package DP;

public class Longest_Palindromic_Substring_5 {

	/**
	 * Method 1:Dynamic Programming, Time O(n^2), Space O(n^2) --- Wrong Answer, "ac" not pass
	 */
	public String longestPalindrome(String s) {
		if (s == null || s.length() <= 1) {
			return s;
		}

		int len = s.length();
		int maxLen = 1;
		boolean[][] dp = new boolean[len][len];

		String longest = null;
		for (int k = 0; k < len; k++) {
			for (int i = 0; i < len - k; i++) {
				int j = i + k;
				if (s.charAt(i) == s.charAt(j) && (j - i <= 2 || dp[i + 1][j - 1])) {
					dp[i][j] = true;
					
					if (j - i + 1 > maxLen) {
						maxLen = j - i + 1;
						longest = s.substring(i, j + 1);
					}
				}
			}
		}
		return longest;
	}

	/** 
	 * Method 2 A Simple Algorithm Time O(n^2), Space O(1) --- ACCEPT
	 */
	public String longestPalindrome_simple(String s) {
		if (s.isEmpty()) {
			return s; // change
		}

		if (s.length() == 1) {
			return s;
		}

		String longest = s.substring(0, 1);

		for (int i = 0; i < s.length(); i++) {

			// get longest palindrome with center of i
			String tmp = helper(s, i, i);
			if (tmp.length() > longest.length()) {
				longest = tmp;
			}

			// get longest palindrome with center of i, i+1
			tmp = helper(s, i, i + 1);
			if (tmp.length() > longest.length()) {
				longest = tmp;
			}

		}
		return longest;
	}

	// Given a center, either one letter or two letter, Find longest palindrome
	public String helper(String s, int begin, int end) {
		while (begin >= 0 && end <= s.length() - 1 && s.charAt(begin) == s.charAt(end)) {
			begin--;
			end++;
		}
		return s.substring(begin + 1, end);
	}

	public static void main(String[] args) {

		Longest_Palindromic_Substring_5 test = new Longest_Palindromic_Substring_5();
		String s1 = "babad";
		String s2 = "cbbd";
		String empty = "";
		String s3 = "ac";
		System.out.println(test.longestPalindrome(s1));
		System.out.println(test.longestPalindrome(s2));
		System.out.println(test.longestPalindrome(empty));
		System.out.println(test.longestPalindrome(s3));  //wrong

		System.out.println("\n---Method 2---");
		System.out.println(test.longestPalindrome_simple(s1));
		System.out.println(test.longestPalindrome_simple(s2));
		System.out.println(test.longestPalindrome_simple(empty));
		System.out.println(test.longestPalindrome_simple(s3));   //correct

	}

}
