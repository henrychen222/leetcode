//9.8   https://leetcode.com/problems/regular-expression-matching/
package DP;

public class Regular_Expression_Matching_10 {

	public boolean isMatch(String s, String p) {
		if (p.length() == 0) {
			return s.length() == 0;
		}

		// p's length 1 is special case
		if (p.length() == 1 || p.charAt(1) != '*') {
			if (s.length() < 1 || (p.charAt(0) != '.' && s.charAt(0) != p.charAt(0))) {
				return false;
			}
			return isMatch(s.substring(1), p.substring(1));
		} else {
			int len = s.length();
			int i = -1;
			while (i < len && (i < 0 || p.charAt(0) == '.' || p.charAt(0) == s.charAt(i))) {
				if (isMatch(s.substring(i + 1), p.substring(2))) {
					return true;
				}
				i++;
			}
			return false;
		}

	}

	public static void main(String[] args) {

		Regular_Expression_Matching_10 test = new Regular_Expression_Matching_10();
		String s1 = "aa";
		String p1 = "a";
		System.out.println(test.isMatch(s1, p1)); // false

		String s2 = "aa";
		String p2 = "a*";
		System.out.println(test.isMatch(s2, p2)); // true

		String s3 = "ab";
		String p3 = ".*";
		System.out.println(test.isMatch(s3, p3)); // true

		String s4 = "aab";
		String p4 = "c*a*b";
		System.out.println(test.isMatch(s4, p4)); // true

		String s5 = "mississippi";
		String p5 = "mis*is*p*.";
		System.out.println(test.isMatch(s5, p5)); // false

	}

}
