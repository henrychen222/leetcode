/**
 * 9.21 afternoon
 * https://leetcode.com/problems/maximum-product-subarray/
 */
package DP;

public class Maximum_Product_Subarray_152 {

	/**
	 * --- ACCEPT
	 */
	public int maxProduct(int[] nums) {
		int[] max = new int[nums.length];
		int[] min = new int[nums.length];

		max[0] = min[0] = nums[0];
		int result = nums[0];

		for (int i = 1; i < nums.length; i++) {
			if (nums[i] > 0) {
				max[i] = Math.max(nums[i], nums[i] * max[i - 1]);
				min[i] = Math.min(nums[i], nums[i] * min[i - 1]);
			} else {
				max[i] = Math.max(nums[i], nums[i] * min[i - 1]);
				min[i] = Math.min(nums[i], nums[i] * max[i - 1]);
			}
			result = Math.max(result, max[i]);
		}
		return result;
	}

	public static void main(String[] args) {
       Maximum_Product_Subarray_152 test = new Maximum_Product_Subarray_152();
       int [] nums1 = new int[] {2,3,-2,4};
       int [] nums2 = new int[] {-2,0,-1};
       System.out.println(test.maxProduct(nums1)); //6
       System.out.println(test.maxProduct(nums2)); //0
       
	}

}
