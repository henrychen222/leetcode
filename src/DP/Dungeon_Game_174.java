/**
 * 9.25 afternoon 
 * https://leetcode.com/problems/dungeon-game/
 */
package DP;

public class Dungeon_Game_174 {

	/**
	 * h[i][j] is the minimum health value before he enters (i,j). h[0][0] is the
	 * value of the answer. The left part is filling in numbers to the table.
	 * ---ACCEPT
	 */
	public int calculateMinimumHP(int[][] dungeon) {
		int m = dungeon.length;
		int n = dungeon[0].length;

		// init dp table
		int[][] dp = new int[m][n];

		dp[m - 1][n - 1] = Math.max(1 - dungeon[m - 1][n - 1], 1);

		// init last row
		for (int i = m - 2; i >= 0; i--) {
			dp[i][n - 1] = Math.max(dp[i + 1][n - 1] - dungeon[i][n - 1], 1);
		}

		// init last column
		for (int j = n - 2; j >= 0; j--) {
			dp[m - 1][j] = Math.max(dp[m - 1][j + 1] - dungeon[m - 1][j], 1);
		}

		// calculate dp table
		for (int i = m - 2; i >= 0; i--) {
			for (int j = n - 2; j >= 0; j--) {
				int down = Math.max(dp[i + 1][j] - dungeon[i][j], 1);
				int right = Math.max(dp[i][j + 1] - dungeon[i][j], 1);
				dp[i][j] = Math.min(down, right);
			}
		}

		return dp[0][0];
	}

	public static void main(String[] args) {
		Dungeon_Game_174 test = new Dungeon_Game_174();
		int[][] dungeon = { { -2, -3, 3 }, { -5, -10, 1 }, { 10, 30, -5 } };
		System.out.println(test.calculateMinimumHP(dungeon)); // 7
	}

}
