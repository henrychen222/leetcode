/**
 * 9.25 evening 
 * https://leetcode.com/problems/house-robber-ii/
 * compare with 198
 */
package DP;

public class House_Robber_II_213 {

	/**
	 * --- ACCEPT
	 */
	public int rob(int[] nums) {
		if (nums == null || nums.length == 0) {
			return 0;
		}
		if (nums.length == 1) {
			return nums[0];
		}

		int max1 = helper(nums, 0, nums.length - 2);
		int max2 = helper(nums, 1, nums.length - 1);

		return Math.max(max1, max2);
	}

	public int helper(int[] nums, int i, int j) {
		if (i == j) {
			return nums[i];
		}

		int[] dp = new int[nums.length];
		dp[i] = nums[i];
		dp[i + 1] = Math.max(nums[i + 1], dp[i]);

		for (int k = i + 2; k <= j; k++) {
			dp[k] = Math.max(dp[k - 1], dp[k - 2] + nums[k]);
		}
		return dp[j];
	}

	public static void main(String[] args) {
		House_Robber_II_213 test = new House_Robber_II_213();
		int[] nums1 = new int[] { 2, 3, 2 };
		int[] nums2 = new int[] { 1, 2, 3, 1 };
		System.out.println(test.rob(nums1)); // 3
		System.out.println(test.rob(nums2)); // 4
	}

}
