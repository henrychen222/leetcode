/**
 * 9.19 night
 * https://leetcode.com/problems/triangle/
 */
package DP;

import java.util.ArrayList;
import java.util.List;

public class Triangle_120 {

	/**
	 * Bottom Up
	 * --- ACCEPT
	 */
	public int minimumTotal(List<List<Integer>> triangle) {
		int[] total = new int[triangle.size()];
		int len = triangle.size() - 1;

		for (int i = 0; i < triangle.get(len).size(); i++) {
			total[i] = triangle.get(len).get(i);
		}

		// iterate from the last second row
		for (int i = triangle.size() - 2; i >= 0; i--) {
			for (int j = 0; j < triangle.get(i + 1).size() - 1; j++) {
				total[j] = triangle.get(i).get(j) + Math.min(total[j], total[j + 1]);
			}
		}
		return total[0];
	}

	public static void main(String[] args) {
		Triangle_120 test = new Triangle_120();

		List<List<Integer>> triangle = new ArrayList<List<Integer>>();

		ArrayList<Integer> t1 = new ArrayList<Integer>();
		t1.add(2);
		triangle.add(t1);

		ArrayList<Integer> t2 = new ArrayList<Integer>();
		t2.add(3);
		t2.add(4);
		triangle.add(t2);

		ArrayList<Integer> t3 = new ArrayList<Integer>();
		t3.add(6);
		t3.add(5);
		t3.add(7);
		triangle.add(t3);

		ArrayList<Integer> t4 = new ArrayList<Integer>();
		t4.add(4);
		t4.add(1);
		t4.add(8);
		t4.add(3);
		triangle.add(t4);

		System.out.println(triangle);
		System.out.println(test.minimumTotal(triangle));
	}

}
