/*
 * 9.18 night
 * https://leetcode.com/problems/interleaving-string/
 */
package DP;

public class Interleaving_String_97 {

	public boolean isInterleave(String s1, String s2, String s3) {
		int len1 = s1.length();
		int len2 = s2.length();
		int len3 = s3.length();

		// if s3 is interleaved by s1 and s2, len1 + len2 should equal to len3
		if (len1 + len2 != len3) {
			return false;
		}

		boolean[][] dp = new boolean[len1 + 1][len2 + 1];

		// s1 not empty，s2 empty. Compare if the string s1 and the string of s3 is
		// equal, of the current traversed position.
		for (int i = 0; i <= len1; i++) {
			// dp[i][0] = s1.substring(0, i).equals(s3.substring(0, i)) ? true : false;
			if (s1.substring(0, i).equals(s3.substring(0, i))) {
				dp[i][0] = true;
			} else {
				dp[i][0] = false;
			}
		}

		// s2 not empty，s1 empty
		for (int i = 0; i <= len2; i++) {
			dp[0][i] = s2.substring(0, i).equals(s3.substring(0, i)) ? true : false;
		}

		/**
		 * s1 and s2 not empty, two conditions: (1) from (i-1,j) tp (i，j), if (i-1，j)
		 * are interleaved, Compare whether the element of the ith position of s1 is
		 * equal to the element of the (i+j)th position of s3. (2) same way
		 */
		for (int i = 1; i <= len1; i++) {
			for (int j = 1; j <= len2; j++) {
				dp[i][j] = (dp[i - 1][j] && s1.charAt(i - 1) == s3.charAt(i + j - 1))
						|| (dp[i][j - 1] && s2.charAt(j - 1) == s3.charAt(i + j - 1));
			}
		}
		return dp[len1][len2];
	}

	public static void main(String[] args) {
		Interleaving_String_97 test = new Interleaving_String_97();
		String s1 = "aabcc";
		String s2 = "dbbca";
		String s3 = "aadbbcbcac";
		System.out.println(test.isInterleave(s1, s2, s3)); // true

		String s4 = "aabcc";
		String s5 = "dbbca";
		String s6 = "aadbbbaccc";
		System.out.println(test.isInterleave(s4, s5, s6)); // false

	}

}
