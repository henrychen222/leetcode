/**
 * 9.15 night 
 * https://leetcode.com/problems/climbing-stairs/
 * http://iryndin.net/post/leetcode_70_climbing_stairs/
 * https://blog.csdn.net/happyaaaaaaaaaaa/article/details/48948247
 */

package DP;

public class Climbing_Stairs_70 {

	/**
	 *
	 * Method 1: Recursion --- FAIL time limit exceed
	 */
	public int climbStairs(int n) {
		switch (n) {
		case 0:
			return 0;
		case 1:
			return 1;
		case 2:
			return 2;
		default:
			return climbStairs(n - 1) + climbStairs(n - 2);
		}
	}

	/**
	 * Method 2: DP ---ACCEPT
	 */
	public int climbStairs_DP(int n) {
		if (n <= 2) {
			return n;
		}

		int prevPrevStair = 1;
		int prevStair = 2;
		int curStair = 0;

		for (int i = 2; i < n; i++) {
			curStair = prevPrevStair + prevStair;
			prevPrevStair = prevStair;
			prevStair = curStair;

		}
		return curStair;
	}

	/**
	 * ---ACCEPT 
	 * 第 n 阶只与第 n - 1 阶和 第 n - 2 阶有关，关系为ways[n] = ways[n - 1] + ways[n -
	 * 2]， 存储的时候只需要2个存储单元，用ways[0]存 n - 2 阶的走法数，ways[1]存储 n - 1 阶走法数：
	 */
	public int climbStairs_DP_CSDN(int n) {
		int[] ways = { 1, 1 };
		for (int i = 1; i < n; i++) {
			int temp = ways[1];
			ways[1] += ways[0];
			ways[0] = temp;
		}
		return ways[1];
	}

	public static void main(String[] args) {
		Climbing_Stairs_70 test = new Climbing_Stairs_70();
		int n1 = 2;
		System.out.println(test.climbStairs_DP(n1));
		System.out.println(test.climbStairs_DP_CSDN(n1));
	}

}
