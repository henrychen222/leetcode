/**
 * 9.25 afternoon 9.25 evening
 * https://leetcode.com/problems/house-robber/
 */
package DP;

import java.util.Arrays;

public class House_Robber_198 {

	/**
	 * Method 1 Dynamic Programming --- ACCEPT, 0ms
	 */
	public int rob(int[] nums) {
		if (nums == null || nums.length == 0) {
			return 0;
		}
		if (nums.length == 1) {
			return nums[0];
		}

		int[] dp = new int[nums.length];
		dp[0] = nums[0];
		dp[1] = Math.max(nums[0], nums[1]);

		for (int i = 2; i < nums.length; i++) {
			dp[i] = Math.max(dp[i - 2] + nums[i], dp[i - 1]);
		}
		return dp[nums.length - 1];
	}

	/**
	 * Method 2: use even and odd, to track the maximum value so far as iterating
	 * the array --- ACCEPT 0ms
	 */
	public int rob2(int[] nums) {
		int even = 0;
		int odd = 0;
		if (nums == null || nums.length == 0) {
			return 0;
		}
		for (int i = 0; i < nums.length; i++) {
			if (i % 2 == 0) {
				even += nums[i];
				even = even > odd ? even : odd;
			} else {
				odd += nums[i];
				odd = even > odd ? even : odd;
			}
		}
		return even > odd ? even : odd;
	}

	/**
	 * Method 3 Dynamic Programming with Memorization --- ACCEPT 0ms
	 */
	public int rob3(int[] nums) {
		if (nums.length == 0) {
			return 0;
		}

		int[] mem = new int[nums.length + 1];
		Arrays.fill(mem, -1);

		mem[0] = 0;
		return helper(nums.length, mem, nums);
	}

	private int helper(int size, int[] mem, int[] nums) {
		if (size < 1) {
			return 0;
		}
		if (mem[size] != -1) {
			return mem[size];
		}

		// two cases
		int firstSelected = helper(size - 2, mem, nums) + nums[nums.length - size];
		int firstUnSelected = helper(size - 1, mem, nums);

		return mem[size] = Math.max(firstSelected, firstUnSelected);
	}

	public static void main(String[] args) {
		House_Robber_198 test = new House_Robber_198();
		int[] nums1 = new int[] { 1, 2, 3, 1 };
		int[] nums2 = new int[] { 2, 7, 9, 3, 1 };

		// Method 1
		System.out.println(test.rob(nums1)); // 4
		System.out.println(test.rob(nums2)); // 12

		// Method 2
		System.out.println(test.rob2(nums1)); // 4
		System.out.println(test.rob2(nums2)); // 12

		// Method 3
		System.out.println(test.rob3(nums1)); // 4
		System.out.println(test.rob3(nums2)); // 12
	}

}
