//7.4 Pass
// https://leetcode.com/problems/russian-doll-envelopes/
package DP;

import java.util.Arrays;
import java.util.Comparator;

public class RussianDollEnvelopes_354_CSDN {

	public int maxEnvelopes(int[][] envelopes) {
		if (envelopes == null || envelopes.length == 0) {
			return 0;
		}

		Comparator<int[]> cmp = new Comparator<int[]>() {
			@Override
			public int compare(int[] a, int[] b) {
				if (a[0] == b[0]) {
					return a[1] - b[1];
				} else {
					return a[0] - b[0];
				}
			}
		};
		Arrays.sort(envelopes, cmp);

		int arr[] = new int[envelopes.length];
        Arrays.fill(arr, 1);  //......
		int max = 1;
		for (int i = 0; i < envelopes.length; i++) {
			//.......
			for (int j = 0; j < i; j++) {
				if (envelopes[j][0] < envelopes[i][0] && envelopes[j][1] < envelopes[i][1]) {
					arr[i] = Math.max(arr[j] + 1, arr[i]);
				}
			}
			max = Math.max(arr[i], max);
		}
		return max;
	}

	public static void main(String[] args) {

		int[][] envelopes = { { 5, 4 }, { 6, 4 }, { 6, 7 }, { 2, 3 } };

		RussianDollEnvelopes_354_CSDN test = new RussianDollEnvelopes_354_CSDN();
		int result = test.maxEnvelopes(envelopes);
		System.out.println(result);

	}

}
