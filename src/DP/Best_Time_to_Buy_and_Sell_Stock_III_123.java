/**
 * 9.19 night  9.20 night 
 * https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii
 */
package DP;

public class Best_Time_to_Buy_and_Sell_Stock_III_123 {

	/**
	 * ---ACCEPT
	 */
	public int maxProfit(int[] prices) {
		if (prices == null || prices.length < 2) {
			return 0;
		}

		// highest profit in 0 ... i
		int[] left = new int[prices.length];
		int[] right = new int[prices.length];

		// DP from left to right
		left[0] = 0;
		int min = prices[0];
		for (int i = 1; i < prices.length; i++) {
			min = Math.min(min, prices[i]);
			left[i] = Math.max(left[i - 1], prices[i] - min);

		}

		// DP from right to left
		right[prices.length - 1] = 0;
		int max = prices[prices.length - 1];
		for (int i = prices.length - 2; i > 0; i--) {
			max = Math.max(max, prices[i]);
			right[i] = Math.max(right[i - 1], max - prices[i]);
		}

		int profit = 0;
		for (int i = 0; i < prices.length; i++) {
			profit = Math.max(profit, left[i] + right[i]);
		}

		return profit;
	}

	public static void main(String[] args) {
		Best_Time_to_Buy_and_Sell_Stock_III_123 test = new Best_Time_to_Buy_and_Sell_Stock_III_123();
		int[] prices1 = new int[] { 3, 3, 5, 0, 0, 3, 1, 4 };
		int[] prices2 = new int[] { 1, 2, 3, 4, 5 };
		int[] prices3 = new int[] { 7, 6, 4, 3, 1 };
		System.out.println(test.maxProfit(prices1)); // 6
		System.out.println(test.maxProfit(prices2)); // 4
		System.out.println(test.maxProfit(prices3)); // 0

	}

}
