/**
 * 9.20 night
 * https://leetcode.com/problems/palindrome-partitioning-ii/
 */
package DP;

public class Palindrome_Partitioning_II_132 {

	/**
	 * --- ACCEPT
	 */
	public int minCut(String s) {
		// tracks the partition position
		boolean[][] dp = new boolean[s.length()][s.length()];
		// tracks the number of minimum cut
		int[] cut = new int[s.length()];

		for (int j = 0; j < s.length(); j++) {
			cut[j] = j;  //set maximum
			for (int i = 0; i <= j; i++) {
				if (s.charAt(i) == s.charAt(j) && (j - i <= 1 || dp[i + 1][j - 1])) {
					dp[i][j] = true;

					// if need to cut, add 1 to the previous cut[i-1]
					if (i > 0) {
						cut[j] = Math.min(cut[j], cut[i - 1] + 1);
					} else {
						// if [0...j] is palindrome, no need to cut
						cut[j] = 0;
					}
				}
			}
		}

		return cut[s.length() - 1];
	}

	public static void main(String[] args) {
		Palindrome_Partitioning_II_132 test = new Palindrome_Partitioning_II_132();
		String s1 = "aab";
		System.out.println(test.minCut(s1));

	}

}
