/**
 * 9.16 morning
 * https://leetcode.com/problems/maximal-rectangle/
 */
package DP;

import java.util.Stack;

public class Maximal_Rectangle_85 {

	/**
	 * ---ACCEPT
	 */
	public int maximalRectangle(char[][] matrix) {
		int m = matrix.length;
		int n = m == 0 ? 0 : matrix[0].length;

		int[][] dp = new int[m][n + 1];

		int maxArea = 0;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (matrix[i][j] == '0') {
					dp[i][j] = 0;
				} else {
					dp[i][j] = i == 0 ? 1 : dp[i - 1][j] + 1;
				}
			}
		}

		for (int i = 0; i < m; i++) {
			int area = maxAreaInHistogram(dp[i]);
			if (area > maxArea) {
				maxArea = area;
			}
		}

		return maxArea;
	}

	private int maxAreaInHistogram(int[] height) {
		Stack<Integer> stack = new Stack<Integer>();
		int i = 0;
		int max = 0;
		while (i < height.length) {
			if (stack.isEmpty() || height[stack.peek()] <= height[i]) {
				// i++;
				// stack.push(i);
				stack.push(i++);
			} else {
				max = Math.max(max, height[stack.pop()] * (stack.isEmpty() ? i : i - stack.peek() - 1));
			}
		}
		return max;
	}

	public static void main(String[] args) {
		Maximal_Rectangle_85 test = new Maximal_Rectangle_85();
		char[][] matrix = { { '1', '0', '1', '0', '0' }, { '1', '0', '1', '1', '1' }, { '1', '1', '1', '1', '1' },
				{ '1', '0', '0', '1', '0' } };
		System.out.println(test.maximalRectangle(matrix));

	}

}
