//9.9
// https://leetcode.com/problems/wildcard-matching/
// https://www.programcreek.com/2014/06/leetcode-wildcard-matching-java/
package DP;

public class Wildcard_Matching_44 {

	//ACCEPT
	public boolean isMatch(String s, String p) {
		int i = 0;
		int j = 0;
		int starIndex = -1;
		int questionMarkIndex = -1;
		while (i < s.length()) {
			if (j < p.length() && (p.charAt(j) == '?' || p.charAt(j) == s.charAt(i))) {
				i++;
				j++;
			} else if (j < p.length() && p.charAt(j) == '*') {
				starIndex = j;
				questionMarkIndex = i;
				j++;
			} else if (starIndex != -1) {
				j = starIndex + 1;
				i = questionMarkIndex + 1;
				questionMarkIndex++;
			} else {
				return false;
			}
		}

		while (j < p.length() && p.charAt(j) == '*') {
			j++;
		}
		
		return j == p.length();
	}

	public static void main(String[] args) {

		Wildcard_Matching_44 test = new Wildcard_Matching_44();
		
		String s1 = "aa";
		String p1 = "a";
		System.out.println(test.isMatch(s1, p1)); // false

		String s2 = "aa";
		String p2 = "*";
		System.out.println(test.isMatch(s2, p2)); // true

		String s3 = "cb";
		String p3 = "?a";
		System.out.println(test.isMatch(s3, p3)); // false

		String s4 = "adceb";
		String p4 = "*a*b";
		System.out.println(test.isMatch(s4, p4)); // true

		String s5 = "acdcb";
		String p5 = "a*c?b";
		System.out.println(test.isMatch(s5, p5)); // false

	}

}
