/**
 * 8.6 night
 * https://leetcode.com/problems/add-to-array-form-of-integer/
 */

package Array;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

class E_989_AddToArrayFormOfInteger {

	// Accepted --- 138ms 42MB 5.19%
	public List<Integer> addToArrayForm(int[] A, int K) {
		String tmp = "";
		for (int i : A) {
			tmp += i + "";
		}
		// System.out.println(s);
		BigInteger A_bi = new BigInteger(tmp);
		BigInteger K_bi = new BigInteger(K + "");
		BigInteger data = A_bi.add(K_bi);
		// System.out.println(A_bi);
		// System.out.println(K_bi);
		// System.out.println(data);
		List<Integer> res = new ArrayList<Integer>();
		String s = data.toString();
		for (int i = 0; i < s.length(); i++) {
			res.add(Character.getNumericValue(s.charAt(i)));
		}
		return res;
	}

	public static void main(String[] args) {
		int[] A = new int[] { 1, 2, 0, 0 };
		int K = 34;
		int[] A2 = new int[] { 2, 7, 4 };
		int K2 = 181;
		int[] A3 = new int[] { 2, 1, 5 };
		int K3 = 806;
		int[] A4 = new int[] { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 };
		int K4 = 1;
		int[] A_debug1 = new int[] { 1, 2, 6, 3, 0, 7, 1, 7, 1, 9, 7, 5, 6, 6, 4, 4, 0, 0, 6, 3 };
		int K_debug1 = 516;

		E_989_AddToArrayFormOfInteger test = new E_989_AddToArrayFormOfInteger();
		System.out.println(test.addToArrayForm(A, K));
		System.out.println(test.addToArrayForm(A2, K2));
		System.out.println(test.addToArrayForm(A3, K3));
		System.out.println(test.addToArrayForm(A4, K4));
		System.out.println(test.addToArrayForm(A_debug1, K_debug1)); // [1,2,6,3,0,7,1,7,1,9,7,5,6,6,4,4,0,5,7,9]

	}
}