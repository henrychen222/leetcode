/**
 * 8.14 night
 * https://leetcode.com/problems/binary-prefix-divisible-by-5/
 */

package Array;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class E_1018_BinaryPrefixDivisibleBy5 {

	public List<Boolean> prefixesDivBy5(int[] A) {
		List<Boolean> res = new ArrayList<>();
		for (int i = 0; i < A.length; i++) {
			BigInteger each = new BigInteger(concatenateDigits(Arrays.copyOfRange(A, 0, i + 1)));
			System.out.println(concatenateDigits(Arrays.copyOfRange(A, 0, i + 1)));
			// System.out.println(each);
			// System.out.println(Arrays.toString(Arrays.copyOfRange(A, 0, i + 1)));
			BigInteger five = new BigInteger("5");
			BigInteger remain = each.remainder(five); // issue
			System.out.println(remain); 
			if (remain.equals(BigInteger.ZERO)) {
				res.add(true);
			} else {
				res.add(false);
			}
		}
		return res;
	}

	public static String concatenateDigits(int[] arr) {
		String res = "";
		for (int item : arr) {
			res += (item + "");
		}
		return res.toString();
	}

	public static void main(String[] args) {
		int[] A = new int[] { 0, 1, 1 };
		int[] A2 = new int[] { 1, 1, 1 };
		int[] A3 = new int[] { 0, 1, 1, 1, 1, 1 };
		int[] A4 = new int[] { 1, 1, 1, 0, 1 };
		int[] debug1 = new int[] { 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1,
				0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1 };

		E_1018_BinaryPrefixDivisibleBy5 test = new E_1018_BinaryPrefixDivisibleBy5();
//		 System.out.println(test.prefixesDivBy5(A)); // [true,false,false]
//		 System.out.println(test.prefixesDivBy5(A2)); // [false,false,false]
//		System.out.println(test.prefixesDivBy5(A3)); // [true,false,false,false,true,false]
		 System.out.println(test.prefixesDivBy5(A4)); // [false,false,false,false,false]
		// System.out.println(test.prefixesDivBy5(debug1));
		// [false,false,true,true,true,true,true,true,true,true,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true,true,false,false,false,true,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true,true,true,false,false,false]

	}

}
