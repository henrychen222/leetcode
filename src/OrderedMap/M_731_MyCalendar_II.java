/**
 * 06/21/21 evening
 * https://leetcode.com/problems/my-calendar-ii/
 * 
 * reference:
 * https://www.cnblogs.com/grandyang/p/7968035.html
 */

package OrderedMap;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.TreeMap;
import java.util.TreeSet;

class MyCalendarTwo {
	class Pair implements Comparable<Pair> {
		int first, second;
		Pair(int first, int second) {
			this.first = first;
			this.second = second;
		}
		@Override
		public int compareTo(Pair p) { // issue
			return first - p.first;
			// return Integer.compare(p.first, p.second);
		}
	}

	TreeSet<Pair> ts1;
	TreeSet<Pair> ts2;

	public MyCalendarTwo() {
		ts1 = new TreeSet<>();
		ts2 = new TreeSet<>();
	}

	public boolean book(int start, int end) {
		for (Pair p : ts2) {
			if (start > p.second || end <= p.first) continue;
			return false;
		}
		for (Pair p : ts1) {
			if (start >= p.second || end <= p.first) continue;
			ts2.add(new Pair(Math.max(start, p.first), Math.min(end, p.second)));
		}
		ts1.add(new Pair(start, end));
		return true;
	}
}

//Accepted --- 1380ms 5.01%
class MyCalendarTwo1 {

	TreeMap<Integer, Integer> tm;

	public MyCalendarTwo1() {
		tm = new TreeMap<>();
	}

	public boolean book(int start, int end) {
		tm.put(start, tm.getOrDefault(start, 0) + 1);
		tm.put(end, tm.getOrDefault(end, 0) - 1);
		int cnt = 0;
		for (int k : tm.keySet()) {
			int occ = tm.get(k);
			cnt += occ;
			if (cnt == 3) {
				tm.put(start, tm.getOrDefault(start, 0) - 1);
				tm.put(end, tm.getOrDefault(end, 0) + 1);
				return false;
			}
		}
		return true;
	}
}

public class M_731_MyCalendar_II {

	static PrintWriter pw;

	public void run() {
		MyCalendarTwo myCalendarTwo = new MyCalendarTwo();
		pw.println(myCalendarTwo.book(10, 20)); // true
		pw.println(myCalendarTwo.book(50, 60)); // true
		pw.println(myCalendarTwo.book(10, 40)); // true
		pw.println(myCalendarTwo.book(5, 15)); // false
		pw.println(myCalendarTwo.book(5, 10)); // true
		pw.println(myCalendarTwo.book(25, 55)); // true
	}

	public static void main(String[] args) {
		pw = new PrintWriter(System.out);
		new M_731_MyCalendar_II().run();
		pw.close();
	}

	void tr(Object... o) {
		pw.println(Arrays.deepToString(o));
	}

}
