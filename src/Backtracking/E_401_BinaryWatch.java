// 5.20 evening
// https://www.cnblogs.com/grandyang/p/9065702.html
package Backtracking;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class E_401_BinaryWatch {

    // not fixed
    public List<String> readBinaryWatch(int num) {
        List<String> res = new ArrayList<>();
        for (int h = 0; h < 12; ++h) {
            for (int m = 0; m < 60; ++m) {
                BitSet bs = new BitSet(10);
                bs.set((h << 6) + m);  // problem
                System.out.println(bs.toString());
                if (bs.cardinality() == num) {
                    res.add(h + (m < 10 ? ":0" : ":") + m);
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        int n = 1;
        E_401_BinaryWatch test = new E_401_BinaryWatch();
        System.out.println(test.readBinaryWatch(n));
    }

}