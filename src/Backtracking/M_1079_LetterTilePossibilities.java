/**
 * 5.21 night
 * https://www.cnblogs.com/hwd9654/p/11008562.html
 */
package Backtracking;

import java.util.Arrays;

public class M_1079_LetterTilePossibilities {

    public int numTilePossibilities(String tiles) {
        if (tiles.length() == 0) {
            return 0;
        }
        int[] count = new int[26];
        System.out.println(Arrays.toString(count));
        for (char c : tiles.toCharArray()) {
            count[c - 'A']++;
        }
        System.out.println(Arrays.toString(count));
        return dfs(count);
    }

    private int dfs(int[] array) {
        int sum = 0;
        for (int i = 0; i < 26; i++) {
            if (array[i] == 0) {
                continue;
            }
            sum++;
            array[i]--;
            sum += dfs(array);
            array[i]++;
        }
        return sum;
    }

    public static void main(String[] args) {
        String tiles = "AAB";
        String tiles2 = "AAABBC";

        M_1079_LetterTilePossibilities test = new M_1079_LetterTilePossibilities();
        System.out.println(test.numTilePossibilities(tiles));
        System.out.println(test.numTilePossibilities(tiles2));

    }

}