/**
 * 05/24/21 morning
 * https://leetcode.com/problems/distant-barcodes/
 */

package Heap;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class M_1054_DistantBarcodes {

	static PrintWriter pw;

	public Map<Integer, Integer> counter(int[] a) {
		Map<Integer, Integer> m = new HashMap<>();
		for (int e : a) {
			m.put(e, m.getOrDefault(e, 0) + 1);
		}
		return m;
	}

	// Accepted --- 35ms 44.81%
	public int[] rearrangeBarcodes(int[] b) {
		Map<Integer, Integer> m = counter(b);
		int n = b.length;
		PriorityQueue<int[]> pq = new PriorityQueue<>((x, y) -> y[1] - x[1]);
		for (int k : m.keySet()) {
			pq.add(new int[] { k, m.get(k) });
		}
		// pw.println(Arrays.deepToString(pq.toArray()));
		int[] res = new int[n];
		int i = 0;
		while (!pq.isEmpty()) {
			int[] f = pq.poll();
			int[] s = new int[0];
			if (!pq.isEmpty()) s = pq.poll();
			if (s.length > 0) {
				res[i] = f[0];
				res[i + 1] = s[0];
				f[1]--;
				s[1]--;
				i += 2;
				if (f[1] > 0) pq.add(f);
				if (s[1] > 0) pq.add(s);
			} else {
				res[i] = f[0];
				f[1]--;
				i++;
				if (f[1] > 0) pq.add(f);
			}
		}
		return res;
	}

	public void run() {
		int[] barcodes = { 1, 1, 1, 2, 2, 2 };
		int[] barcodes2 = { 1, 1, 1, 1, 2, 2, 3, 3 };
		pw.println(Arrays.toString(rearrangeBarcodes(barcodes)));
		pw.println(Arrays.toString(rearrangeBarcodes(barcodes2)));
	}

	public static void main(String[] args) {
		pw = new PrintWriter(System.out);
		new M_1054_DistantBarcodes().run();
		pw.close();
	}

}
