/**
 * 03/20/21 night   03/21/21 night complete
 * https://leetcode.com/contest/weekly-contest-233/problems/maximum-ascending-subarray-sum/
 */

package Heap;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class M_1801_NumberOrdersBacklog {

	private final long MOD = 1000000007L;

	// Accepted --- 54ms
	public int getNumberOfBacklogOrders3_clean(int[][] orders) {
		TreeMap<Integer, Integer> sell = new TreeMap<>();
		TreeMap<Integer, Integer> buy = new TreeMap<>(Collections.reverseOrder());
		for (int[] e : orders) {
			int price = e[0];
			int amount = e[1];
			int type = e[2];
			if (type == 0) {
				while (amount > 0 && !sell.isEmpty()) {
					int k = sell.firstKey();
					int v = sell.firstEntry().getValue();
					if (k <= price) {
						int take = Math.min(v, amount);
						amount -= take;
						v -= take;
						sell.put(k, v);
						if (v == 0)
							sell.remove(k);
					} else {
						break;
					}
				}
				if (amount > 0)
					buy.put(price, buy.getOrDefault(price, 0) + amount);
			} else {
				while (amount > 0 && !buy.isEmpty()) {
					int k = buy.firstKey();
					int v = buy.firstEntry().getValue();
					if (k >= price) {
						int take = Math.min(v, amount);
						amount -= take;
						v -= take;
						buy.put(k, v);
						if (v == 0)
							buy.remove(k);
					} else {
						break;
					}
				}
				if (amount > 0)
					sell.put(price, sell.getOrDefault(price, 0) + amount);
			}
		}
		long res = 0L;
		for (long v : buy.values()) {
			res += v;
		}
		for (long v : sell.values()) {
			res += v;
		}
		return (int) (res % MOD);
	}

	// Accepted --- 61ms
	// https://leetcode.com/contest/weekly-contest-233/ranking A_Le_K
	public int getNumberOfBacklogOrders3(int[][] orders) {
		TreeMap<Integer, Integer> sell = new TreeMap<>();
		TreeMap<Integer, Integer> buy = new TreeMap<>(Collections.reverseOrder());
		for (int[] e : orders) {
			int price = e[0];
			int amount = e[1];
			int type = e[2];
			if (type == 0) {
				// System.out.println("buy: " + price);
				while (amount > 0 && !sell.isEmpty()) {
					int k = 0;
					if (sell.firstKey() != null) { // no need, cuz sell is not empty
						k = sell.firstKey();
					}
					int v = 0;
					if (sell.firstEntry().getValue() != null) {
						v = sell.firstEntry().getValue();
					}
					// System.out.println(k + " " + v);
					if (k <= price) {
						int take = Math.min(v, amount);
						amount -= take;
						v -= take;
						sell.put(k, v); // forget this line, c++ iterator will modify map
						if (v == 0)
							sell.remove(k);
					} else {
						break;
					}
				}
				if (amount > 0)
					buy.put(price, buy.getOrDefault(price, 0) + amount);
			} else {
				// System.out.println("sell: " + price);
				while (amount > 0 && !buy.isEmpty()) {
					int k = 0;
					if (buy.firstKey() != null) {
						k = buy.firstKey();
					}
					int v = 0;
					if (buy.firstEntry().getValue() != null) {
						v = buy.firstEntry().getValue();
					}
					// System.out.println(k + " " + v);
					if (k >= price) {
						int take = Math.min(v, amount);
						amount -= take;
						v -= take;
						buy.put(k, v);
						if (v == 0)
							buy.remove(k);
					} else {
						break;
					}
				}
				if (amount > 0)
					sell.put(price, sell.getOrDefault(price, 0) + amount);
			}
			System.out.println(buy + " " + sell);
		}
		long res = 0L;
		for (long v : buy.values()) {
			res += v;
		}
		for (long v : sell.values()) {
			res += v;
		}
		return (int) (res % MOD);
	}

	// kmjp
	// Accepted --- 48ms
	public int getNumberOfBacklogOrders(int[][] orders) {
		TreeMap<Integer, Integer> sell = new TreeMap<>();
		TreeMap<Integer, Integer> buy = new TreeMap<>();
		for (int[] e : orders) {
			int price = e[0];
			int amount = e[1];
			int type = e[2];
			if (type == 0) {
				// System.out.println("buy: " + price);
				while (amount > 0 && sell.size() > 0) {
					int k = sell.firstKey();
					if (k > price)
						break;
					int v = sell.firstEntry().getValue();
					int take = Math.min(amount, v);
					amount -= take;
					sell.put(k, v - take);
					if (v - take == 0)
						sell.remove(k);
				}
				if (amount > 0)
					buy.put(price, buy.getOrDefault(price, 0) + amount);
			} else {
				// System.out.println("sell: " + price);
				while (amount > 0 && buy.size() > 0) {
					int k = buy.lastKey();
					if (k < price)
						break;
					int v = buy.lastEntry().getValue();
					int take = Math.min(amount, v);
					amount -= take;
					// shit, here is wrong, copy from top, cost 1 hour to debug
//					sell.put(k, v - take);
//					if (v - take == 0)
//						sell.remove(k);
					buy.put(k, v - take);
					if (v - take == 0)
						buy.remove(k);
				}
				if (amount > 0)
					sell.put(price, sell.getOrDefault(price, 0) + amount);
			}
			// System.out.println(buy + " " + sell);
		}
		long res = 0L;
		for (long v : buy.values()) {
			res += v;
		}
		for (long v : sell.values()) {
			res += v;
		}
		return (int) (res % MOD);
	}

//	public int getNumberOfBacklogOrders1(int[][] orders) {
//		Map<Integer, Integer> sell = new TreeMap<>();
//		Map<Integer, Integer> buy = new TreeMap<>(Collections.reverseOrder());
//		outer: for (int[] e : orders) {
//			int price = e[0];
//			int amount = e[1];
//			int type = e[2];
//			if (type == 0) {
//				if (sell.size() == 0) {
//					buy.put(price, buy.getOrDefault(price, 0) + amount);
//					continue;
//				}
//				for (Map.Entry<Integer, Integer> cur : sell.entrySet()) {
//					int k = cur.getKey();
//					int v = cur.getValue();
//					if (k <= price) {
//						if (amount > v) {
//							sell.remove(k); // issue ConcurrentModificationException
//							amount -= v;
//						} else if (amount < v) {
//							sell.put(k, v - amount);
//							continue outer;
//						} else {
//							sell.remove(k);
//							amount -= v;
//							continue outer;
//						}
//					} else {
//						if (amount > 0)
//							buy.put(price, buy.getOrDefault(price, 0) + amount);
//						continue outer;
//					}
//				}
//				if (amount > 0)
//					buy.put(price, buy.getOrDefault(price, 0) + amount);
//			} else {
//				if (buy.size() == 0) {
//					sell.put(price, sell.getOrDefault(price, 0) + amount);
//					continue;
//				}
//				for (Map.Entry<Integer, Integer> cur : buy.entrySet()) {
//					int k = cur.getKey();
//					int v = cur.getValue();
//					if (k >= price) {
//						if (amount > v) {
//							// buy.remove(k);
//							amount -= v;
//						} else if (amount < v) {
//							buy.put(k, v - amount);
//							continue outer;
//						} else {
//							// buy.remove(k);
//							amount -= v;
//							continue outer;
//						}
//					} else {
//						if (amount > 0)
//							sell.put(price, sell.getOrDefault(price, 0) + amount);
//						continue outer;
//					}
//				}
//				if (amount > 0)
//					sell.put(price, sell.getOrDefault(price, 0) + amount);
//			}
//		}
//		long res = 0L;
//		for (long v : buy.values()) {
//			res += v;
//		}
//		for (long v : sell.values()) {
//			res += v;
//		}
//		return (int) (res % MOD);
//	}

	public static void main(String[] args) {
		int[][] orders = { { 10, 5, 0 }, { 15, 2, 1 }, { 25, 1, 1 }, { 30, 4, 0 } };
		int[][] orders2 = { { 7, 1000000000, 1 }, { 15, 3, 0 }, { 5, 999999995, 0 }, { 5, 1, 1 } };
		int[][] orders_debug1 = { { 1, 29, 1 }, { 22, 7, 1 }, { 24, 1, 0 }, { 25, 15, 1 }, { 18, 8, 1 }, { 8, 22, 0 },
				{ 25, 15, 1 }, { 30, 1, 1 }, { 27, 30, 0 } };
		int[][] orders_debug2 = { { 26, 5, 1 }, { 14, 7, 1 }, { 22, 4, 1 }, { 30, 18, 1 }, { 30, 29, 1 }, { 25, 18, 0 },
				{ 3, 24, 0 }, { 3, 5, 0 }, { 6, 30, 0 }, { 9, 25, 1 } };
		M_1801_NumberOrdersBacklog t = new M_1801_NumberOrdersBacklog();
		System.out.println(t.getNumberOfBacklogOrders(orders)); // 6
		System.out.println(t.getNumberOfBacklogOrders(orders2)); // 999999984
		System.out.println(t.getNumberOfBacklogOrders(orders_debug1)); // 22
		System.out.println(t.getNumberOfBacklogOrders(orders_debug2)); // 129
	}

}
