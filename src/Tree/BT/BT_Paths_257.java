/* 2.15 evening 2019
 * https://leetcode.com/problems/binary-tree-paths/
 * */

package Tree.BT;

import java.util.ArrayList;
import java.util.List;

public class BT_Paths_257 {

	public List<String> binaryTreePaths(TreeNode root) {
		String sb = "";
		ArrayList<String> result = new ArrayList<String>();
		
		simplified_DFS(root, result, sb);
		return result;
	}

	public void simplified_DFS(TreeNode root, ArrayList<String> result, String s) {
		// root not exist
		if (root == null) {
			return;
		}

		s = s + "->" + root.val;

		// only has root
		if (root.left == null && root.right == null) {
			result.add(s.substring(2)); // beginIndex
			return;
		}

		if (root.left != null) {
			simplified_DFS(root.left, result, s);
		}
		if (root.right != null) {
			simplified_DFS(root.right, result, s);
		}
	}

	public static void main(String[] args) {
		BT_Paths_257 test = new BT_Paths_257();

		// Create Tree [1,2,3,null,5]
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.left.right = new TreeNode(5);

		// check output
		List<String> result = test.binaryTreePaths(root);
		System.out.println(result);
		
	}

}
