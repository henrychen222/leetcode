/* 1.28 created   3.13 night 2019
 * https://leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/
 * compare to 105
 * */

package Tree.BT;

public class BT_InOrder_PostOrder_106 {

	public TreeNode buildTree(int[] inorder, int[] postorder) {
		int in_start = 0;
		int in_end = inorder.length - 1;
		int post_start = 0;
		int post_end = postorder.length - 1;
		return create(inorder, in_start, in_end, postorder, post_start, post_end);
	}

	public TreeNode create(int[] inorder, int in_start, int in_end, int[] postorder, int post_start, int post_end) {
		if (in_start > in_end || post_start > post_end) {
			return null;
		}
		// get the root value with post order, and create root
		int root_val = postorder[post_end];
		TreeNode root = new TreeNode(root_val);

		int k = 0;
		for (int i = 0; i < inorder.length; i++) {
			if (inorder[i] == root_val) {
				k = i;
				break;
			}
		}
		root.left = create(inorder, in_start, k - 1, postorder, post_start, post_start + k - (in_start + 1));
		root.right = create(inorder, k + 1, in_end, postorder, post_start + k - in_start, post_end - 1);
		return root;
	}

	public static void main(String args[]) {
			BT_InOrder_PostOrder_106 test = new BT_InOrder_PostOrder_106();

			// Create Tree
			TreeNode root = new TreeNode(3);
			root.left = new TreeNode(9);
			root.right = new TreeNode(20);
			root.right.left = new TreeNode(15);
			root.right.right = new TreeNode(7);
			
			//test in order and post order result
			root.printInorder(root);
			System.out.println();
			root.printPostorder(root);
			
			System.out.println("\n");
			int[] inorder = {9,3,15,20,7};
			int[] postorder = {9,3,15,20,7};
			TreeNode result = test.buildTree(inorder, postorder);
			root.printPreorder(result);
			System.out.println();
			root.printInorder(result);
			System.out.println();
			root.printPostorder(result);
			
	}
}