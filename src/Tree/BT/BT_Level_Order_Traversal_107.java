/* 3.17 afternoon howe center 4th floor     3.17 evening 2019
 * https://leetcode.com/problems/binary-tree-level-order-traversal-ii/
 * */
package Tree.BT;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;

public class BT_Level_Order_Traversal_107 {

	public List<List<Integer>> levelOrderBottom(TreeNode root) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		LinkedList<TreeNode> current = new LinkedList<TreeNode>();
		LinkedList<TreeNode> next = new LinkedList<TreeNode>();
		ArrayList<Integer> number = new ArrayList<Integer>();

		if (root == null) {
			return result;
		}

		current.offer(root);

		// track when each level starts
		while (!current.isEmpty()) {
			TreeNode node = current.poll(); // add the head to List
			number.add(node.val);
			if (node.left != null) {
				next.offer(node.left);
			}
			if (node.right != null) {
				next.offer(node.right);
			}

			if (current.isEmpty()) {
				current = next;
				next = new LinkedList<TreeNode>(); // refresh LinkedList
				result.add(number);
				number = new ArrayList<Integer>(); // refresh ArrayList
			}
		}

		// reverse result
		List<List<Integer>> result_reverse = new ArrayList<List<Integer>>();
		for (int i = result.size() - 1; i >= 0; i--) {
			result_reverse.add(result.get(i));
		}
		return result_reverse;
	}

	public static void main(String[] args) {
		BT_Level_Order_Traversal_107 test = new BT_Level_Order_Traversal_107();

		// Create Tree [3,9,20,null,null,15,7]
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);

		List<List<Integer>> result = new ArrayList<List<Integer>>();
		result = test.levelOrderBottom(root);
		System.out.println(result);

	}

}
