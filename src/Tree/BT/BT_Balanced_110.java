/* 3.17 evening 2019
 * https://leetcode.com/problems/balanced-binary-tree/
 * */
package Tree.BT;

public class BT_Balanced_110 {

	public boolean isBalanced(TreeNode root) {
		if (root == null) {
			return true;
		}
		if (getHeight(root) == -1) {
			return false;
		}
		return true;
	}

	public int getHeight(TreeNode root) {
		if (root == null) {
			return 0;
		}
		int left_height = getHeight(root.left);
		int right_height = getHeight(root.right);
		if (left_height == -1 || right_height == -1) {
			return -1;
		}
		if (Math.abs(left_height - right_height) > 1) {
			return -1;
		}
		return Math.max(left_height, right_height) + 1;
	}

	public static void main(String[] args) {
		BT_Balanced_110 test = new BT_Balanced_110();

		// Create Tree [3,9,20,null,null,15,7]
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);

		boolean result = test.isBalanced(root);
		System.out.println(result);

	}

}
