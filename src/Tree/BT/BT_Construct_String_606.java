/* 2.26 afternoon library 2019
 * https://leetcode.com/problems/construct-string-from-binary-tree/
 * */

package Tree.BT;

public class BT_Construct_String_606 {

	public void DFS(TreeNode node, StringBuilder sb) {
		if (node == null) {
			return;
		}
		sb.append(node.val);
		if (node.left != null) {
			sb.append("(");
			DFS(node.left, sb);
			sb.append(")");
		}
		if (node.right != null) {
			if (node.left == null) {
				sb.append("()");
			}
			sb.append("(");
			DFS(node.right, sb);
			sb.append(")");
		}
	}

	public String tree2str(TreeNode t) {
		StringBuilder sb = new StringBuilder();
		DFS(t, sb);
		return sb.toString();
	}

	public static void main(String[] args) {
		BT_Construct_String_606 test = new BT_Construct_String_606();

		// Create Tree [1,2,3,4]
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.left.left = new TreeNode(4);

		String result = test.tree2str(root);
		System.out.println(result); // "1(2(4))(3)"

	}
}
