/*2.25 noon   2.26 afternoon library 2019
 * https://leetcode.com/problems/subtree-of-another-tree/ 
 * */

package Tree.BT;

public class SubTree_572 {

	public boolean isSubtree(TreeNode s, TreeNode t) {
		if (s == null) {
			return false;
		}
		if (check_if_same(s, t)) {
			return true;
		}
		return isSubtree(s.left, t) || isSubtree(s.right, t);
	}

	public boolean check_if_same(TreeNode subtree, TreeNode tree) {
		if (subtree == null && tree == null) {
			return true;
		} else if (subtree == null || tree == null) {
			return false;
		} else {
			// subtree and tree exist, check Node value
			if (subtree.val != tree.val) {
				return false;
			}
		}
		// both left and right part should be the same
		return check_if_same(subtree.left, tree.left) && check_if_same(subtree.right, tree.right);
	}

	public static void main(String[] args) {
		SubTree_572 test = new SubTree_572();

		// Create TreeNode t [3,4,5,1,2]
		TreeNode t = new TreeNode(3);
		t.left = new TreeNode(4);
		t.right = new TreeNode(5);
		t.left.left = new TreeNode(1);
		t.left.right = new TreeNode(2);

		// Create TreeNode t2 [3,4,5,1,2,0]
		TreeNode t2 = new TreeNode(3);
		t2.left = new TreeNode(4);
		t2.right = new TreeNode(5);
		t2.left.left = new TreeNode(1);
		t2.left.right = new TreeNode(2);
		t2.left.right.left = new TreeNode(0);

		// Create TreeNode s [4,1,2]
		TreeNode s = new TreeNode(4);
		s.left = new TreeNode(1);
		s.right = new TreeNode(2);

		boolean result = test.isSubtree(s, t);
		System.out.println(result); // True   Don't know why false, Leetcode Run true
		boolean result2 = test.isSubtree(s, t2);
		System.out.println(result2); // False
	}

}
