/* 2.18 night 2019
 * https://leetcode.com/problems/path-sum-iii/
 * */

package Tree.BT;

public class Path_Sum_III_437 {
	public int DFS(TreeNode root, int sum) {
		if (root == null) {
			return 0;
		}
		int count = 0;
		if (sum == root.val) {
			count++;
		}
		count += DFS(root.left, sum - root.val);
		count += DFS(root.right, sum - root.val);  
		return count;
	}

	public int pathSum(TreeNode root, int sum) {
		if (root == null) {
			return 0;
		}
		
		return DFS(root, sum) + pathSum(root.left, sum) + pathSum(root.right, sum); // ??
	}

	public static void main(String[] args) {
		Path_Sum_III_437 test = new Path_Sum_III_437();

		// Create Tree [10,5,-3,3,2,null,11,3,-2,null,1]
		TreeNode root = new TreeNode(10);
		root.left = new TreeNode(5);
		root.right = new TreeNode(-3);
		root.left.left = new TreeNode(3);
		root.left.right = new TreeNode(2);
		root.right.right = new TreeNode(11);
		root.left.left.left = new TreeNode(3);
		root.left.left.right = new TreeNode(-2);
		root.left.right.right = new TreeNode(1);
		
		int sum = 8;
		int result = test.pathSum(root, sum);
		System.out.println(result);

	}

}
