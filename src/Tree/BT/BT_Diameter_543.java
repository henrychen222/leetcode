/* 2.24 afternoon 2019
 * https://leetcode.com/problems/diameter-of-binary-tree/
 * */
package Tree.BT;

public class BT_Diameter_543 {
	// 二叉树的直径：二叉树中从一个结点到另一个节点最长的路径，叫做二叉树的直径
	// 采用分治和递归的思想：根节点为root的二叉树的直径 =
	// Max(左子树直径，右子树直径，左子树的最大深度（不包括根节点）+右子树的最大深度（不包括根节点）+1)
	int diameter = 0;

	public int getDepth(TreeNode root) {
		if (root == null) {
			return 0;
		}
		int left = getDepth(root.left);
		int right = getDepth(root.right);

		diameter = Math.max(diameter, left + right);
		int depth = Math.max(left, right) + 1;
		return depth;
	}

	public int diameterOfBinaryTree(TreeNode root) {
		getDepth(root);
		return diameter;
	}

	public static void main(String[] args) {
		BT_Diameter_543 test = new BT_Diameter_543();

		// Create Tree [1,2,3,4,5]
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(5);

		int result = test.diameterOfBinaryTree(root);
		System.out.println(result);
	}

}
