/* 2.14 night 2019
 * https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/
 * */

package Tree.BT;

public class BT_LowestCommonAncestor_236 {

	public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
		if (root == null) {
			return null;
		}
		// any of them is root
		if (p == root || q == root) {
			return root;
		}

		TreeNode left_node = lowestCommonAncestor(root.left, p, q);
		TreeNode right_node = lowestCommonAncestor(root.right, p, q);

		// left_node and right_node both exist
		if (left_node != null && right_node != null) {
			return root;
		}
		// left_node and right_node both not exist
		else if (left_node == null && right_node == null) {
			return null;
		}
		// left not exist, right exist
		else if (left_node == null && right_node != null) {
			return right_node;
		}
		// left exist, right not exist
		else {
			return left_node;
		}
	}

	public static void main(String[] args) {
		BT_LowestCommonAncestor_236 test = new BT_LowestCommonAncestor_236();

		// Create Tree [3,5,1,6,2,0,8,null,null,7,4]
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(5);
		root.right = new TreeNode(1);
		root.left.left = new TreeNode(6);
		root.left.right = new TreeNode(2);
		root.right.left = new TreeNode(0);
		root.right.right = new TreeNode(8);
		root.left.right.left = new TreeNode(7);
		root.left.right.right = new TreeNode(4);

		// check input parameters p = 5 q = 1
		TreeNode p = new TreeNode(5);
		TreeNode q = new TreeNode(1);

		// check output
		TreeNode result = test.lowestCommonAncestor(root, p, q);
		System.out.println(result);   // don't know why is null here
		System.out.println(result.val);  // Null Pointer 

	}

}
