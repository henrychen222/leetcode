/*
 * 2.18 night 2019
 * https://leetcode.com/problems/sum-of-left-leaves/
 * */

package Tree.BT;

import java.util.Stack;

public class Sum_Left_Leaves_404 {
	// Method 1
	public int sum_of_left_leaves(TreeNode root, boolean isLeft) {
		int sum = 0;
		if (root == null) {
			return 0;
		}
		if (root.left != null || root.right != null) {
			sum += sum_of_left_leaves(root.left, true);
			sum += sum_of_left_leaves(root.right, false);
		} else if (isLeft) {
			sum += root.val;
		}
		// if (isLeft) {
		// sum += root.val;
		// }
		return sum;
	}

	public int sumOfLeftLeaves(TreeNode root) {
		/* Method 1 use recursion */
		// return sum_of_left_leaves(root, false);

		/* Method 2 use Stack */
		if (root == null) {
			return 0;
		}
		int sum = 0;
		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(root);

		// this is loop, it will save all nodes into stack use push() with DFS
		while (!stack.isEmpty()) {
			TreeNode node = stack.pop();
			// the node depth 1 left
			if (node.left != null) {
				// until the node depth 2 left is empty, calculate the sum, otherwise continue
				// add left node ??
				if (node.left.left == null && node.left.right == null) {
					sum += node.left.val;
				} else {
					stack.push(node.left);
				}

			}
			// depth 1 right
			if (node.right != null) {
				// continue to add right node, if the node depth 2 right have leaves ??
				if (node.right.left != null || node.right.right != null) {
					stack.push(node.right);
				}
			}
		}
		return sum;
	}

	public static void main(String[] args) {
		Sum_Left_Leaves_404 test = new Sum_Left_Leaves_404();

		// Create Tree [3,9,20,null,null,15,7]
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);

		int result = test.sumOfLeftLeaves(root);
		System.out.println(result);

	}

}
