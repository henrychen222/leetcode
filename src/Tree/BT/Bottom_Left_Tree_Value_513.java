/* 2.23 noon 2019
 * https://leetcode.com/problems/find-bottom-left-tree-value/
 * 
 * https://docs.oracle.com/javase/7/docs/api/java/util/LinkedList.html
 *   poll(): Retrieves and removes the head (first element) of this list.
 *   offer(): Adds the specified element as the tail (last element) of this list.
 * */

package Tree.BT;

import java.util.LinkedList;

public class Bottom_Left_Tree_Value_513 {
	int result = 0;

	public int findBottomLeftValue(TreeNode root) {
		if (root == null) {
			return result;
		}
		BFS(root);
		return result;
	}

	public void BFS(TreeNode root) {
		LinkedList<TreeNode> lk = new LinkedList<TreeNode>();
		lk.add(root);
		// 首先存储第一层的结点值
		result = root.val;
		// 层次遍历，只需要记录本层结点个数即可
		int current_height_num = 1;
		// 下一层的节点数
		int next_height_num = 0;

		while (!lk.isEmpty()) {
			TreeNode node = lk.poll();
			current_height_num--;
			if (node.left != null) {
				lk.offer(node.left);
				next_height_num++;
			}
			if (node.right != null) {
				lk.offer(node.right);
				next_height_num++;
			}
			if (current_height_num == 0) {
				// 当下一层不空的时候，第一个结点就是最左侧结点
				if (!lk.isEmpty()) {
					result = lk.peek().val;
					current_height_num = next_height_num;
					next_height_num = 0;
				}
			}
		}
	}

	public static void main(String[] args) {
		Bottom_Left_Tree_Value_513 test = new Bottom_Left_Tree_Value_513();

		// Create Tree [2,1,3]
		TreeNode root = new TreeNode(2);
		root.left = new TreeNode(1);
		root.right = new TreeNode(3);

		int result = test.findBottomLeftValue(root);
		System.out.println(result);
	}
}
