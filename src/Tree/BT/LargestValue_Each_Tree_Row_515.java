/* 2.24 noon 2019
 * https://leetcode.com/problems/find-largest-value-in-each-tree-row/
 * poll(), offer() check 513
 * 
 * https://docs.oracle.com/javase/7/docs/api/java/util/List.html
 * set(): Replaces the element at the specified position in this list with the specified element 
 *  */
package Tree.BT;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LargestValue_Each_Tree_Row_515 {
	// Method 1
	public void BFS(TreeNode root, List<Integer> result) {
		LinkedList<TreeNode> lk = new LinkedList<TreeNode>();
		lk.add(root);
		// 层次遍历，只需要记录本层结点个数即可
		int current_height_num = 1;
		// 下一层的节点数
		int next_height_num = 0;
		int largest = Integer.MIN_VALUE;
		while (!lk.isEmpty()) {
			TreeNode node = lk.poll(); // Retrieve from the head, in each loop (Same as Queue)
			current_height_num--;
			// get the largest value in each row, in each loop
			largest = Math.max(largest, node.val);
			// breath first, add all nodes to lk
			if (node.left != null) {
				lk.offer(node.left);
				next_height_num++;
			}
			if (node.right != null) {
				lk.offer(node.right);
				next_height_num++;
			}
			// ??
			if (current_height_num == 0) {
				result.add(largest);
				// reset the parameters for the next line
				current_height_num = next_height_num;
				next_height_num = 0;
				largest = Integer.MIN_VALUE;
			}
		}
	}

	/*
	 * Method 2 a simple pre-order traverse idea DFS. Use depth to expand result
	 * list size and put the max value in the appropriate position
	 */
	private void preorder_DFS(TreeNode root, List<Integer> result, int depth) {
		if (root == null) {
			return;
		}
		// expand list size
		if (depth == result.size()) {
			result.add(root.val);
		} else {
			// or set value
			result.set(depth, Math.max(result.get(depth), root.val));
		}

		preorder_DFS(root.left, result, depth + 1);
		preorder_DFS(root.right, result, depth + 1);
	}

	public List<Integer> largestValues(TreeNode root) {
//		// Method 1
//		List<Integer> result = new ArrayList<Integer>();
//		if (root == null) {
//			return result;
//		}
//		BFS(root, result);
//		return result;

		// Method 2
		List<Integer> result = new ArrayList<Integer>();
		preorder_DFS(root, result, 0);
		return result;
	}

	public static void main(String[] args) {
		LargestValue_Each_Tree_Row_515 test = new LargestValue_Each_Tree_Row_515();

		// Create Tree [1,3,2,5,3,null,9]
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(3);
		root.right = new TreeNode(2);
		root.left.left = new TreeNode(5);
		root.left.right = new TreeNode(3);
		root.right.right = new TreeNode(9);
		
		List<Integer> result = test.largestValues(root);
		System.out.println(result);
	}
}
