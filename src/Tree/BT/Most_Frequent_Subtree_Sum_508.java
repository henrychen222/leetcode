/* 2.23 morning (understand) 2019
 * https://leetcode.com/problems/most-frequent-subtree-sum/
 * 
 * https://docs.oracle.com/javase/7/docs/api/java/util/Iterator.html
 *   hasNext()： Returns true if the iteration has more elements.
 *   next()： Returns the next element in the iteration.
 * https://docs.oracle.com/javase/7/docs/api/java/util/Map.Entry.html
 *   getKey(): Returns the key corresponding to this entry.
 *   getValue(): Returns the value corresponding to this entry.
 * */

package Tree.BT;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Arrays;

public class Most_Frequent_Subtree_Sum_508 {
	// 找到出现最频繁的子树和
	public int[] findFrequentTreeSum(TreeNode root) {
		if (root == null) {
			return new int[0];
		}
		// 子树和为key, 出现次数为value
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		countSum(root, map);
		// 数组来存储hashMap中的Key,也就是子数和
		int[] occur = new int[map.size()];
		// 出现的次数最多的子树和，从hashMap中的value
		int largest = 0;
		// 出现的次数最多的子树和，的个数
		int num = 0;

		Iterator iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry entry = (Map.Entry) iterator.next();

			// 当出现更大次数的时候就重新记录，
			if ((int) entry.getValue() > largest) {
				occur[0] = (int) entry.getKey(); // 把最大子树和存到数组第一个位置中
				num = 1; // 出现的次数最多的子树和，个数记为1
				largest = (int) entry.getValue(); // 重设出现的次数最多的子树和
			} else if ((int) entry.getValue() == largest) {
				occur[num] = (int) entry.getKey();
				num++;
			}
		}

		// 遍历HashMap完后前num个数组元素是要的结果
		return Arrays.copyOfRange(occur, 0, num);
	}

	// 记录所有子树和:
	// 用HashMap，以子树和为key，以出现次数为value，对于已经出现过的子树和，就将其value+1，没出现过的就添加到HashMap中去，其value设为1
	public int countSum(TreeNode root, HashMap<Integer, Integer> map) {
		int sumValue = 0;
		sumValue += root.val;
		if (root.left != null) {
			sumValue += countSum(root.left, map);
		}
		if (root.right != null) {
			sumValue += countSum(root.right, map);
		}

		if (map.get(sumValue) != null) {
			// 已经出现过的子树和，就将其value+1
			map.put(sumValue, map.get(sumValue) + 1);
		} else {
			// 没出现过的就添加到HashMap中去，其value设为1
			map.put(sumValue, 1);
		}
		return sumValue;
	}

	public static void main(String[] args) {
		Most_Frequent_Subtree_Sum_508 test = new Most_Frequent_Subtree_Sum_508();

		// Create Tree [5,2,-3]
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(2);
		root.right = new TreeNode(-3);
		
		int[] result = test.findFrequentTreeSum(root);
//		for (int i = 0; i < result.length; i++) {
//			System.out.println(result[i]);
//		}
		for (int i : result) {
			System.out.println(i);
		}
	}
}