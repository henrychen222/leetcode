/* 2.17 night 2019
 * https://leetcode.com/problems/house-robber-iii/    ???
 * */

package Tree.BT;

public class House_Robber_III_337 {
	// Method 1
	public int[] DFS(TreeNode root) {
		int[] rob = { 0, 0 };
		if (root != null) {
			int[] robLeft = DFS(root.left);
			int[] robRight = DFS(root.right);
			// rob[0]存储的是从叶子节点到当前节点的左右孩子层节点抢劫到的最大值
			rob[0] = robLeft[1] + robRight[1];
			// rob[1]存储的是从叶子节点到当前节点抢劫的最大值
			rob[1] = Math.max(robLeft[0] + robRight[0] + root.val, rob[0]);

		}
		return rob;
	}

	// Method 2
	public int[] helper(TreeNode root) {
		if (root == null) {
			int[] rob = { 0, 0 };
			return rob;
		}

		int[] rob = new int[2];
		int[] robLeft = helper(root.left);
		int[] robRight = helper(root.right);

		rob[0] = robLeft[1] + robRight[1] + root.val;
		rob[1] = Math.max(robLeft[0], robLeft[1]) + Math.max(robRight[0], robRight[1]);

		return rob;
	}

	public int rob(TreeNode root) {
		// //Method 1
		// return DFS(root)[1];

		// Method 2
		if (root == null) {
			return 0;
		}
		int[] rob = helper(root);
		return Math.max(rob[0], rob[1]);
	}

	public static void main(String[] args) {
		House_Robber_III_337 test = new House_Robber_III_337();

		// Create Tree [3,2,3,null,3,null,1]
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.left.right = new TreeNode(3);
		root.right.right = new TreeNode(1);

		int result = test.rob(root);
		System.out.println(result);
	}
}
