/* 3.17 evening 2019
 * https://leetcode.com/problems/minimum-depth-of-binary-tree/
 * */

package Tree.BT;

import java.util.LinkedList;

public class BT_MinDepth_111 {

	public int minDepth(TreeNode root) {
		LinkedList<TreeNode> nodes = new LinkedList<TreeNode>();
		LinkedList<Integer> counts = new LinkedList<Integer>();
		nodes.add(root);
		counts.add(1);

		if (root == null) {
			return 0;
		}

		while (!nodes.isEmpty()) {
			TreeNode node = nodes.remove();
			int count = counts.remove();
			if (node.left == null && node.right == null) {
				return count;
			}
			if (node.left != null) {
				nodes.add(node.left);
				counts.add(count + 1);
			}
			if (node.right != null) {
				nodes.add(node.right);
				counts.add(count + 1);
			}
		}
		return 0;
	}

	public static void main(String[] args) {
		BT_MinDepth_111 test = new BT_MinDepth_111();

		// Create Tree [3,9,20,null,null,15,7]
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);

		int depth = test.minDepth(root);
		System.out.println(depth);
	}

}
