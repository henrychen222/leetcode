/* 2.25 noon 2019
 * https://leetcode.com/problems/binary-tree-tilt/
 * */
package Tree.BT;

public class BT_Tilt_563 {
	int sum = 0;

	public int findTilt(TreeNode root) {
		helper(root);
		return sum;
	}

	public int helper(TreeNode root) {
		if (root == null) {
			return 0;
		}
		int left = helper(root.left);
		int right = helper(root.right);
		sum += Math.abs(left - right);
		return root.val + left + right; // ??
	}

	public static void main(String[] args) {
		BT_Tilt_563 test = new BT_Tilt_563();

		// Create Tree [1,2,3]
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);

		int result = test.findTilt(root);
		System.out.println(result);
	}
}
