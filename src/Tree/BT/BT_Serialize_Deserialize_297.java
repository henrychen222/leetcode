/* 2.15 night 2019
 * https://leetcode.com/problems/serialize-and-deserialize-binary-tree/
 * */

package Tree.BT;

import java.util.Stack;

public class BT_Serialize_Deserialize_297 {

	/* use Preorder Traversal */

	//Encodes a tree to a single string.
	public String serialize(TreeNode root) {
		if (root == null) {
			return null;
		}

		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(root);
		StringBuilder data = new StringBuilder();

		// iterative pop out TreeNode saved in stack
		while (!stack.isEmpty()) {
			TreeNode node = stack.pop();
			if (node != null) {
				data.append(node.val + ",");// append TreeNode values
				// iterative add
				stack.push(node.right);
				stack.push(node.left);
			} else {   
//				data.append("#")   type wrong here    
				data.append("#,"); //??? 
			}
		}
		return data.toString().substring(0, data.length() - 1); // beginIndex, endIndex
	}

	// Decodes your encoded data to tree.
	public TreeNode deserialize(String data) {
		if (data == null) {
			return null;
		}
		int[] data_int = { 0 };
		String[] node_val = data.split(",");
		return deserialize_recursion(node_val, data_int);
	}

	public TreeNode deserialize_recursion(String[] s, int[] t) {
		// s[t[0]] is a string   ????
		if (s[t[0]].equals("#")) {
			return null;
		}

		TreeNode root = new TreeNode(Integer.parseInt(s[t[0]])); // covert string to int, and create root with int

		t[0] = t[0] + 1;
		root.left = deserialize_recursion(s, t);
		t[0] = t[0] + 1;
		root.right = deserialize_recursion(s, t);
		return root;

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
