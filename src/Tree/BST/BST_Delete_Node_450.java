/*2.22 noon at library 
 * https://leetcode.com/problems/delete-node-in-a-bst/
 * */
package Tree.BST;

public class BST_Delete_Node_450 {

	public TreeNode searchLeft(TreeNode node) {
//		if (node.left != null) {
//			node = node.left;
//		}
		while (node.left != null) {
			node = node.left;
		}
		return node;
	}

	public TreeNode deleteNode(TreeNode root, int key) {
		if (root == null) {
			return null;
		}

		if (root.val > key) {
			// delete the root.left , fill the root.left position
			root.left = deleteNode(root.left, key);
		} else if (root.val < key) {
			// delete the root.right , fill the root.right position
			root.right = deleteNode(root.right, key);
		} else {
			// val = key
			if (root.left == null) {
				return root.right;
			} 
//			else if (root.right == null) {
//				return root.left;
//			}
			if (root.right == null) {
				return root.left;
			}

			// root.left and root.right exist
			TreeNode temp = searchLeft(root.right);
			// 将当前节点重新赋值
			root.val = temp.val;
			// 重新构建右子树, 方式:将右子树的最左叶结点的值付给当前节点，然后删除该最左叶结点
			root.right = deleteNode(root.right, temp.val);

		}
		return root;
	}

	public static void main(String[] args) {
		BST_Delete_Node_450 test = new BST_Delete_Node_450();

		// Create Tree [5,3,6,2,4,null,7]
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(3);
		root.right = new TreeNode(6);
		root.left.left = new TreeNode(2);
		root.left.right = new TreeNode(4);
		root.right.right = new TreeNode(7);

		int key = 3;
		TreeNode result = test.deleteNode(root, key);
		// show result
		root.printPreorder(result);

	}
}