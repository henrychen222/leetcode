/*2.22, 2019 afternoon 
 * https://leetcode.com/problems/find-mode-in-binary-search-tree/
 * */

package Tree.BST;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class BST_Find_Mode_501 {

	private Map<Integer, Integer> map;
	private int max = 0;

	public void in_order(TreeNode node) {
		if (node.left != null) {
			in_order(node.left);
		}

		// get(): Returns the value to which the specified key is mapped, or null if
		// this map contains no mapping for the key
		// getOrDefault(): Returns the value to which the specified key is mapped, or
		// defaultValue if this map contains no mapping for the key
		map.put(node.val, map.getOrDefault(node.val, 0) + 1);
		max = Math.max(max, map.get(node.val));

		if (node.right != null) {
			in_order(node.right);
		}
	}

	public int[] findMode(TreeNode root) {
		if (root == null) {
			return new int[0];
		}
		this.map = new HashMap<>();
		in_order(root);

		List<Integer> list = new LinkedList<>();
		// keySet(): Returns a Set view of the keys contained in this map
		for (int key : map.keySet()) {
			if (map.get(key) == max) {
				list.add(key);
			}
		}

		// save the list data into array and return it
		int[] result = new int[list.size()];
		for (int i = 0; i < result.length; i++) {
			result[i] = list.get(i);
		}
		return result;

	}

	public static void main(String[] args) {
		BST_Find_Mode_501 test = new BST_Find_Mode_501();

		// Create Tree [1,null,2,2]
		TreeNode root = new TreeNode(1);
		root.right = new TreeNode(2);
		root.right.left = new TreeNode(2);

		int[] result = test.findMode(root);
		
		//two ways to print the array
		for (int i = 0; i < result.length; i++) {
			System.out.println(result[i]);
		}
		
		for (int i : result) {
			System.out.println(i);
		}

	}

}
