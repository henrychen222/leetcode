/*
 * 2.18 night 2019
 * https://leetcode.com/problems/serialize-and-deserialize-bst/   hard
 * */

package Tree.BST;

import java.util.Stack;

public class BST_Serialize_Deserialize_449 {

	// wrong
	public String serialize_wrong(TreeNode root) {
		// StringBuilder sb = new StringBuilder("");
		StringBuffer sb = new StringBuffer("");
		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(root);
		while (stack.size() != 0) {
			TreeNode node = stack.pop(); // create a TreeNode with root ** wrong here will report nullpointer Exception
			if (node != null) {
				stack.push(node.left);
				stack.push(node.right);
				sb.append(node.val);
				sb.append(",");
			}
		}
		return sb.toString();
	}

	// Encodes a tree to a single string.
	public String serialize(TreeNode root) {
		// StringBuilder sb = new StringBuilder("");
		StringBuffer sb = new StringBuffer("");
		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(root);
		while (stack.size() != 0) {
			root = stack.pop(); // reuse root
			if (root != null) {
				// stack.push(root.left);
				// stack.push(root.right);
				stack.push(root.right);
				stack.push(root.left);
				sb.append(root.val);
				sb.append(",");
			}
		}
		return sb.toString();
	}

	// Decodes your encoded data to tree.
	public TreeNode deserialize(String data) {
		if (data.length() == 0) {
			return null;
		}
		String[] data_nums_arr = data.split(",");
		TreeNode root = new TreeNode(Integer.parseInt(data_nums_arr[0])); // first element in data_nums_arr to be root
		TreeNode iterator_node = root;
		Stack<TreeNode> stack = new Stack<TreeNode>();

		for (int i = 1; i < data_nums_arr.length; i++) {
			// for (int i = 0; i < data_nums_arr.length; i++) {
			TreeNode each_node = new TreeNode(Integer.parseInt(data_nums_arr[i])); // convert all string to a TreeNode
			if (each_node.val < iterator_node.val) {
				iterator_node.left = each_node; // ??
				stack.push(iterator_node);
			} else {
				// stack not empty
				while (!stack.isEmpty()) {
					TreeNode parent_node = stack.pop();
					if (each_node.val < parent_node.val) {
						iterator_node.right = each_node; // ??
						stack.push(parent_node);
						break;
					} else {
						iterator_node = parent_node;
					}
				}
				// stack empty
				if (stack.empty()) {
					iterator_node.right = each_node;
				}
			}
			// didn't write this
			iterator_node = each_node;
		}
		return root;
	}

	public static void main(String[] args) {
		BST_Serialize_Deserialize_449 codec = new BST_Serialize_Deserialize_449();

		// Create Tree [2,1,3]
		TreeNode root = new TreeNode(2);
		root.left = new TreeNode(1);
		root.right = new TreeNode(3);
		
		//check output
		TreeNode result = codec.deserialize(codec.serialize(root));
		result.printPreorder(root);
	}

}
