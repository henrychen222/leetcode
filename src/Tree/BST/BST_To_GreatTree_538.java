/* 2.24 afternoon  2019
 * https://leetcode.com/problems/convert-bst-to-greater-tree/
 * */
package Tree.BST;

public class BST_To_GreatTree_538 {

	int sum = 0;

	public TreeNode convertBST(TreeNode root) {
		convert(root);
		return root;
	}

	public void convert(TreeNode node) {
//		int sum = 0;
		if (node == null) {
			return;
		}
		convert(node.right);
		node.val += sum;
		sum = node.val;
		convert(node.left);
	}

	public static void main(String[] args) {
		BST_To_GreatTree_538 test = new BST_To_GreatTree_538();

		// Create Tree [5,2,13]
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(2);
		root.right = new TreeNode(13);
		
		TreeNode result = test.convertBST(root);
		root.printPreorder(result);
	}

}
