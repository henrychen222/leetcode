/* 2.12 night  2.14 night 2019
 * https://leetcode.com/problems/kth-smallest-element-in-a-bst/
 */

package Tree.BST;

import java.util.Stack;

public class BST_Kth_Smallest_Element_230 {

	public int kthSmallest(TreeNode root, int k) {
		// use inorder traverse left --> root --> right
		Stack<TreeNode> stack = new Stack<TreeNode>();
		// save root node
		TreeNode root_node = root;
		int result = 0;

		while (!stack.isEmpty() || root_node != null) {
			// left
			if (root_node != null) {
				stack.push(root_node);
				root_node = root_node.left;
			} else {
				TreeNode temp = stack.pop();
				k--;
				// root, only one element, smallest element is it
				if (k == 0) {
					result = temp.val;
				}
				// right
				root_node = temp.right;
			}
		}
		return result;
	}

	// Method 2
	public int kthSmallest_Method2(TreeNode root, int k) {
		Stack<TreeNode> stack = new Stack<TreeNode>();
		// save root node
		TreeNode root_node = root;

		while (root_node != null) {
			stack.push(root_node);
			root_node = root_node.left;
		}
		int i = 0;
		// ??
		while (!stack.isEmpty()) {
			TreeNode temp = stack.pop();
			i++;
			if (i == k) {
				return temp.val;
			}
			TreeNode temp_right = temp.right;
			while (temp_right != null) {
				stack.push(temp_right);
				temp_right = temp_right.left;
			}

		}
		return -1;
	}

	public static void main(String[] args) {
		BST_Kth_Smallest_Element_230 test = new BST_Kth_Smallest_Element_230();

		// Create Tree [3,1,4,null,2]
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(1);
		root.right = new TreeNode(4);
		root.left.right = new TreeNode(2);

		// check input parameters
		int k = 1;

		// check output
		int result = test.kthSmallest(root, k);
		int result_method2 = test.kthSmallest_Method2(root, k);
		System.out.println(result);
		System.out.println(result_method2);

	}

}
