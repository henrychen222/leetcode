/*  2.14 night 2019
 * https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/
 * */

package Tree.BST;

public class BST_LowestCommonAncestor_235 {

	// Recursive
	public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
		TreeNode root_node = root;
		// p is left, q is right, so common ancestor is root_node
		if (root_node.val > p.val && root_node.val < q.val) {
			return root_node;
		}
		// p q both is in left
		if (root_node.val > p.val && root_node.val > q.val) {
			return lowestCommonAncestor(root_node.left, p, q);
		}
		// p q both is in right
		if (root_node.val < p.val && root_node.val < q.val) {
			return lowestCommonAncestor(root_node.right, p, q);
		} else {
			return root;
		}
	}

	// Method2: iterative
	public TreeNode lowestCommonAncestor_iterative(TreeNode root, TreeNode p, TreeNode q) {
		TreeNode root_node = root;
		while (root_node != null) {
			// p q both is in left
			if (root_node.val > p.val && root_node.val > q.val) {
				root_node = root_node.left;
			}
			// p q both is in right
			else if (root_node.val < p.val && root_node.val < q.val) {
				root_node = root_node.right;
			} else {
				// p q one is left another is right
				return root_node;
			}

		}
		return null;
	}

	public static void main(String[] args) {
		BST_LowestCommonAncestor_235 test = new BST_LowestCommonAncestor_235();

		// Create Tree [6,2,8,0,4,7,9,null,null,3,5]
		TreeNode root = new TreeNode(6);
		root.left = new TreeNode(2);
		root.right = new TreeNode(8);
		root.left.left = new TreeNode(0);
		root.left.right = new TreeNode(4);
		root.right.left = new TreeNode(7);
		root.right.right = new TreeNode(9);
		root.left.right.left = new TreeNode(3);
		root.left.right.right = new TreeNode(5);

		// check input parameters p = 2 q = 8
		TreeNode p = new TreeNode(2);
		TreeNode q = new TreeNode(8);

		// check output
		TreeNode result = test.lowestCommonAncestor(root, p, q);
		TreeNode result_method2 = test.lowestCommonAncestor_iterative(root, p, q);
		System.out.println(result.val);
		System.out.println(result_method2.val);
	}

}
