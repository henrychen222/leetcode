/**
 * 12.31 night
 * https://leetcode.com/problems/clone-graph/
 * https://www.geeksforgeeks.org/clone-an-undirected-graph/
 */
package Graph;

import java.util.List;
import java.util.Vector;

class Node {
	public int val;
	public List<Node> neighbors;

	public Node() {
	}

	public Node(int _val, List<Node> _neighbors) {
		val = _val;
		neighbors = _neighbors;
	}

	public Node(int val) {
		this.val = val;
		neighbors = new Vector<Node>();
	}

	public void addEdge(Node node) {
		if (!neighbors.contains(node))
			neighbors.add(node);
	}
};