/**
 * 12.31 night
 * https://leetcode.com/problems/clone-graph/
 */
package Graph;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Vector;

public class M_133_CloneGraph {

	/**
	 * DFS Time Limited
	 */
	public Node cloneGraph(Node node) {
		HashMap<Node, Node> map = new HashMap<>();
		map.put(node, new Node(node.val, new ArrayList<>()));

		for (Node neighbor : node.neighbors) {
			if (map.containsKey(neighbor)) {
				map.get(node).neighbors.add(map.get(neighbor));
			} else {
				map.get(node).neighbors.add(cloneGraph(neighbor));
			}
		}

		return map.get(node);
	}

	/**
	 * Accepted --- 2 ms 33.2 MB 48.69%
	 */
	public Node cloneGraph2(Node node) {
		Map<Node, Node> map = new HashMap<>();
		Queue<Node> queue = new ArrayDeque<>();

		queue.offer(node);
		map.put(node, new Node(node.val, new ArrayList<>()));
		while (!queue.isEmpty()) {
			Node h = queue.poll();

			for (Node neighbor : h.neighbors) {
				if (!map.containsKey(neighbor)) {
					map.put(neighbor, new Node(neighbor.val, new ArrayList<>()));
					queue.offer(neighbor);
				}
				map.get(h).neighbors.add(map.get(neighbor));
			}
		}
		return map.get(node);
	}

	public Node buildGraph() {
		// Build Graph
		// https://www.geeksforgeeks.org/clone-an-undirected-graph/
		Node node1 = new Node(1);
		Node node2 = new Node(2);
		Node node3 = new Node(3);
		Node node4 = new Node(4);

		Vector<Node> v = new Vector<Node>();
		v.add(node2);
		v.add(node4);
		node1.neighbors = v;

		v = new Vector<Node>();
		v.add(node1);
		v.add(node3);
		node2.neighbors = v;

		v = new Vector<Node>();
		v.add(node2);
		v.add(node4);
		node3.neighbors = v;

		v = new Vector<Node>();
		v.add(node3);
		v.add(node1);
		node4.neighbors = v;
		
		return node1;
	}

	public static void main(String[] args) {

	}

}
